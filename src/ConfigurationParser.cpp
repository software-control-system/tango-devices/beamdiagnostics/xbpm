//=============================================================================
// ConfigurationParser.cpp
//=============================================================================
// abstraction.......Mode configuration parser
// class.............ConfigurationParser
// original author...S. GARA - NEXEYA
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "ConfigurationParser.h"
#include <yat/utils/StringTokenizer.h>
#include <yat/utils/XString.h>
#include <yat/utils/String.h>
#include <yat/Exception.h>

namespace Xbpm_ns
{

// ======================================================================
// ConfigurationParser::ConfigurationParser
// ======================================================================
ConfigurationParser::ConfigurationParser(Tango::DeviceImpl * host_device)
: Tango::LogAdapter(host_device)
{
}

// ======================================================================
// ConfigurationParser::~ConfigurationParser
// ======================================================================
ConfigurationParser::~ConfigurationParser ()
{
}

// ======================================================================
// ConfigurationParser::parseConfigProperty
// ====================================================================== 
ModeKeys_t ConfigurationParser::parseConfigProperty (std::vector<std::string> config_property)
{
  DEBUG_STREAM << "ConfigurationParser::parseConfigProperty ==>" << std::endl;

  // This function parses a "Mode<i>" property and fills a map with the <KEY, value>
  // list defining the mode.

  ModeKeys_t modeKeyList;

  // Parse config vector and extract KEY::value fields
  for (std::vector<std::string>::iterator it = config_property.begin() ; it != config_property.end(); ++it)
  {
    std::vector<std::string> tokenList = splitLine(*it, kKEY_VALUE_SEPARATOR);

    if (tokenList.size() != 2)
    {
      // empty list => fatal error
      yat::OSStream oss;
      oss << "Bad configuration syntax: " << *it << " - check device property";
      ERROR_STREAM << oss.str() << std::endl;
      THROW_DEVFAILED("CONFIGURATION_ERROR", 
                      oss.str().c_str(), 
                      "ConfigurationParser::parseConfigProperty");
    }

    // insert <key,value> in map
    modeKeyList.insert(ModeKey_pair_t(tokenList[0], tokenList[1]));
    DEBUG_STREAM << "ConfigurationParser::parseConfigProperty - add <" << tokenList[0] << "," << tokenList[1] << "> field" << std::endl;
  }

  return modeKeyList;
}


// ======================================================================
// ConfigurationParser::splitLine
// ======================================================================
std::vector<std::string> ConfigurationParser::splitLine(std::string line, std::string separator, yat::uint16 min_token, yat::uint16 max_tokens)
{
	//- result : the split line 
    std::vector<std::string> tokenList;

    yat::StringTokenizer st(line, separator);

	while (st.has_more_tokens()) // while there is a remaining token
	{
        std::string token_str = st.next_token();
		tokenList.push_back(token_str);
	}

    // checks minimum expected number of tokens if not set to a null value
    if ((min_token != 0) &&
        (tokenList.size() < min_token) )
    {
        // expected more tokens
        yat::OSStream oss;
        oss << "Bad value: " << line << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED("CONFIGURATION_ERROR", 
                        oss.str().c_str(), 
                        "ConfigurationParser::splitLine"); 
    }

    // checks maximum expected number of tokens if not set to a null value
    if ((max_tokens != 0) &&
        (tokenList.size() > max_tokens) )
    {
        // expected less tokens
        yat::OSStream oss;
        oss << "Bad value: " << line << " - check device property";
        ERROR_STREAM << oss.str() << std::endl;
        THROW_DEVFAILED("CONFIGURATION_ERROR", 
                        oss.str().c_str(), 
                        "ConfigurationParser::splitLine"); 
    }

	return tokenList;
}

// ======================================================================
// ConfigurationParser::testKey
// ======================================================================
bool ConfigurationParser::testKey(std::string line, std::string separator, std::string key)
{
  bool isWrightKey = false;
  std::vector<std::string> tokenList = splitLine(line, separator);

  if (tokenList.size() != 2)
  {
    // empty list => fatal error
    yat::OSStream oss;
    oss << "Bad configuration syntax: " << line << " - check device property";
    ERROR_STREAM << oss.str() << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    oss.str().c_str(), 
                    "ConfigurationParser::getKeyValue");
  }

  if (0 == tokenList[0].compare(key))
  {
    // Wright key word => return true
    isWrightKey = true;
  }

  return isWrightKey;
}

// ======================================================================
// ConfigurationParser::getBoolean
// ======================================================================
bool ConfigurationParser::getBoolean(std::string token)
{
  bool result = false;
  yat::String ytoken(token);
  
  if (ytoken.is_equal_no_case("true"))
  {
    result = true;
  }
  else if (ytoken.is_equal_no_case("false"))
  {
    result = false;
  }
  else
  {
    // bad boolean value ==> fatal error
    THROW_DEVFAILED("CONFIGURATION_ERROR", 
                    "Bad boolean value", 
                    "ConfigurationParser::parseConfigProperty"); 
  }

  return result;
}

// ======================================================================
// ConfigurationParser::extractOffsetX
// ======================================================================
double ConfigurationParser::extractOffsetX(ModeKeys_t acq_config)
{
	double l_offset_x = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_X);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OffsetX not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_offset_x = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad offset X value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOffsetX"); 
		}
	}

	DEBUG_STREAM << "OffsetX: " << l_offset_x << std::endl;

	return l_offset_x;
}

// ======================================================================
// ConfigurationParser::extractOffsetZ
// ======================================================================
double ConfigurationParser::extractOffsetZ(ModeKeys_t acq_config)
{
	double l_offset_z = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_Z);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OffsetZ not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_offset_z = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad offset Z value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOffsetZ"); 
		}
	}

	DEBUG_STREAM << "OffsetZ: " << l_offset_z << std::endl;

	return l_offset_z;
}

// ======================================================================
// ConfigurationParser::extractKXFactor
// ======================================================================
double ConfigurationParser::extractKXFactor(ModeKeys_t acq_config)
{
	double l_k_x_factor = 1;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_K_X_FACTOR);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Kx factor not found in configuration - using default value : 1";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_k_x_factor = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad Kx factor value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractKXFactor"); 
		}
	}

	DEBUG_STREAM << "Kx factor: " << l_k_x_factor << std::endl;

	return l_k_x_factor;
}

// ======================================================================
// ConfigurationParser::extractKZFactor
// ======================================================================
double ConfigurationParser::extractKZFactor(ModeKeys_t acq_config)
{
	double l_k_z_factor = 1;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_K_Z_FACTOR);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Kz factor not found in configuration - using default value : 1";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_k_z_factor = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad Kz factor value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractKZFactor"); 
		}
	}

	DEBUG_STREAM << "Kz factor: " << l_k_z_factor << std::endl;

	return l_k_z_factor;
}

// ======================================================================
// ConfigurationParser::extractMisalignementOffset
// ======================================================================
double ConfigurationParser::extractMisalignementOffset(ModeKeys_t acq_config)
{
	double l_mis_offset = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_MISALIGNEMENT_OFFSET);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "MisalignementOffset not found in configuration - check device property";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_mis_offset = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad misalignement offset value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractMisalignementOffset"); 
		}
	}

	DEBUG_STREAM << "MisalignementOffset: " << l_mis_offset << std::endl;

	return l_mis_offset;
}

// ======================================================================
// ConfigurationParser::extractMisalignementOffsetX
// ======================================================================
double ConfigurationParser::extractMisalignementOffsetX(ModeKeys_t acq_config)
{
	double l_mis_offset_x = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_MISALIGNEMENT_OFFSET_X);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "MisalignementOffsetX not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_mis_offset_x = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad misalignement offset X value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractMisalignementOffsetX"); 
		}
	}

	DEBUG_STREAM << "MisalignementOffsetX: " << l_mis_offset_x << std::endl;

	return l_mis_offset_x;
}

// ======================================================================
// ConfigurationParser::extractMisalignementOffsetZ
// ======================================================================
double ConfigurationParser::extractMisalignementOffsetZ(ModeKeys_t acq_config)
{
	double l_mis_offset_z = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_MISALIGNEMENT_OFFSET_Z);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "MisalignementOffsetZ not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_mis_offset_z = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad misalignement offset Z value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractMisalignementOffsetZ"); 
		}
	}

	DEBUG_STREAM << "MisalignementOffsetZ: " << l_mis_offset_z << std::endl;

	return l_mis_offset_z;
}

// ======================================================================
// ConfigurationParser::extractElectronicOffset
// ======================================================================
double ConfigurationParser::extractElectronicOffset(ModeKeys_t acq_config)
{
	double l_op_offset = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_ELECTRONIC_OFFSET);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "ElectronicOffset not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_op_offset = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad Electronic offset value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractElectronicOffset"); 
		}
	}

	DEBUG_STREAM << "ElectronicOffset: " << l_op_offset << std::endl;

	return l_op_offset;
}

// ======================================================================
// ConfigurationParser::extractOperatorOffset
// ======================================================================
double ConfigurationParser::extractOperatorOffset(ModeKeys_t acq_config)
{
	double l_op_offset = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OPERATOR_OFFSET);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OperatorOffset not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_op_offset = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad operator offset value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOperatorOffset"); 
		}
	}

	DEBUG_STREAM << "OperatorOffset: " << l_op_offset << std::endl;

	return l_op_offset;
}


// ======================================================================
// ConfigurationParser::extractOperatorOffsetX
// ======================================================================
double ConfigurationParser::extractOperatorOffsetX(ModeKeys_t acq_config)
{
	double l_op_offset_x = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OPERATOR_OFFSET_X);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OperatorOffsetX not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_op_offset_x = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad operator offset X value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOperatorOffsetX"); 
		}
	}

	DEBUG_STREAM << "OperatorOffsetX: " << l_op_offset_x << std::endl;

	return l_op_offset_x;
}


// ======================================================================
// ConfigurationParser::extractOperatorOffsetZ
// ======================================================================
double ConfigurationParser::extractOperatorOffsetZ(ModeKeys_t acq_config)
{
	double l_op_offset_z = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OPERATOR_OFFSET_Z);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OperatorOffsetZ not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_op_offset_z = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad operator offset Z value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOperatorOffsetZ"); 
		}
	}

	DEBUG_STREAM << "OperatorOffsetZ: " << l_op_offset_z << std::endl;

	return l_op_offset_z;
}

// ======================================================================
// ConfigurationParser::extractCorrectionTable
// ======================================================================
std::string ConfigurationParser::extractCorrectionTable(ModeKeys_t acq_config)
{

	std::string l_corr_table = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_CORR_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: CorrectionTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractCorrectionTable");
	}
	else
	{
		l_corr_table = it->second;
	}

	return l_corr_table;
}

// ======================================================================
// ConfigurationParser::extractZCorrectionTable
// ======================================================================
std::string ConfigurationParser::extractZCorrectionTable(ModeKeys_t acq_config)
{
	std::string l_corr_table = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ZCORR_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: ZCorrectionTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractZCorrectionTable");
	}
	else
	{
		l_corr_table = it->second;
	}

	return l_corr_table;
}

// ======================================================================
// ConfigurationParser::extractKZ1Factor
// ======================================================================
double ConfigurationParser::extractKZ1Factor(ModeKeys_t acq_config)
{
	double l_k_z1_factor = 1;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_K_Z1_FACTOR);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Kz1 factor not found in configuration - using default value : 1";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_k_z1_factor = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad Kz1 factor value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractKZ1Factor"); 
		}
	}

	DEBUG_STREAM << "Kz1 factor: " << l_k_z1_factor << std::endl;

	return l_k_z1_factor;
}

// ======================================================================
// ConfigurationParser::extractKZ2Factor
// ======================================================================
double ConfigurationParser::extractKZ2Factor(ModeKeys_t acq_config)
{
	double l_k_z2_factor = 1;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_K_Z2_FACTOR);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "Kz2 factor not found in configuration - using default value : 1";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_k_z2_factor = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad Kz2 factor value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractKZ2Factor"); 
		}
	}

	DEBUG_STREAM << "Kz2 factor: " << l_k_z2_factor << std::endl;

	return l_k_z2_factor;
}

// ======================================================================
// ConfigurationParser::extractDeltaZ
// ======================================================================
double ConfigurationParser::extractDeltaZ(ModeKeys_t acq_config)
{
	double l_delta_z = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_DELTA_Z);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "DeltaZ not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_delta_z = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad DeltaZ value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractDeltaZ"); 
		}
	}

	DEBUG_STREAM << "DEltaZ: " << l_delta_z << std::endl;

	return l_delta_z;
}

// ======================================================================
// ConfigurationParser::extractOffsetZ1
// ======================================================================
double ConfigurationParser::extractOffsetZ1(ModeKeys_t acq_config)
{
	double l_offset_z1 = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_Z1);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OffsetZ1 not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_offset_z1 = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad OffsetZ1 value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOffsetZ1"); 
		}
	}

	DEBUG_STREAM << "OffsetZ1: " << l_offset_z1 << std::endl;

	return l_offset_z1;
}

// ======================================================================
// ConfigurationParser::extractOffsetZ2
// ======================================================================
double ConfigurationParser::extractOffsetZ2(ModeKeys_t acq_config)
{
	double l_offset_z2 = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_Z2);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "OffsetZ2 not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_offset_z2 = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad OffsetZ2 value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractOffsetZ2"); 
		}
	}

	DEBUG_STREAM << "OffsetZ2: " << l_offset_z2 << std::endl;

	return l_offset_z2;
}


// ======================================================================
// ConfigurationParser::extractDetectorGap
// ======================================================================
double ConfigurationParser::extractDetectorGap(ModeKeys_t acq_config)
{
	double l_detector_gap = 0;

	// get timeout 
	ModeKeys_it_t it = acq_config.find(kKEY_DETECTOR_GAP);

	if (it == acq_config.end())
	{
		// key not found => error
		yat::OSStream oss;
		oss << "DetectorGap not found in configuration - using default value : 0";
		INFO_STREAM << oss.str() << std::endl;
	}
	else
	{
		try
		{
			l_detector_gap = yat::XString<double>::to_num(it->second);
		}
		catch (const yat::Exception& ye)
		{
			// bad int value ==> fatal error
			yat::OSStream oss;
			oss << "Bad DetectorGap value: " << it->second << " - check device property";
			ERROR_STREAM << oss.str() << std::endl;
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				oss.str().c_str(), 
				"ConfigurationParser::extractDetectorGap"); 
		}
	}

	DEBUG_STREAM << "DetectorGap: " << l_detector_gap << std::endl;

	return l_detector_gap;
}
// ======================================================================
// ConfigurationParser::extractBeamEnergyProxyName
// ======================================================================
std::string ConfigurationParser::extractBeamEnergyProxyName(ModeKeys_t acq_config)
{
	std::string l_proxy_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_DEVICE_BEAM);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: BeamEnergyProxyName not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractBeamEnergyProxyName");
	}
	else
	{
		l_proxy_name = it->second;
	}

	return l_proxy_name;
}

// ======================================================================
// ConfigurationParser::extractHSlitProxyName
// ======================================================================
std::string ConfigurationParser::extractHSlitProxyName(ModeKeys_t acq_config)
{
	std::string l_proxy_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_DEVICE_HSLIT);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: HSlitProxyName not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractHSlitProxyName");
	}
	else
	{
		l_proxy_name = it->second;
	}

	return l_proxy_name;
}

// ======================================================================
// ConfigurationParser::extractVSlitProxyName
// ======================================================================
std::string ConfigurationParser::extractVSlitProxyName(ModeKeys_t acq_config)
{
	std::string l_proxy_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_DEVICE_VSLIT);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: VSlitProxyName not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractVSlitProxyName");
	}
	else
	{
		l_proxy_name = it->second;
	}

	return l_proxy_name;
}

// ======================================================================
// ConfigurationParser::extractInsertionProxyName
// ======================================================================
std::string ConfigurationParser::extractInsertionProxyName(ModeKeys_t acq_config)
{
	std::string l_proxy_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_DEVICE_INSERTION);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: InsertionProxyName not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractInsertionProxyName");
	}
	else
	{
		l_proxy_name = it->second;
	}

	return l_proxy_name;
}

// ======================================================================
// ConfigurationParser::extractHSlitGapAttr
// ======================================================================
std::string ConfigurationParser::extractHSlitGapAttr(ModeKeys_t acq_config)
{
	std::string l_attr_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ATTR_HSLIT_GAP);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: HSlitGapAttr not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractHslitGapAttr");
	}
	else
	{
		l_attr_name = it->second;
	}

	return l_attr_name;
}

// ======================================================================
// ConfigurationParser::extractVSlitGapAttr
// ======================================================================
std::string ConfigurationParser::extractVSlitGapAttr(ModeKeys_t acq_config)
{
	std::string l_attr_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ATTR_VSLIT_GAP);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: VSlitGapAttr not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractVSlitGapAttr");
	}
	else
	{
		l_attr_name = it->second;
	}

	return l_attr_name;
}

// ======================================================================
// ConfigurationParser::extractEnergyAttr
// ======================================================================
std::string ConfigurationParser::extractEnergyAttr(ModeKeys_t acq_config)
{
	std::string l_attr_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ATTR_ENERGY);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: EnergyAttr not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractEnergyAttr");
	}
	else
	{
		l_attr_name = it->second;
	}

	return l_attr_name;
}

// ======================================================================
// ConfigurationParser::extractPhaseAttr
// ======================================================================
std::string ConfigurationParser::extractPhaseAttr(ModeKeys_t acq_config)
{
	std::string l_attr_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ATTR_PHASE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: PhaseAttr not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractPhaseAttr");
	}
	else
	{
		l_attr_name = it->second;
	}

	return l_attr_name;
}

// ======================================================================
// ConfigurationParser::extractGapAttr
// ======================================================================
std::string ConfigurationParser::extractGapAttr(ModeKeys_t acq_config)
{
	std::string l_attr_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_ATTR_GAP);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: GapAttr not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractGapAttr");
	}
	else
	{
		l_attr_name = it->second;
	}

	return l_attr_name;
}

// ======================================================================
// ConfigurationParser::extractKTable
// ======================================================================
std::string ConfigurationParser::extractKTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_K_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: K_Table not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractKTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractKxTable
// ======================================================================
std::string ConfigurationParser::extractKxTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_K_X_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: KxTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractKxTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractKzTable
// ======================================================================
std::string ConfigurationParser::extractKzTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_K_Z_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: KzTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractKzTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractOffsetTable
// ======================================================================
std::string ConfigurationParser::extractOffsetTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: OffsetTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractOffsetTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractBeamSizeTable
// ======================================================================
std::string ConfigurationParser::extractBeamSizeTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_BEAMSIZE_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: BeamSizeTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractBeamSizeTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractOffsetXTable
// ======================================================================
std::string ConfigurationParser::extractOffsetXTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_X_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: OffsetXTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractOffsetXTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

// ======================================================================
// ConfigurationParser::extractOffsetZTable
// ======================================================================
std::string ConfigurationParser::extractOffsetZTable(ModeKeys_t acq_config)
{
	std::string l_table_name = "";

	// get user key
	ModeKeys_it_t it = acq_config.find(kKEY_OFFSET_Z_TABLE);

	if (it == acq_config.end())
	{
		// key not found => exception
		yat::OSStream oss;
		oss << "Key: OffsetZTable not found";
		INFO_STREAM << oss.str() << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			oss.str().c_str(), 
			"ConfigurationParser::extractOffsetZTable");
	}
	else
	{
		l_table_name = it->second;
	}

	return l_table_name;
}

} // namespace Xbpm_ns


