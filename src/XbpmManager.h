//=============================================================================
// XbpmManager.h
//=============================================================================
// abstraction.......XbpmManager for Xbpm
// class.............XbpmManager
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_MANAGER_H
#define _XBPM_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "CurrentDataGrabber.h"
#include "PositionDataGrabber.h"
#include "NexusManager.h"
#include <yat4tango/DeviceTask.h>


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmManager
// ============================================================================
class XbpmManager :  public yat4tango::DeviceTask
{

public:

  //- constructor
  XbpmManager(Tango::DeviceImpl * hostDevice);

  //- destructor
  ~XbpmManager();

  //- gets xbpm manager state
  Tango::DevState get_state();

  //- gets xbpm manager status
  std::string get_status();

  //- updates state and status
  void update_state_status();

  //- init
  void init(XbpmConfig& cfg);

  //- starts the task
  void start();

  //- stops the task
  void stop();

  //- sets autorange
  void set_autorange(bool auto_range);

  //- gets autorange
  bool get_autorange();

  //- sets current unit
  void set_current_unit(double unit);

  //- gets voltage data
  Voltages_t get_voltage_data();

  //- gets current data
  Currents_t get_current_data();

  //- gets position data
  Position_t get_position_data();

  //- gets gain
  double get_gain();

  //- sets new computationMode
  void set_computation_mode(unsigned short mode);

  //- force data update (useful in scan mode)
  void force_update();

  //- nexus stuff
  //- reset buffer index
  void reset_nexus_buffer_index();

  //- initialization
  void init_nexus(NexusConfig nx_cfg);

  //- Sets new nexus file path
  void set_nexus_file_path(std::string path);
  //- Gets new nexus file path
  std::string get_nexus_file_path()
  {
    return m_nexus_cfg.nexusTargetPath;
  };

  //- Sets new nexus file generation flag
  void set_nexus_file_generation(bool enable);
  //- Gets new nexus file generation flag
  bool get_nexus_file_generation()
  {
    return m_nexus_cfg.nexusFileGeneration;
  };

  //- Sets new number of acquisition per nexus file
  void set_nexus_nb_per_file(yat::uint32 nb);
  //- Gets new number of acquisition per nexus file
  yat::uint32 get_nexus_nb_per_file()
  {
    return m_nexus_cfg.nexusNbPerFile;
  };

protected:

	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg);

private:

  //- intialization
  void init_i();
 
  //- periodic job
  void periodic_job_i();

  //- get device state
  Tango::DevState get_device_state();

  //- computes intensity alarms
  void compute_intensity_alarms();

  //- get raw SAI historics and extract data
  void get_sai_historics(bool& histo_reset);

  //- start internal
  void start_i();

  //- stop internal
  void stop_i();
	
  //- set nexus item list
  nxItemList_t build_nexus_item_list();

  //- configuration
  XbpmConfig m_cfg;
  
  //- dynamic attributes maanger
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- device state
  Tango::DevState m_state;

  //- device status
  std::string m_status;

  //- autorange
  bool m_autorange;
  
  //- current data grabber
  CurrentDataGrabber * m_current_data_grabber;

  //- position data grabber
  PositionDataGrabber * m_position_data_grabber;

  //- integration time
  double m_integrationTime;

  //- default polling period use flag
  bool m_useDefaultPP;

  //- autorange
  bool m_autoRange;

  //- voltage data
  Voltages_t m_voltage_data;

  //- current data
  Currents_t m_current_data;

  //- position data
  Position_t m_position;

  //- dynamic current data
  DynamicCurrentData_t m_dyn_current_data;

  //- dynamic position data
  DynamicPositionData_t m_dyn_position_data;

  //- gain
  double m_gain;

  //- unit factor
  double m_unit;

  //- computation mode
  unsigned short m_computation_mode;

  //- current device state storage
  Tango::DevState m_previousState;

  //- current historics management
  size_t m_histo_size;
  Currents_Histos_t m_sai_histos;

  //- to avoid race condition between XBPM class & periodic_job
  yat::Mutex m_lock;

  //- Nexus manager
  NexusManager * m_nexus_manager;

  //- nexus config
  NexusConfig m_nexus_cfg;

  //- nexus storage (for all possible items)
  NexusStorage m_nexus_storage;
};

} // namespace Xbpm_ns

#endif // _XBPM_MANAGER_H
