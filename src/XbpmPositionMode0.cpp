//=============================================================================
// XbpmPositionMode0.cpp
//=============================================================================
// abstraction.......XbpmPositionMode0 for XbpmPositionModes
// class.............XbpmPositionMode0
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode0.h"

namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode0::XbpmPositionMode0 ()
// ============================================================================ 
XbpmPositionMode0::XbpmPositionMode0(const XbpmConfig & cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager) 
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
	m_beam_energy_proxy_name = "";
	m_energy_attr = "";
	m_gap_attr = "";
	m_h_slit_gap_attr = "";
	m_h_slit_proxy_name = "";
	m_insertion_proxy_name = "";
	m_k_table = "";
	m_kx_table = "";
	m_kz_table = "";
	m_offset_table = "";
	m_offset_x_table = "";
	m_offset_z_table = "";
	m_phase_attr_name = "";
	m_v_slit_gap_attr = "";
	m_v_slit_proxy_name = "";
	m_state = Tango::UNKNOWN;
	m_status = "";
	m_coeff_grabber = NULL;
	m_kx = 0.0;
	m_kz = 0.0;
	m_offset_x = 0.0;
	m_offset_z = 0.0;
	m_internal_mode = MODE_UNKNOWN;
	m_is_dyn_k_table = false;
	m_is_dyn_kx_table = false;
	m_is_dyn_kz_table = false;
	m_is_dyn_offset_table = false;
	m_is_dyn_offset_x_table = false;
	m_is_dyn_offset_z_table = false;
}

// ============================================================================
// XbpmPositionMode0::~XbpmPositionMode0 ()
// ============================================================================ 
XbpmPositionMode0::~XbpmPositionMode0()
{
	// delete proxies
	if (m_coeff_grabber)
	{
		delete m_coeff_grabber;
		m_coeff_grabber = NULL;
	}
	if (m_is_dyn_k_table)
	{
		m_dyn_attr_manager->remove_attribute(K_TABLE);
	}
	if (m_is_dyn_kx_table)
	{
		m_dyn_attr_manager->remove_attribute(KX_TABLE);
	}
	if (m_is_dyn_kz_table)
	{
		m_dyn_attr_manager->remove_attribute(KZ_TABLE);
	}
	if (m_is_dyn_offset_table)
	{
		m_dyn_attr_manager->remove_attribute(OFFSET_TABLE);
	}
	if (m_is_dyn_offset_x_table)
	{
		m_dyn_attr_manager->remove_attribute(OFFSETX_TABLE);
	}
	if (m_is_dyn_offset_z_table)
	{
		m_dyn_attr_manager->remove_attribute(OFFSETZ_TABLE);
	}
}

// ============================================================================
// XbpmPositionMode0::init ()
// ============================================================================ 
void XbpmPositionMode0::init()
{
	if (m_coeff_grabber)
	{
		delete m_coeff_grabber;
		m_coeff_grabber	= NULL;
	}

	switch (m_internal_mode)
	{
	case MODE_ENERGY:
		{
			CoefficientGrabberEnergyConfig l_cge_cfg;
			l_cge_cfg.hostDevice = m_cfg.hostDevice;
			l_cge_cfg.m_beam_energy_proxy_name = m_beam_energy_proxy_name;
			l_cge_cfg.m_energy_attr_name = m_energy_attr;
			l_cge_cfg.m_k_table_path = m_k_table;
			l_cge_cfg.m_offset_table_path = m_offset_table;
			try
			{
				m_coeff_grabber = new CoefficientGrabberEnergy(l_cge_cfg);
			}
			catch (...)
			{
				ERROR_STREAM << "Problem creating the coefficientGrabberEnergy" << std::endl;
				m_state = Tango::FAULT;
				m_status = "Problem creating the coefficientGrabberEnergy";
			}
			break;
		}
	case MODE_SLIT:
		{
			CoefficientGrabberSlitConfig l_cgs_cfg;
			l_cgs_cfg.hostDevice = m_cfg.hostDevice;
			l_cgs_cfg.m_beam_energy_proxy_name = m_beam_energy_proxy_name;
			l_cgs_cfg.m_energy_attr_name = m_energy_attr;
			l_cgs_cfg.m_h_slit_proxy_name = m_h_slit_proxy_name;
			l_cgs_cfg.m_h_slit_gap_attr = m_h_slit_gap_attr;
			l_cgs_cfg.m_v_slit_proxy_name = m_v_slit_proxy_name;
			l_cgs_cfg.m_v_slit_gap_attr = m_v_slit_gap_attr;
			l_cgs_cfg.m_kx_table_path = m_kx_table;
			l_cgs_cfg.m_kz_table_path = m_kz_table;
			l_cgs_cfg.m_offset_table_path = m_offset_table;
			try
			{
				m_coeff_grabber = new CoefficientGrabberSlit(l_cgs_cfg);
			}
			catch (...)
			{
				ERROR_STREAM << "Problem creating the coefficientGrabberSlit" << std::endl;
				m_state = Tango::FAULT;
				m_status = "Problem creating the coefficientGrabberSlit";
			}
			break;
		}
	case MODE_INSERT:
		{
			CoefficientGrabberInsertConfig l_cgi_cfg;
			l_cgi_cfg.hostDevice = m_cfg.hostDevice;
			l_cgi_cfg.m_insertion_proxy_name = m_insertion_proxy_name;
			l_cgi_cfg.m_gap_attr_name = m_gap_attr;
			l_cgi_cfg.m_k_table_path = m_k_table;
			l_cgi_cfg.m_offset_table_path = m_offset_table;
			try
			{
				m_coeff_grabber = new CoefficientGrabberInsert(l_cgi_cfg);
			}
			catch (...)
			{
				ERROR_STREAM << "Problem creating the coefficientGrabberInsert" << std::endl;
				m_state = Tango::FAULT;
				m_status = "Problem creating the coefficientGrabberInsert";
			}
			break;
		}
	case MODE_INSERT_XZ:
		{
			CoefficientGrabberInsertXZConfig l_cgixz_cfg;
			l_cgixz_cfg.hostDevice = m_cfg.hostDevice;
			l_cgixz_cfg.m_insertion_proxy_name = m_insertion_proxy_name;
			l_cgixz_cfg.m_gap_attr_name = m_gap_attr;
			l_cgixz_cfg.m_phase_attr_name = m_phase_attr_name;
			l_cgixz_cfg.m_kx_table_path = m_kx_table;
			l_cgixz_cfg.m_kz_table_path = m_kz_table;
			l_cgixz_cfg.m_offsetx_table_path = m_offset_x_table;
			l_cgixz_cfg.m_offsetz_table_path = m_offset_z_table;
			try
			{
				m_coeff_grabber = new CoefficientGrabberInsertXZ(l_cgixz_cfg);
			}
			catch (...)
			{
				ERROR_STREAM << "Problem creating the coefficientGrabberInsertXZ" << std::endl;
				m_state = Tango::FAULT;
				m_status = "Problem creating the coefficientGrabberInsertXZ";
			}
			break;
		}
	default:
		ERROR_STREAM << "Unknown error with internal mode" << std::endl;
		m_state = Tango::FAULT;
		m_status = "Unknown error with internal mode";
		break;
	}

	if (!m_coeff_grabber)
	{		
		ERROR_STREAM << "Failed to access coefficient grabber" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
		"initialization failed - failed to access coefficient grabber", 
		"XbpmPositionMode0::init"); 
	}

	// coeff grabber initialization
	try
	{
		m_coeff_grabber->init();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_state = Tango::FAULT;
		m_status = "Position mode initialization failed";
		RETHROW_DEVFAILED(e, 
            "DEVICE_ERROR", 
			"initialization failed - failed to init coefficient grabber", 
			"XbpmPositionMode0::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Problem initializing the CoefficientGrabber" << std::endl;
		m_state = Tango::FAULT;
		m_status = "Position mode initialization failed";
		THROW_DEVFAILED("DEVICE_ERROR", 
            "initialization failed - failed to init coefficient grabber", 
            "XbpmPositionMode0::init"); 
	}

    // init OK
    m_state = Tango::ON;
}

// ============================================================================
// XbpmPositionMode0::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode0::get_state()
{
	if (m_coeff_grabber)
	{
		m_state =  m_coeff_grabber->get_state();
	}

	return m_state;
}

// ============================================================================
// XbpmPositionMode0::get_status ()
// ============================================================================ 
std::string XbpmPositionMode0::get_status()
{
	if (m_coeff_grabber)
	{
		m_status =  m_coeff_grabber->get_status();
	}
    else
    {
        m_status = "Unknown";
    }

	return m_status;
}

// ============================================================================
// XbpmPositionMode0::get_properties ()
// ============================================================================ 
void XbpmPositionMode0::get_properties()
{
	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode0"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);

	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	ModeKeys_t l_keys;
	if (!dev_prop[0].is_empty())
	{
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"Failed to parse Mode0 property", 
				"XbpmPositionMode0::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Failed to parse Mode0 property", 
				"XbpmPositionMode0::get_properties"); 
		}
		try
		{
			m_insertion_proxy_name = l_conf_parser.extractInsertionProxyName(l_keys);
		}
		catch (...)
		{
			INFO_STREAM << "No InsertionProxyName" << endl;
		}
		try
		{
			m_beam_energy_proxy_name = l_conf_parser.extractBeamEnergyProxyName(l_keys);
		}
		catch (...)
		{
			INFO_STREAM << "No BeamEnergyProxyName" << endl;
		}
	}
	else
	{
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			"The property Mode0 must be filled", 
			"XbpmPositionMode0::get_properties"); 
	}

	bool l_is_insertion = false;
	bool l_is_beam = false;
	if (!m_insertion_proxy_name.size() == 0)
	{
		l_is_insertion = true;
	}
	if (!m_beam_energy_proxy_name.size() == 0)
	{
		l_is_beam = true;
	}
	if ((l_is_beam && l_is_insertion && !m_cfg.enableFluxComputing) || 
      (!l_is_beam && !l_is_insertion))
	{
		THROW_DEVFAILED("CONFIGURATION_ERROR", 
			"You must either fill the InsertionProxyName or the BeamEnergyProxyName properties", 
			"XbpmPositionMode0::get_properties"); 
	}
	if (l_is_insertion)
	{
		try
		{
			m_gap_attr = l_conf_parser.extractGapAttr(l_keys);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"The property Gap must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"The property Gap or/and phase must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		m_internal_mode = MODE_INSERT;
		try
		{
			m_phase_attr_name = l_conf_parser.extractPhaseAttr(l_keys);
		}
		catch (...)
		{
			INFO_STREAM << "If you want the InsertXY mode, you must fill PhaseAttr property" << endl;
		}
		if (!(m_phase_attr_name.size() == 0))
		{
			m_internal_mode = MODE_INSERT_XZ;
		}
	}
	else
	{
		bool l_is_slit = false;
		try
		{
			m_h_slit_proxy_name = l_conf_parser.extractHSlitProxyName(l_keys);
			m_v_slit_proxy_name = l_conf_parser.extractVSlitProxyName(l_keys);
			m_internal_mode = MODE_SLIT;
			l_is_slit = true;
		}
		catch (...)
		{
			INFO_STREAM << "If you want the SLIT mode, you must fill HSlitProxyName and VSlitProxyName properties" << endl;
		}
		if (!l_is_slit)
		{
			try
			{
				m_k_table = l_conf_parser.extractKTable(l_keys);
			}
			catch (...)
			{
				m_is_dyn_k_table = true;
				//add dyn attr kTable
				yat4tango::DynamicAttributeInfo dai;
				dai.dev = m_cfg.hostDevice;
				dai.tai.name = K_TABLE;
				dai.tai.label = K_TABLE;
				//- describe the dyn attr we want...
				dai.tai.data_type = Tango::DEV_STRING;
				dai.tai.data_format = Tango::SCALAR;
				dai.tai.writable = Tango::READ_WRITE;
				dai.tai.disp_level = Tango::EXPERT;
				dai.tai.description = "Path and name of the K table";
				//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
				dai.cdb = false;
				//- read callback
				dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
					&XbpmPositionMode0::read_KTable);
				dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
					&XbpmPositionMode0::write_KTable);
				m_dyn_attr_manager->add_attribute(dai);

                try
                {
                    std::string prop_name = std::string("__") + K_TABLE;
                    m_k_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
                }
                catch(...)
                {
                    // no memorized value, set default value
                    m_k_table = kDEFAULT_FILE_NAME;
                }
			}
			m_internal_mode = MODE_ENERGY;
		}
	}

	DEBUG_STREAM << "Mode 0 internal mode : " << m_internal_mode << endl;
	switch (m_internal_mode)
	{
	case MODE_ENERGY:
		try
		{
			m_energy_attr = l_conf_parser.extractEnergyAttr(l_keys);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"The EnergyAttr property must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"The EnergyAttr property must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		try
		{
			m_offset_table = l_conf_parser.extractOffsetTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_offset_table = true;
			//add dyn attr offsetTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = OFFSET_TABLE;
			dai.tai.label = OFFSET_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Offset table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_OffsetTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_OffsetTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + OFFSET_TABLE;
                m_offset_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_offset_table = kDEFAULT_FILE_NAME;
            }
		}
		break;

	case MODE_SLIT:
		try
		{
			m_energy_attr = l_conf_parser.extractEnergyAttr(l_keys);
			m_h_slit_gap_attr = l_conf_parser.extractHSlitGapAttr(l_keys);
			m_v_slit_gap_attr = l_conf_parser.extractVSlitGapAttr(l_keys);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"The EnergyAttr, HSlitGapAttr and VSlitGapAttr properties must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"The EnergyAttr, HSlitGapAttr and VSlitGapAttr properties must be filled", 
				"XbpmPositionMode0::get_properties"); 
		}
		try
		{
			m_offset_table = l_conf_parser.extractOffsetTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_offset_table = true;
			//add dyn attr offsetTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = OFFSET_TABLE;
			dai.tai.label = OFFSET_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Offset table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_OffsetTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_OffsetTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + OFFSET_TABLE;
                m_offset_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_offset_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_kx_table= l_conf_parser.extractKxTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_kx_table = true;
			//add dyn attr kxTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = KX_TABLE;
			dai.tai.label = KX_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Kx table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_KxTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_KxTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + KX_TABLE;
                m_kx_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_kx_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_kz_table = l_conf_parser.extractKzTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_kz_table = true;
			//add dyn attr kzTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = KZ_TABLE;
			dai.tai.label = KZ_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Kz table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_KzTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_KzTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + KZ_TABLE;
                m_kz_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_kz_table = kDEFAULT_FILE_NAME;
            }
		}
		break;

	case MODE_INSERT:
		try
		{
			m_offset_table = l_conf_parser.extractOffsetTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_offset_table = true;
			//add dyn attr offsetTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = OFFSET_TABLE;
			dai.tai.label = OFFSET_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Offset table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_OffsetTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_OffsetTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + OFFSET_TABLE;
                m_offset_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_offset_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_k_table = l_conf_parser.extractKTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_k_table = true;
			//add dyn attr kTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = K_TABLE;
			dai.tai.label = K_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the K table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_KTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_KTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + K_TABLE;
                m_k_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_k_table = kDEFAULT_FILE_NAME;
            }
		}
		break;

	case MODE_INSERT_XZ:
		try
		{
			m_kx_table= l_conf_parser.extractKxTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_kx_table = true;
			//add dyn attr kxTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = KX_TABLE;
			dai.tai.label = KX_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Kx table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_KxTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_KxTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + KX_TABLE;
                m_kx_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_kx_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_kz_table = l_conf_parser.extractKzTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_kz_table = true;
			//add dyn attr kzTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = KZ_TABLE;
			dai.tai.label = KZ_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Kz table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_KzTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_KzTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + KZ_TABLE;
                m_kz_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_kz_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_offset_x_table = l_conf_parser.extractOffsetXTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_offset_x_table = true;
			//add dyn attr offsetXTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = OFFSETX_TABLE;
			dai.tai.label = OFFSETX_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Offset x table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_OffsetXTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_OffsetXTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + OFFSETX_TABLE;
                m_offset_x_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_offset_x_table = kDEFAULT_FILE_NAME;
            }
		}
		try
		{
			m_offset_z_table = l_conf_parser.extractOffsetZTable(l_keys);
		}
		catch (...)
		{
			m_is_dyn_offset_z_table = true;
			//add dyn attr offsetZTable
			yat4tango::DynamicAttributeInfo dai;
			dai.dev = m_cfg.hostDevice;
			dai.tai.name = OFFSETZ_TABLE;
			dai.tai.label = OFFSETZ_TABLE;
			//- describe the dyn attr we want...
			dai.tai.data_type = Tango::DEV_STRING;
			dai.tai.data_format = Tango::SCALAR;
			dai.tai.writable = Tango::READ_WRITE;
			dai.tai.disp_level = Tango::EXPERT;
			dai.tai.description = "Path and name of the Offset z table";
			//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
			dai.cdb = false;
			//- read callback
			dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&XbpmPositionMode0::read_OffsetZTable);
			dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&XbpmPositionMode0::write_OffsetZTable);
			m_dyn_attr_manager->add_attribute(dai);

            try
            {
                std::string prop_name = std::string("__") + OFFSETZ_TABLE;
                m_offset_z_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
            }
            catch(...)
            {
                // no memorized value, set default value
                m_offset_z_table = kDEFAULT_FILE_NAME;
            }
		}
		break;
	default:
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Bad internal position mode value", 
				"XbpmPositionMode0::get_properties"); 
		break;
	}
}

// ============================================================================
// XbpmPositionMode0::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode0::get_position(Currents_t p_currents)
{
	m_currents = p_currents;

	if (!m_coeff_grabber)
	{		
		ERROR_STREAM << "Failed to access coefficient grabber" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to access coefficient grabber", 
			"XbpmPositionMode0::get_position"); 
	}

    // get formula coeffs according to internal mode
	try
	{
		m_coeff_grabber->get_coefficients(m_kx, m_kz, m_offset_x, m_offset_z);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"get_position failed", 
			"XbpmPositionMode0::get_position"); 
	}
	catch (...)
	{
		ERROR_STREAM << "get_position failed" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"get_position failed", 
			"XbpmPositionMode0::get_position"); 
	}

    // compute position
	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode0::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode0::compute_x_position_i()
{
	// X = [ ((I0 + I3) - (I1 + I2)) / Isum ] * Kx_corrected_factor + OffsetX_corrected_factor
	m_position.x = (((m_currents.current1 + m_currents.current4) - (m_currents.current2 + m_currents.current3)) / m_currents.intensity)* m_kx + m_offset_x;
}

// ============================================================================
// XbpmPositionMode0::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode0::compute_z_position_i()
{
	// Z = [ ((I0 + I1) - (I3 + I2)) / Isum ] * Kz_corrected_factor + OffsetZ_corrected_factor
	m_position.z = (((m_currents.current1 + m_currents.current2) - (m_currents.current4 + m_currents.current3)) / m_currents.intensity) * m_kz + m_offset_z;
}

// ============================================================================
// XbpmPositionMode0::read_OffsetTable ()
// ============================================================================ 
void XbpmPositionMode0::read_OffsetTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __offset_table__;
	static std::string __offset_table_str__;

	__offset_table_str__ = m_offset_table;
	__offset_table__ = const_cast<char*>(__offset_table_str__.c_str());  
	cbd.tga->set_value(&__offset_table__);
}

// ============================================================================
// XbpmPositionMode0::write_OffsetTable ()
// ============================================================================ 
void XbpmPositionMode0::write_OffsetTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __offset_table__;
	cbd.tga->get_write_value(__offset_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing OffsetTable", 
			"XbpmPositionMode0::write_OffsetTable"); 
	}

	if (m_offset_table.compare(__offset_table__) == 0)
	{
		DEBUG_STREAM << "Same value for OffsetTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__offset_table__))
		{
			m_offset_table = __offset_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetTable"); 
			}

 			std::string prop_name = std::string("__") + OFFSET_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __offset_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_OffsetTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_OffsetTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode0::read_OffsetXTable ()
// ============================================================================ 
void XbpmPositionMode0::read_OffsetXTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __offsetx_table__;
	static std::string __offsetx_table_str__;

	__offsetx_table_str__ = m_offset_x_table;
	__offsetx_table__ = const_cast<char*>(__offsetx_table_str__.c_str());  
	cbd.tga->set_value(&__offsetx_table__);
}

// ============================================================================
// XbpmPositionMode0::write_OffsetXTable ()
// ============================================================================ 
void XbpmPositionMode0::write_OffsetXTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __offsetx_table__;
	cbd.tga->get_write_value(__offsetx_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state()== Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing OffsetTable", 
			"XbpmPositionMode0::write_OffsetXTable"); 
	}

	if (m_offset_x_table.compare(__offsetx_table__) == 0)
	{
		DEBUG_STREAM << "Same value for OffsetTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__offsetx_table__))
		{
			m_offset_x_table = __offsetx_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetXTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetXTable"); 
			}
 			std::string prop_name = std::string("__") + OFFSETX_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __offsetx_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_OffsetXTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_OffsetXTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode0::read_OffsetZTable ()
// ============================================================================ 
void XbpmPositionMode0::read_OffsetZTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __offsetz_table__;
	static std::string __offsetz_table_str__;

	__offsetz_table_str__ = m_offset_z_table;
	__offsetz_table__ = const_cast<char*>(__offsetz_table_str__.c_str());  
	cbd.tga->set_value(&__offsetz_table__);
}

// ============================================================================
// XbpmPositionMode0::write_OffsetZTable ()
// ============================================================================ 
void XbpmPositionMode0::write_OffsetZTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __offsetz_table__;
	cbd.tga->get_write_value(__offsetz_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing OffsetTable", 
			"XbpmPositionMode0::write_OffsetZTable"); 
	}

	if (m_offset_z_table.compare(__offsetz_table__) == 0)
	{
		DEBUG_STREAM << "Same value for OffsetTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__offsetz_table__))
		{
			m_offset_z_table = __offsetz_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetZTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_OffsetZTable"); 
			}
 			std::string prop_name = std::string("__") + OFFSETZ_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __offsetz_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_OffsetZTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_OffsetZTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode0::read_KTable ()
// ============================================================================ 
void XbpmPositionMode0::read_KTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __k_table__;
	static std::string __k_table_str__;

	__k_table_str__ = m_k_table;
	__k_table__ = const_cast<char*>(__k_table_str__.c_str());  
	cbd.tga->set_value(&__k_table__);
}

// ============================================================================
// XbpmPositionMode0::write_KTable ()
// ============================================================================ 
void XbpmPositionMode0::write_KTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __k_table__;
	cbd.tga->get_write_value(__k_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing KTable", 
			"XbpmPositionMode0::write_KTable"); 
	}

	if (m_k_table.compare(__k_table__) == 0)
	{
		DEBUG_STREAM << "Same value for KTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__k_table__))
		{
			m_k_table = __k_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KTable"); 
			}
 			std::string prop_name = std::string("__") + K_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __k_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_KTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_KTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode0::read_KxTable ()
// ============================================================================ 
void XbpmPositionMode0::read_KxTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __kx_table__;
	static std::string __kx_table_str__;

	__kx_table_str__ = m_kx_table;
	__kx_table__ = const_cast<char*>(__kx_table_str__.c_str());  
	cbd.tga->set_value(&__kx_table__);
}

// ============================================================================
// XbpmPositionMode0::write_KxTable ()
// ============================================================================ 
void XbpmPositionMode0::write_KxTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __kx_table__;
	cbd.tga->get_write_value(__kx_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing KxTable", 
			"XbpmPositionMode0::write_KxTable"); 
	}

	if (m_kx_table.compare(__kx_table__) == 0)
	{
		DEBUG_STREAM << "Same value for KxTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__kx_table__))
		{
			m_kx_table = __kx_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KxTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KxTable"); 
			}
 			std::string prop_name = std::string("__") + KX_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __kx_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_KxTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_KxTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode0::read_KzTable ()
// ============================================================================ 
void XbpmPositionMode0::read_KzTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __kz_table__;
	static std::string __kz_table_str__;

	__kz_table_str__ = m_kz_table;
	__kz_table__ = const_cast<char*>(__kz_table_str__.c_str());  
	cbd.tga->set_value(&__kz_table__);
}

// ============================================================================
// XbpmPositionMode0::write_KzTable ()
// ============================================================================ 
void XbpmPositionMode0::write_KzTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __kz_table__;
	cbd.tga->get_write_value(__kz_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing KzTable", 
			"XbpmPositionMode0::write_KzTable"); 
	}

	if (m_kz_table.compare(__kz_table__) == 0)
	{
		DEBUG_STREAM << "Same value for KzTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__kz_table__))
		{
			m_kz_table = __kz_table__;
			try
			{
				init();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
				RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KzTable"); 
			}
			catch (...)
			{
				ERROR_STREAM << "Init failed" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Init failed", 
					"XbpmPositionMode0::write_KzTable"); 
			}
 			std::string prop_name = std::string("__") + KZ_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __kz_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_KzTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode0::write_KzTable"); 
		}
	}
}
} // namespace Xbpm_ns
