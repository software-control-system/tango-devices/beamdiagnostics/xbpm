//=============================================================================
// XbpmTypesAndConsts.h
//=============================================================================
// abstraction.......XBPM 
// class.............XbpmTypesAndConsts
// original author.... S. GARA - NEXEYA
//=============================================================================

#ifndef _XBPM_TYPES_AND_CONSTS_H_
#define _XBPM_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
/*Use Interpolator library*/
#include "Table1D.h"
#include "Table2D.h"

#include <sstream>
#include <map>

/*Use YAT library*/
#include <yat/memory/DataBuffer.h>
#include <yat/any/Any.h>
#include <yat4tango/ExceptionHelper.h>

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace Xbpm_ns
{
// Max number of modes for SOLEIL XBPM
#define XBPM_MAX_NUM_MODES 8

//- MIN & MAX values for electrometer range
const double MIN_RANGE_VALUE = 0.0001;
const double MAX_RANGE_VALUE = 1000.;

//- Number of gain values in correction table
#define NB_ELTS_IN_GAIN_CORRECTION_TABLE 8

//- names for dynamic attributes
#define H_POS               "horizontalPosition"
#define V_POS               "verticalPosition"
#define H_POS_HIS           "historyPositionH"
#define V_POS_HIS           "historyPositionV"
#define H_POS_STD_DEV       "standardDeviationPositionH"
#define V_POS_STD_DEV       "standardDeviationPositionV"
#define FLUX                "flux"
#define INTENSITY           "intensity"
#define INTENSITY_HIS       "intensityHistory"
#define INTENSITY_STD_DEV   "standardDeviationIntensity"
#define CURRENT_SPEC_PRE    "current"
#define QUADRANT_SPEC_PRE   "quadrant"
#define CURRENT_SPEC_POST   "Spectrum"
#define H_POS_MOD_34        "verticalPosition1"
#define H_POS_MOD_5         "xPos"
#define V_POS_MOD_34        "verticalPosition2"
#define V_POS_MOD_5         "zPos"
#define RESPONSIVITY_TABLE  "responsivityTable"
#define COMPUTATION_MODE    "computationMode"
#define CORR_TABLE          "correctionTable"
#define ZCORR_TABLE         "zCorrectionTable"
#define K_TABLE             "kTable"
#define KX_TABLE            "kxTable"
#define KZ_TABLE            "kzTable"
#define OFFSET_TABLE        "offsetTable"
#define OFFSETX_TABLE       "offsetXTable"
#define OFFSETZ_TABLE       "offsetZTable"
#define BEAMSIZE_TABLE      "beamSizeTable"

//- Constants
#define ELECTRON_CHARGE 1.6e-19
#define FLUX_CST 1e-6

//- constant values for 1D & 2D tables reading (Interpolator lib)
const int INDEX_ENERGY_IN_FILE_1D = 0;
const int INDEX_GAP_IN_FILE_1D = 0;
const int INDEX_GAIN_IN_FILE_1D = 0;
const int INDEX_RESPONSIVITY_IN_FILE_1D = 1;
const int INDEX_OFFSETX_IN_FILE_1D = 1;
const int INDEX_OFFSETZ_IN_FILE_1D = 2;
const int INDEX_COEFFX_IN_FILE_1D = 1;
const int INDEX_COEFFZ_IN_FILE_1D = 2;
const int INDEX_BEAMX_IN_FILE_1D = 1;
const int INDEX_BEAMZ_IN_FILE_1D = 2;
#define TABLE_1D_INTERPOLATION "Linear"
#define TABLE_2D_INTERPOLATION "Bilinear"

// default values
#define kDEFAULT_FILE_NAME "./FileToBeDefined.txt"

//- constant values for nexus data types
#define NX_POSITION "POSITION"
#define NX_INTENSITY "INTENSITY"
#define NX_FLUX "FLUX"
#define NX_POSITION_HISTORY "POSITION_HIST"
#define NX_INTENSITY_HISTORY "INTENSITY_HIST"

//- Mode 0 & 2 - internal modes
typedef enum
{
  MODE_UNKNOWN = -1,
  MODE_ENERGY = 0,
  MODE_SLIT = 1,
  MODE_INSERT = 2,
  MODE_INSERT_XZ = 3
} INTERNAL_MODE;


// ============================================================================
// MODE CONFIG - DATA TYPEs
// ============================================================================
//- List of <KEY, value> defining the computation mode configuration
typedef std::pair<const std::string, std::string> ModeKey_pair_t;
typedef std::map<std::string, std::string> ModeKeys_t;
typedef ModeKeys_t::iterator ModeKeys_it_t;


// ============================================================================
// Position_t:  struct for position values
// ============================================================================
typedef struct Position_t
{
  //- members
  //- x
  double x;
  //- z
  double z;

  //- default constructor -----------------------
  Position_t ()
  : x(0.0),
    z(0.0)
  {
  }

  //- destructor -----------------------
  ~Position_t ()
  {
  }

  //- copy constructor ------------------
  Position_t (const Position_t& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const Position_t & operator= (const Position_t& src)
  {
    if (this == & src) 
      return *this;

    x = src.x;
    z = src.z;

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "Position_t::x........." 
		<< x
		<< std::endl; 
    std::cout << "Position_t::z........." 
		<< z
		<< std::endl; 
  }
} Position_t;

// ============================================================================
// Voltages_t:  struct for voltages values
// ============================================================================
typedef struct Voltages_t
{
  //- members
  //- voltage 1
  double voltage1;
  //- voltage 2
  double voltage2;
  //- voltage 3
  double voltage3;
  //- voltage 4
  double voltage4;
  //- voltage in alarms
  bool voltageInAlarm;

  //- default constructor -----------------------
  Voltages_t ()
    : voltage1(0.0),
      voltage2(0.0),
      voltage3(0.0),
      voltage4(0.0),
      voltageInAlarm(false)
  {
  }

  //- destructor -----------------------
  ~Voltages_t ()
  {
  }

  //- copy constructor ------------------
  Voltages_t (const Voltages_t& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const Voltages_t & operator= (const Voltages_t& src)
  {
    if (this == & src) 
      return *this;

	  voltage1 = src.voltage1;
	  voltage2 = src.voltage2;
	  voltage3 = src.voltage3;
	  voltage4 = src.voltage4;
	  voltageInAlarm = src.voltageInAlarm; 

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "Voltages_t::voltage1........." 
        << voltage1
			  << std::endl; 
    std::cout << "Voltages_t::voltage2........." 
			  << voltage2
			  << std::endl; 
    std::cout << "Voltages_t::voltage3........." 
		    << voltage3
			  << std::endl; 
    std::cout << "Voltages_t::voltage4........." 
			  << voltage4
			  << std::endl; 
    std::cout << "Voltages_t::voltageInAlarm........." 
        << voltageInAlarm
        << std::endl; 
  }
} Voltages_t;

// ============================================================================
// Currents_t:  struct for currents values
// ============================================================================
typedef struct Currents_t
{
  //- members
  //- current 1
  double current1;
  //- current 2
  double current2;
  //- current 3
  double current3;
  //- current 4
  double current4;
  //- intensity
  double intensity;
  //- intensity in alarm
  bool intensityInAlarm;

  //- default constructor -----------------------
  Currents_t ()
  : current1(0.0),
	current2(0.0),
	current3(0.0),
	current4(0.0),
	intensity(0.0),
	intensityInAlarm(false)      
  {
  }

  //- destructor -----------------------
  ~Currents_t ()
  {
  }

  //- copy constructor ------------------
  Currents_t (const Currents_t& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const Currents_t & operator= (const Currents_t& src)
  {
    if (this == & src) 
      return *this;

	  current1 = src.current1;
	  current2 = src.current2;
	  current3 = src.current3;
	  current4 = src.current4;
      intensity = src.intensity;
	  intensityInAlarm = src.intensityInAlarm;

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "Currents_t::current1........." 
        << current1
			  << std::endl; 
    std::cout << "Currents_t::current2........." 
			  << current2
			  << std::endl; 
    std::cout << "Currents_t::current3........." 
			  << current3
			  << std::endl; 
    std::cout << "Currents_t::current4........." 
			  << current4
			  << std::endl; 
    std::cout << "Currents_t::intensity........." 
			  << intensity
			  << std::endl; 
    std::cout << "Currents_t::intensityInAlarm........." 
			  << intensityInAlarm
			  << std::endl; 
  }
} Currents_t;

// ============================================================================
// Histos_t:  struct for current historics
// ============================================================================
typedef struct Currents_Histos_t
{
  //- members
  // current historics for each channel
  yat::Buffer<double> histo0;
  yat::Buffer<double> histo1;
  yat::Buffer<double> histo2;
  yat::Buffer<double> histo3;

  //- default constructor -----------------------
  Currents_Histos_t()
  {
    histo0.clear();
    histo0.capacity(0);
    histo0.force_length(0);
    histo1.clear();
    histo1.capacity(0);
    histo1.force_length(0);
    histo2.clear();
    histo2.capacity(0);
    histo2.force_length(0);
    histo3.clear();
    histo3.capacity(0);
    histo3.force_length(0);
  }

  //- destructor -----------------------
  ~Currents_Histos_t()
  {
    histo0.clear();
    histo0.capacity(0);
    histo0.force_length(0);
    histo1.clear();
    histo1.capacity(0);
    histo1.force_length(0);
    histo2.clear();
    histo2.capacity(0);
    histo2.force_length(0);
    histo3.clear();
    histo3.capacity(0);
    histo3.force_length(0);
  }

  //- copy constructor ------------------
  Currents_Histos_t(const Currents_Histos_t& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const Currents_Histos_t & operator= (const Currents_Histos_t& src)
  {
    if (this == &src)
      return *this;

    histo0 = src.histo0;
    histo1 = src.histo1;
    histo2 = src.histo2;
    histo3 = src.histo3;

    return *this;
  }

  void clear_all()
  {
      histo0.clear();
      histo0.capacity(0);
      histo0.force_length(0);
      histo1.clear();
      histo1.capacity(0);
      histo1.force_length(0);
      histo2.clear();
      histo2.capacity(0);
      histo2.force_length(0);
      histo3.clear();
      histo3.capacity(0);
      histo3.force_length(0);
  }
} Currents_Histos_t;

//- types for easy history
//- Channel id type
typedef yat::uint16 ChannelId_t;
typedef yat::Buffer<double> History_t;
typedef yat::Buffer<double> CurrentsBuffer_t;

// ============================================================================
// DynamicCurrentData_t:  struct for dynamic current data values
// ============================================================================
typedef struct DynamicCurrentData_t
{
	//- members
	//- current buffer for all channels
	std::map<ChannelId_t,CurrentsBuffer_t> currents_buffer;
	//- intensity std deviation
	std::map<ChannelId_t, double> intensity_std_deviation;
	//- flux
	double flux;

	//- default constructor -----------------------
  DynamicCurrentData_t ()
  {
  }

  //- destructor -----------------------
  ~DynamicCurrentData_t ()
  {
	  currents_buffer.clear();
	  intensity_std_deviation.clear();
  }

  //- copy constructor ------------------
  DynamicCurrentData_t (const DynamicCurrentData_t& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const DynamicCurrentData_t & operator= (const DynamicCurrentData_t& src)
  {
    if (this == & src) 
      return *this;

    currents_buffer = src.currents_buffer;
	  flux = src.flux;

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {
	  std::cout << "DynamicCurrentData_t::flux........." 
		  << flux
		  << std::endl; 
  }
} DynamicCurrentData_t;

// ============================================================================
// DynamicpositionData_t:  struct for dynamic position data values
// ============================================================================
typedef struct DynamicPositionData_t
{
	//- members
	//- position std dev H
	double position_std_dev_h;
	//- position std dev V
	double position_std_dev_v;


	//- default constructor -----------------------
	DynamicPositionData_t ()
	{
	}

	//- destructor -----------------------
	~DynamicPositionData_t ()
	{
	}

	//- copy constructor ------------------
	DynamicPositionData_t (const DynamicPositionData_t& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const DynamicPositionData_t & operator= (const DynamicPositionData_t& src)
	{
		if (this == & src) 
			return *this;

		position_std_dev_h = src.position_std_dev_h;
		position_std_dev_v = src.position_std_dev_v;

		return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout << "DynamicPositionData_t::position_std_dev_h........." 
			<< position_std_dev_h
			<< std::endl; 
		std::cout << "DynamicPositionData_t::position_std_dev_v........." 
			<< position_std_dev_v
			<< std::endl; 
	}
} DynamicPositionData_t;

// ============================================================================
// CoefficientGrabberEnergyConfig:  struct for CoefficientGrabberEnergy configuration
// ============================================================================
typedef struct CoefficientGrabberEnergyConfig
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;
	//- k_table path
	std::string m_k_table_path;
	//- beam energy proxy name
	std::string m_beam_energy_proxy_name;
	//- energy attr name
	std::string m_energy_attr_name;
	//- offset table path
	std::string m_offset_table_path;


	//- default constructor -----------------------
	CoefficientGrabberEnergyConfig ()
	{

	}

	//- destructor -----------------------
	~CoefficientGrabberEnergyConfig ()
	{
	
	}

	//- copy constructor ------------------
	CoefficientGrabberEnergyConfig (const CoefficientGrabberEnergyConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const CoefficientGrabberEnergyConfig & operator= (const CoefficientGrabberEnergyConfig& src)
	{
		if (this == & src) 
			return *this;
		
		hostDevice = src.hostDevice;
		m_beam_energy_proxy_name = src.m_beam_energy_proxy_name;
		m_energy_attr_name = src.m_energy_attr_name;
		m_k_table_path = src.m_k_table_path;
		m_offset_table_path = src.m_offset_table_path;

		return *this;
	}


	//- dump -----------------------
	void dump () const
	{
		std::cout << "CoefficientGrabberEnergyConfig::m_beam_energy_proxy_name........." 
			<< m_beam_energy_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_energy_attr_name........." 
			<< m_energy_attr_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_k_table_path........." 
			<< m_k_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_offset_table_path........." 
			<< m_offset_table_path
			<< std::endl; 
	}

} CoefficientGrabberEnergyConfig;

// ============================================================================
// CoefficientGrabberSlitConfig:  struct for CoefficientGrabberSlitConfig configuration
// ============================================================================
typedef struct CoefficientGrabberSlitConfig
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;
	//- beam energy proxy name
	std::string m_beam_energy_proxy_name;
	//- energy attr name
	std::string m_energy_attr_name;
	//- h slit proxy name
	std::string m_h_slit_proxy_name;
	//- Hslit gap attr
	std::string m_h_slit_gap_attr;
	//- v slit proxy name
	std::string m_v_slit_proxy_name;
	//- Vslit gap attr
	std::string m_v_slit_gap_attr;
	//- kx_table path
	std::string m_kx_table_path;
	//- kz_table path
	std::string m_kz_table_path;
	//- offset table path
	std::string m_offset_table_path;


	//- default constructor -----------------------
	CoefficientGrabberSlitConfig ()
	{

	}

	//- destructor -----------------------
	~CoefficientGrabberSlitConfig ()
	{
	
	}

	//- copy constructor ------------------
	CoefficientGrabberSlitConfig (const CoefficientGrabberSlitConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const CoefficientGrabberSlitConfig & operator= (const CoefficientGrabberSlitConfig& src)
	{
		if (this == & src) 
			return *this;
		
		hostDevice = src.hostDevice;
		m_beam_energy_proxy_name = src.m_beam_energy_proxy_name;
		m_energy_attr_name = src.m_energy_attr_name;
		m_h_slit_proxy_name = src.m_h_slit_proxy_name;
		m_h_slit_gap_attr = src.m_h_slit_gap_attr;
		m_v_slit_proxy_name = src.m_v_slit_proxy_name;
		m_v_slit_gap_attr = src.m_v_slit_gap_attr;
		m_kx_table_path = src.m_kx_table_path;
		m_kz_table_path = src.m_kz_table_path;
		m_offset_table_path = src.m_offset_table_path;

		return *this;
	}


	//- dump -----------------------
	void dump () const
	{
		std::cout << "CoefficientGrabberEnergyConfig::m_beam_energy_proxy_name........." 
			<< m_beam_energy_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_energy_attr_name........." 
			<< m_energy_attr_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_h_slit_proxy_name........." 
			<< m_h_slit_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_h_slit_gap_attr........." 
			<< m_h_slit_gap_attr
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_v_slit_proxy_name........." 
			<< m_v_slit_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_v_slit_gap_attr........." 
			<< m_v_slit_gap_attr
			<< std::endl;  
		std::cout << "CoefficientGrabberEnergyConfig::m_kx_table_path........." 
			<< m_kx_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_kz_table_path........." 
			<< m_kz_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberEnergyConfig::m_offset_table_path........." 
			<< m_offset_table_path
			<< std::endl; 
	}

} CoefficientGrabberSlitConfig;

// ============================================================================
// CoefficientGrabberInsertConfig:  struct for CoefficientGrabberInsert configuration
// ============================================================================
typedef struct CoefficientGrabberInsertConfig
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;
	//-insertion proxy name
	std::string m_insertion_proxy_name;
	//- gap attr name
	std::string m_gap_attr_name;
	//- offset table path
	std::string m_offset_table_path;
	//- k_table path
	std::string m_k_table_path;


	//- default constructor -----------------------
	CoefficientGrabberInsertConfig ()
	{

	}

	//- destructor -----------------------
	~CoefficientGrabberInsertConfig ()
	{

	}

	//- copy constructor ------------------
	CoefficientGrabberInsertConfig (const CoefficientGrabberInsertConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const CoefficientGrabberInsertConfig & operator= (const CoefficientGrabberInsertConfig& src)
	{
		if (this == & src) 
			return *this;

		hostDevice = src.hostDevice;
		m_insertion_proxy_name = src.m_insertion_proxy_name;
		m_gap_attr_name = src.m_gap_attr_name;
		m_k_table_path = src.m_k_table_path;
		m_offset_table_path = src.m_offset_table_path;

		return *this;
	}


	//- dump -----------------------
	void dump () const
	{
		std::cout << "CoefficientGrabberInsertConfig::m_insertion_proxy_name........." 
			<< m_insertion_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_gap_attr_name........." 
			<< m_gap_attr_name
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_k_table_path........." 
			<< m_k_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_offset_table_path........." 
			<< m_offset_table_path
			<< std::endl; 
	}

} CoefficientGrabberInsertConfig;

// ============================================================================
// CoefficientGrabberInsertXZConfig:  struct for CoefficientGrabberInsertXZ configuration
// ============================================================================
typedef struct CoefficientGrabberInsertXZConfig
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;
	//-insertion proxy name
	std::string m_insertion_proxy_name;
	//- gap attr name
	std::string m_gap_attr_name;
	//- phase attr name
	std::string m_phase_attr_name;
	//- offsetx table path
	std::string m_offsetx_table_path;
	//- offsetz table path
	std::string m_offsetz_table_path;
	//- kx_table path
	std::string m_kx_table_path;
	//- kz_table path
	std::string m_kz_table_path;


	//- default constructor -----------------------
	CoefficientGrabberInsertXZConfig ()
	{

	}

	//- destructor -----------------------
	~CoefficientGrabberInsertXZConfig ()
	{

	}

	//- copy constructor ------------------
	CoefficientGrabberInsertXZConfig (const CoefficientGrabberInsertXZConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const CoefficientGrabberInsertXZConfig & operator= (const CoefficientGrabberInsertXZConfig& src)
	{
		if (this == & src) 
			return *this;

		hostDevice = src.hostDevice;
		m_insertion_proxy_name = src.m_insertion_proxy_name;
		m_gap_attr_name = src.m_gap_attr_name;
		m_phase_attr_name = src.m_phase_attr_name;
		m_kx_table_path = src.m_kx_table_path;
		m_kz_table_path = src.m_kz_table_path;
		m_offsetx_table_path = src.m_offsetx_table_path;
		m_offsetz_table_path = src.m_offsetz_table_path;

		return *this;
	}


	//- dump -----------------------
	void dump () const
	{
		std::cout << "CoefficientGrabberInsertConfig::m_insertion_proxy_name........." 
			<< m_insertion_proxy_name
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_gap_attr_name........." 
			<< m_gap_attr_name
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_phase_attr_name........." 
			<< m_phase_attr_name
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_kx_table_path........." 
			<< m_kx_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_kz_table_path........." 
			<< m_kz_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_offsetx_table_path........." 
			<< m_offsetx_table_path
			<< std::endl; 
		std::cout << "CoefficientGrabberInsertConfig::m_offsetz_table_path........." 
			<< m_offsetz_table_path
			<< std::endl; 
	}

} CoefficientGrabberInsertXZConfig;

// ============================================================================
// XBPM POSITION MODE
// ============================================================================
typedef enum
{
  XBPM_MODE_0    = 0x0,
  XBPM_MODE_1    = 0x1,
  XBPM_MODE_2    = 0x2,
  XBPM_MODE_3    = 0x3,
  XBPM_MODE_4    = 0x4,
  XBPM_MODE_5    = 0x5,
  XBPM_MODE_6    = 0x6,
  XBPM_MODE_7    = 0x7
} XbpmMode_t;

// ============================================================================
// XBPM TYPE
// ============================================================================
typedef enum
{
  TYPE_UNKNOWN,
  TYPE_PSD,    
  TYPE_QUADRANT
} XbpmType_t;

#define XBPM_TYPE_PSD "PSD"
#define XBPM_TYPE_QUADRANT "Quadrant"


// ============================================================================
// XbpmConfig:  struct containing the Xbpm configuration
// ============================================================================
typedef struct XbpmConfig
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;
	//- electrometer gain attribute name
	std::string electrometerGainName;
	//- electrometer device proxy name
	std::string electrometerProxyName;
	//- enables the flux computing
	bool enableFluxComputing;
	//- enables the intensity history
	bool enableIntensityHistory;
	//- enables the position history
	bool enablePositionHistory;
	//- enables the sigma intensity calculation
	bool enableSigmaIntensity;
	//- Gain correction (in uA/V) for electrode 1.
	double gI1;
	//- Gain correction (in uA/V) for electrode 2.
	double gI2;
	//- Gain correction (in uA/V) for electrode 3.
	double gI3;
	//- Gain correction (in uA/V) for electrode 4.
	double gI4;
	//- Current offset (in uA) on electrode 1.
	double i1Offset;
	//- Current offset (in uA) on electrode 2.
	double i2Offset;
	//- Current offset (in uA) on electrode 3.
	double i3Offset;
	//- Current offset (in uA) on electrode 4.
	double i4Offset;
	//- Current out of which the Measured Intensity has a meaning (in micro-A).
	double intensityThreshold;
	//- If the input voltage of the ADC exceed this threshold (in V), the corresponding average current attribute become ALARM (i.e. |Vmes| > threshold).
	double highVoltageThreshold;
	//- If the input voltage of the ADC decrease this threshold (in V), the corresponding average current attribute become ALARM (i.e. if |Vmes| < threshold).
	double lowVoltageThreshold;
	//- Path and name of the responsivity table for the flux computing (E -> responsivity). 
	std::string responsivityTable;
	//- sai controller device proxy name
	std::string saiControllerProxyName;
	//- Name of the SaiController channel 1 attribute
	std::string saiControllerChan0Name;
	//- Name of the SaiController channel 2 attribute
	std::string saiControllerChan1Name;
	//- Name of the SaiController channel 3 attribute
	std::string saiControllerChan2Name;
	//- Name of the SaiController channel 4 attribute
	std::string saiControllerChan3Name;
	//- Voltage offset (in V) on electrode 1.
	double v1Offset;
	//- Voltage offset (in V) on electrode 2.
	double v2Offset;
	//- Voltage offset (in V) on electrode 3.
	double v3Offset;
	//- Voltage offset (in V) on electrode 4.
	double v4Offset;
	//- Beam position computation mode.
	unsigned short xBPMmode;
	//- beam energy attribute name
	std::string energyAttrName;
	//- beam energy device proxy name
	std::string beamEnergyProxyName;
	//- Launch start command at initialization time.
	bool startAtInit;
    //- Scan mode enabled
    bool scanMode;
    // XBPM equipment type
    XbpmType_t xbpmType;
    //- User polling period
    double userPollingPeriod;

  //- default constructor -----------------------
  XbpmConfig ()
	  : hostDevice(NULL),
	  electrometerGainName(""),
	  electrometerProxyName(""),
	  enableFluxComputing(false),
	  enableIntensityHistory(false),
	  enablePositionHistory(false),
	  enableSigmaIntensity(false),
	  gI1(0.0),
	  gI2(0.0),
	  gI3(0.0),
	  gI4(0.0),
	  i1Offset(0.0),
	  i2Offset(0.0),
	  i3Offset(0.0),
	  i4Offset(0.0),
	  intensityThreshold(0.0),
	  highVoltageThreshold(0.0),
	  lowVoltageThreshold(0.0),
	  responsivityTable(""),
	  saiControllerProxyName(""),
	  saiControllerChan0Name(""),
	  saiControllerChan1Name(""),
	  saiControllerChan2Name(""),
	  saiControllerChan3Name(""),
	  v1Offset(0.0),
	  v2Offset(0.0),
	  v3Offset(0.0),
	  v4Offset(0.0),
	  xBPMmode(0),
	  energyAttrName(""),
	  beamEnergyProxyName(""),
	  startAtInit(false),
      scanMode(false),
      xbpmType(TYPE_UNKNOWN),
      userPollingPeriod(0.0)
  {
  }

  //- destructor -----------------------
  ~XbpmConfig ()
  {
  }

  //- copy constructor ------------------
  XbpmConfig (const XbpmConfig& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const XbpmConfig & operator= (const XbpmConfig& src)
  {
    if (this == & src) 
      return *this;

    beamEnergyProxyName = src.beamEnergyProxyName;
    electrometerGainName = src.electrometerGainName;
    electrometerProxyName = src.electrometerProxyName;
    enableFluxComputing = src.enableFluxComputing;
    enableIntensityHistory = src.enableIntensityHistory;
    enablePositionHistory = src.enablePositionHistory;
    enableSigmaIntensity = src.enableSigmaIntensity;
    energyAttrName = src.energyAttrName;
    gI1 = src.gI1;
    gI2 = src.gI2;
    gI3 = src.gI3;
    gI4 = src.gI4;
    highVoltageThreshold = src.highVoltageThreshold;
    hostDevice = src.hostDevice;
    i1Offset = src.i1Offset;
    i2Offset = src.i2Offset;
    i3Offset = src.i3Offset;
    i4Offset = src.i4Offset;
    intensityThreshold = src.intensityThreshold;
    lowVoltageThreshold = src.lowVoltageThreshold;
    responsivityTable = src.responsivityTable;
    saiControllerChan0Name = src.saiControllerChan0Name;
    saiControllerChan1Name = src.saiControllerChan1Name;
    saiControllerChan2Name = src.saiControllerChan2Name;
    saiControllerChan3Name = src.saiControllerChan3Name;
    saiControllerProxyName = src.saiControllerProxyName;
    startAtInit = src.startAtInit;
    v1Offset = src.v1Offset;
    v2Offset = src.v2Offset;
    v3Offset = src.v3Offset;
    v4Offset = src.v4Offset;
    xBPMmode = src.xBPMmode;
    scanMode = src.scanMode;
    xbpmType = src.xbpmType;
    userPollingPeriod = src.userPollingPeriod;

    return *this;
  }

  //- dump -----------------------
  void dump () const
  {
    std::cout << "XbpmConfig::beamEnergyProxyName........." 
      << beamEnergyProxyName
      << std::endl; 
	  std::cout << "XbpmConfig::electrometerGainName........." 
		  << electrometerGainName
		  << std::endl; 
	  std::cout << "XbpmConfig::electrometerProxyName........." 
		  << electrometerProxyName
		  << std::endl; 
	  std::cout << "XbpmConfig::enableFluxComputing........." 
		  << enableFluxComputing
		  << std::endl; 
	  std::cout << "XbpmConfig::enableIntensityHistory........." 
		  << enableIntensityHistory
		  << std::endl; 
	  std::cout << "XbpmConfig::enablePositionHistory........." 
		  << enablePositionHistory
		  << std::endl;
	  std::cout << "XbpmConfig::enableSigmaIntensity........." 
		  << enableSigmaIntensity
		  << std::endl; 
	  std::cout << "XbpmConfig::energyAttrName........." 
		  << energyAttrName
		  << std::endl; 
	  std::cout << "XbpmConfig::gI1........." 
		  << gI1
		  << std::endl; 
	  std::cout << "XbpmConfig::gI2........." 
		  << gI2
		  << std::endl; 
	  std::cout << "XbpmConfig::gI3........." 
		  << gI3
		  << std::endl; 
	  std::cout << "XbpmConfig::gI4........." 
		  << gI4
		  << std::endl;
	  std::cout << "XbpmConfig::highVoltageThreshold........." 
		  << highVoltageThreshold
		  << std::endl;  
	  std::cout << "XbpmConfig::lowVoltageThreshold........." 
		  << lowVoltageThreshold
		  << std::endl;    
	  std::cout << "XbpmConfig::hostDevice........." 
		  << hostDevice
		  << std::endl;     
	  std::cout << "XbpmConfig::i1Offset........." 
		  << i1Offset
		  << std::endl;      
	  std::cout << "XbpmConfig::i2Offset........." 
		  << i2Offset
		  << std::endl;      
	  std::cout << "XbpmConfig::i3Offset........." 
		  << i3Offset
		  << std::endl;      
	  std::cout << "XbpmConfig::i4Offset........." 
		  << i4Offset
		  << std::endl;        
	  std::cout << "XbpmConfig::intensityThreshold........." 
		  << intensityThreshold
		  << std::endl;         
	  std::cout << "XbpmConfig::responsivity_table........." 
		  << responsivityTable
		  << std::endl;         
	  std::cout << "XbpmConfig::saiControllerChan0Name........." 
		  << saiControllerChan0Name
		  << std::endl;          
	  std::cout << "XbpmConfig::saiControllerChan1Name........." 
		  << saiControllerChan1Name
		  << std::endl;          
	  std::cout << "XbpmConfig::saiControllerChan2Name........." 
		  << saiControllerChan2Name
		  << std::endl;          
	  std::cout << "XbpmConfig::saiControllerChan3Name........." 
		  << saiControllerChan3Name
		  << std::endl;          
	  std::cout << "XbpmConfig::saiControllerProxyName........." 
		  << saiControllerProxyName
		  << std::endl;          
	  std::cout << "XbpmConfig::startAtInit........." 
		  << startAtInit
		  << std::endl;           
	  std::cout << "XbpmConfig::v1Offset........." 
		  << v1Offset
		  << std::endl;          
	  std::cout << "XbpmConfig::v2Offset........." 
		  << v2Offset
		  << std::endl;          
	  std::cout << "XbpmConfig::v3Offset........." 
		  << v3Offset
		  << std::endl;          
	  std::cout << "XbpmConfig::v4Offset........." 
		  << v4Offset
		  << std::endl;          
	  std::cout << "XbpmConfig::xBPMmode........." 
		  << xBPMmode
		  << std::endl; 
	  std::cout << "XbpmConfig::scanMode........." 
      << scanMode
		  << std::endl; 
	  std::cout << "XbpmConfig::xbpmType........." 
      << xbpmType
		  << std::endl; 
      std::cout << "XbpmConfig::userPollingPeriod........."
          << userPollingPeriod
          << std::endl;
  }
      
} XbpmConfig;

// ============================================================================
// computes standard deviation:
//  p_buff: data buffer
//  average value of the data buffer
// ============================================================================
double std_dev_calculation(yat::Buffer<double> p_buff, double l_val);

// ============================================================================
// tells if a file is readable:
//  file: the file to test
// ============================================================================
bool is_file_readable( const std::string & file );

// ============================================================================
// Get stored value in Device property
// ============================================================================
template <class T>
T get_value_as_property(Tango::DeviceImpl* dev_p, const std::string& property_name)
{
  if (!Tango::Util::instance()->_UseDb)
  {
    THROW_DEVFAILED(
	      "DEVICE_ERROR", 
	      "NO DB", 
	      "get_value_as_property"); 
  } 

  T value;
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum(property_name));

	//-	Call database and extract values
	//--------------------------------------------
  try
  {
    dev_p->get_db_device()->get_property(dev_prop);
  }
  catch (Tango::DevFailed &df)
  {
    std::string message = "Error in reading " + property_name + " in Configuration DataBase.";
    RETHROW_DEVFAILED(
	    df,
	    "DEVICE_ERROR", 
	    std::string(df.errors[0].desc).c_str(), 
	    "get_value_as_property"); 
  }

  //-	Try to extract saved property from database
  if (dev_prop[0].is_empty()==false)
	{
	  dev_prop[0] >> value;
  }
  else
  {
    // no value stored in data base
		THROW_DEVFAILED(
			"DEVICE_ERROR", 
			"No value in database", 
			"get_value_as_property"); 
  }

  return value;
}

// ============================================================================
// NexusConfig:  struct containing the nexus configuration
// ============================================================================
typedef struct NexusConfig
{
  // Enable or disable Nexus file generation
  bool nexusFileGeneration;

  // Nexus target path
  std::string nexusTargetPath;

  // Number of acquisitions pushed in a single Nexus file
  yat::uint32 nexusNbPerFile;

  // Nexus file name
  std::string nexusFileName;

  // Nexus data types
  std::vector<std::string> nexusDataTypes;

} NexusConfig;

// ============================================================================
// NexusStorage:  struct containing the items to be stored
// ============================================================================
typedef struct NexusStorage
{
    // position (H & V)
    bool position;

    // intensiy
    bool intensity;

    // flux
    bool flux;

    // position history (H & V)
    bool position_history;

    // intensity history
    bool intensity_history;

    //- reset values
    void reset()
    {
      position = false;
      intensity = false;
      flux = false;
      position_history = false;
      intensity_history = false;
    }
} NexusStorage;

} // namespace Xbpm_ns

#endif // _XBPM_TYPES_AND_CONSTS_H_

