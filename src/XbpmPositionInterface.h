//=============================================================================
// XbpmPositionInterface.h
//=============================================================================
// abstraction.......XbpmPositionInterface for XbpmPositionModes
// class.............XbpmPositionInterface
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _XBPM_POSITION_INTERFACE
#define _XBPM_POSITION_INTERFACE

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "TableReader.h"
#include "ConfigurationParser.h"

#include <yat4tango/LogHelper.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DynamicAttributeManager.h>
#include <yat4tango/PropertyHelper.h>


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionInterface
// ============================================================================
class XbpmPositionInterface : public yat4tango::TangoLogAdapter
{

public:

  //- constructor
  XbpmPositionInterface(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager): yat4tango::TangoLogAdapter(cfg.hostDevice), m_dyn_attr_manager(p_dyn_attr_manager){};

  //- destructor
  virtual ~XbpmPositionInterface(){};

  //- init
  virtual void init() = 0;

  //- gets state
  virtual Tango::DevState get_state() = 0;

  //- gets status
  virtual std::string get_status() = 0;
  
  //-gets properties
  virtual void get_properties() = 0;
  
  //-gets position
  virtual Position_t get_position(Currents_t p_currents) = 0;
  
  //- sets new electrometer gain value
  virtual void set_electrometer_gain (double gain)
  {
    // default behaviour: nothing to do
  };
  
private:
  //-computes x position
  virtual void compute_x_position_i() = 0;
  
  //-computes z position
  virtual void compute_z_position_i() = 0;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_INTERFACE
