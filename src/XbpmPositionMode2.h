//=============================================================================
// XbpmPositionMode2.h
//=============================================================================
// abstraction.......XbpmPositionMode2 for XbpmPositionModes
// class.............XbpmPositionMode2
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_POSITION_MODE2
#define _XBPM_POSITION_MODE2

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"
#include "TableReader.h"
#include "CoefficientGrabber.h"
#include "CoefficientGrabberEnergy.h"
#include "CoefficientGrabberSlit.h"


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionMode2
// ============================================================================
class XbpmPositionMode2 : public XbpmPositionInterface
{

public:

  //- constructor
  XbpmPositionMode2(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~XbpmPositionMode2();

  //- init
  void init();

  //- gets xbpm manager state
  Tango::DevState get_state();
  
  //- gets status
  std::string get_status();

  //-gets properties
  void get_properties();
  
  //-gets position
  Position_t get_position(Currents_t p_currents);
  
  //- read callback for dyn attr OffsetTable
  void read_OffsetTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr OffsetTable
  void write_OffsetTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for dyn attr KTable
  void read_KTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr KTable
  void write_KTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for dyn attr KxTable
  void read_KxTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr KxTable
  void write_KxTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for dyn attr KzTable
  void read_KzTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr KzTable
  void write_KzTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);
  
private:
  
  //-computes x position
  void compute_x_position_i();
  
  //-computes z position
  void compute_z_position_i();

  //- configuration
  XbpmConfig m_cfg;

  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;

  //- BeamEnergyProxy name
  std::string m_beam_energy_proxy_name;

  //- Energy attr name
  std::string m_energy_attr;

  //- Offset table
  std::string m_offset_table;

  //- K table
  std::string m_k_table;

  //- HSlitGap attr name
  std::string m_h_slit_gap_attr;

  //- HSlitProxy name
  std::string m_h_slit_proxy_name;

  //- VSlitGap attr name
  std::string m_v_slit_gap_attr;

  //- VSlitProxy name
  std::string m_v_slit_proxy_name;

  //- kx table
  std::string m_kx_table;

  //- kz table
  std::string m_kz_table;

  //- position
  Position_t m_position;

  //- Currents
  Currents_t m_currents;

  //- kx
  double m_kx;

  //- kz
  double m_kz;

  //- Offset x
  double m_offset_x;

  //- Offset z
  double m_offset_z;

  //- coefficient grabber
  CoefficientGrabber * m_coeff_grabber;

  //- internal mode
  INTERNAL_MODE m_internal_mode;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- boolean for dyn offsettable
  bool m_is_dyn_offset_table;

  //- boolean for dyn kxtable
  bool m_is_dyn_kx_table;

  //- boolean for dyn kztable
  bool m_is_dyn_kz_table;

  //- boolean for dyn ktable
  bool m_is_dyn_k_table;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_MODE2
