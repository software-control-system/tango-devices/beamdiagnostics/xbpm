//=============================================================================
// CoefficientGrabberEnergy.h
//=============================================================================
// abstraction.......CoefficientGrabberEnergy for XbpmPositionModes
// class.............CoefficientGrabberEnergy
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COEFFICIENT_GRABBER_ENERGY
#define _COEFFICIENT_GRABBER_ENERGY

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "CoefficientGrabber.h"
#include "TableReader.h"
#include <yat4tango/LogHelper.h>


namespace Xbpm_ns
{

	// ============================================================================
	// class: CoefficientGrabberEnergy
	// ============================================================================
	class CoefficientGrabberEnergy : public CoefficientGrabber
	{

	public:

		//- constructor
		CoefficientGrabberEnergy(const CoefficientGrabberEnergyConfig& cfg);

		//- destructor
		virtual ~CoefficientGrabberEnergy();

		//- init
		void init();

		//- gets state
		Tango::DevState get_state();

		//- gets status
		std::string get_status();

		//- gets coefficients
		void get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z);

	private:
		//- configuration
		CoefficientGrabberEnergyConfig m_cfg;

		//- module state
		Tango::DevState m_state;

        //- detailed states
        Tango::DevState m_state_beam_energy;

		//- status
		std::string m_status;

		//- Proxy on beam energy
		Tango::DeviceProxy * m_dev_beam_energy;

		//- Reader for k_table X
		TableReader * m_k_table_x_reader;

		//- Reader for k_table Z
		TableReader * m_k_table_z_reader;

		//- Reader for offset table x
		TableReader * m_offset_table_x_reader;

		//- Reader for offset table z
		TableReader * m_offset_table_z_reader;

        //- boolean for status details
        bool m_beam_energy_in_fault;
        bool m_internal_error;
	};

} // namespace Xbpm_ns

#endif // _COEFFICIENT_GRABBER_ENERGY