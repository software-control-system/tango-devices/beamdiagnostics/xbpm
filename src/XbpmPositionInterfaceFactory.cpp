//=============================================================================
// XbpmPositionInterfaceFactory.cpp
//=============================================================================
// abstraction.......XbpmPositionInterfaceFactory for XbpmPositionMode
// class.............XbpmPositionInterfaceFactory
// original author...S.GARA - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "XbpmPositionMode0.h"
#include "XbpmPositionMode1.h"
#include "XbpmPositionMode2.h"
#include "XbpmPositionMode3.h"
#include "XbpmPositionMode4.h"
#include "XbpmPositionMode5.h"
#include "XbpmPositionMode6.h"
#include "XbpmPositionMode7.h"

#include "XbpmPositionInterfaceFactory.h"
#include <yat4tango/DeviceTask.h>


namespace Xbpm_ns
{

// ======================================================================
// XbpmPositionInterfaceFactory::instanciate
// ======================================================================
 XbpmPositionInterface * XbpmPositionInterfaceFactory::instanciate(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
{
  XbpmPositionInterface * xbpmInterface;

  // instanciate position mode according to mode value
  switch(cfg.xBPMmode)
  {
    case XBPM_MODE_0:
	  xbpmInterface = new XbpmPositionMode0(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_1:
      xbpmInterface = new XbpmPositionMode1(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_2:
      xbpmInterface = new XbpmPositionMode2(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_3:
      xbpmInterface = new XbpmPositionMode3(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_4:
      xbpmInterface = new XbpmPositionMode4(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_5:
      xbpmInterface = new XbpmPositionMode5(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_6:
      xbpmInterface = new XbpmPositionMode6(cfg, p_dyn_attr_manager);
      break;
    case XBPM_MODE_7:
      xbpmInterface = new XbpmPositionMode7(cfg, p_dyn_attr_manager);
      break;
    default:
      THROW_DEVFAILED("CONFIG_ERROR",
                      "invalid insertion device type specified [check device property]",
                      "XbpmPositionInterfaceFactory::instanciate");
      break;
  }
 
  return xbpmInterface;
}

} // namespace Xbpm_ns


