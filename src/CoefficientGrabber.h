//=============================================================================
// CoefficientGrabber.h
//=============================================================================
// abstraction.......CoefficientGrabber for XbpmPositionModes
// class.............CoefficientGrabber
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COEFFICIENT_GRABBER
#define _COEFFICIENT_GRABBER

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "XbpmTypesAndConsts.h"


namespace Xbpm_ns
{

	// ============================================================================
	// class: CoefficientGrabber
	// ============================================================================
	class CoefficientGrabber : public yat4tango::TangoLogAdapter
	{
	public:

		//- constructor
		CoefficientGrabber(Tango::DeviceImpl * hostDevice): yat4tango::TangoLogAdapter(hostDevice){};

		//- destructor
		virtual ~CoefficientGrabber(){};

		//- init
		virtual void init() = 0;

		//- gets state
		virtual Tango::DevState get_state() = 0;

		//- gets status
		virtual std::string get_status() = 0;

		//- gets coefficients
		virtual void get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z) = 0;
	};

} // namespace Xbpm_ns

#endif // _COEFFICIENT_GRABBER