//=============================================================================
// NexusManager.h
//=============================================================================
// abstraction.......Nexus storage manager abstraction
// class.............NexusManager
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _NEXUS_MANAGER_H_
#define _NEXUS_MANAGER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <nexuscpp/nexuscpp.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/LogHelper.h>

#include <yat/utils/Logging.h>

namespace Xbpm_ns
{

//- nexus data storage type
typedef enum
{
  NX_DATA_TYPE_UNDEF = 0x0,
  NX_DATA_TYPE_0D    = 0x1,
  NX_DATA_TYPE_1D    = 0x2,
  NX_DATA_TYPE_2D    = 0x3
} E_NxDataStorageType_t;

//- Item to store in Nexus file
typedef struct nxItem
{
  //- members --------------------
  //- Item name
  std::string name;

  //- Item storage type (dimension)
  E_NxDataStorageType_t storageType;

  //- Item 1D dimension
  yat::uint32 dim1;

  //- Item 2D dimension
  yat::uint32 dim2;

  //- default constructor -----------------------
  nxItem()
    : name(""),
      storageType(NX_DATA_TYPE_UNDEF),
      dim1(0),
      dim2(0)
  {
  }

  //- copy constructor ------------------
  nxItem(const nxItem& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const nxItem & operator= (const nxItem& src)
  {
      if (this == & src) 
        return *this;

      name = src.name;
      storageType = src.storageType;
      dim1 = src.dim1;
      dim2 = src.dim2;

      return *this;
  }
} nxItem;

//- nxItemList_t: list of items to store in Nexus file
typedef std::vector<nxItem> nxItemList_t;
typedef nxItemList_t::iterator nxItemList_it_t;

// ============================================================================
// class: NexusManager
// ============================================================================
class NexusManager : public Tango::LogAdapter, nxcpp::IExceptionHandler
{
public:

  //- constructor
  //- host_device: host device (for logs)
  NexusManager(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~NexusManager();
  
  //- sets nexus storage parameters
  //- nexus_file_path: path name for nexus files storage
  //- nexus_file_name: nexus file name
  //- acquisition_size: number of acquisitions (0 if infinite acquisition)
  //- nx_number_per_file: number of acquisition per nx file
  //- item_list: list of data types to store
  void initNexusAcquisition(std::string nexus_file_path, 
                            std::string nexus_file_name,                            
                            yat::uint32 acquisition_size,
                            yat::uint32 nx_number_per_file,
                            nxItemList_t item_list);

  //- normally ends nexus storage
  void finalizeNexusGeneration();

  //- aborts nexus storage
  void manageNexusAbort();

  //- reset buffer index
  void resetNexusBufferIndex();
	
  //- 
  std::string getNexusFileName()
	{return m_nexus_file_name;}
    

  //- stores a T data in a nexus file
  template <class T> void pushNexusData(std::string item_name, T * data, unsigned long size_to_copy = 0)
  {
    if (!m_pAcqWriter)
    {
      THROW_DEVFAILED("DEVICE_ERROR",
            "Failed to push data: internal pointer is NULL!",
            "NexusManager::pushNexusData");
    }

      // check if specified item is in item list
      bool itemFound = false;
      E_NxDataStorageType_t measDim = NX_DATA_TYPE_UNDEF;

      for (nxItemList_it_t it = m_itemList.begin(); it != m_itemList.end(); ++it)
      {
        if ((*it).name == item_name)
        {
          itemFound = true;
          measDim = (*it).storageType;
          break;
        }
      }

      if (!itemFound)
      {
        ERROR_STREAM << "Bad item name:" << item_name << std::endl;
        manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Bad item name, cannot store data in Nexus file", 
                        "NexusManager::pushNexusData");
      }

      if (!data)
      {
        ERROR_STREAM << "Try to send null data to Nexus!" << std::endl;
        manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Cannot store data in Nexus file: null data!", 
                        "NexusManager::pushNexusData");
      }
	  
      try
      {
        // check measure dimension
        if (measDim == NX_DATA_TYPE_1D)
        {
          // 1D
		  DEBUG_STREAM << "NexusManager::pushNexusData - send pushData 1D for: " << item_name << " - size to copy = " << size_to_copy << std::endl;
          m_pAcqWriter->PushData(item_name, data); // case: BUFFERED mode
        }
        else
        {
          // 0D
          // check size
		  DEBUG_STREAM << "NexusManager::pushNexusData - send pushData 0D for: " << item_name << " - size to copy = " << size_to_copy << std::endl;
          if (size_to_copy == 0)
            m_pAcqWriter->PushData(item_name, data); // case: SCALAR mode
          else
            m_pAcqWriter->PushData(item_name, data, size_to_copy); // case: BUFFERED mode
        }
      }
      catch(nxcpp::NexusException &n)
      {
        ERROR_STREAM << "NexusManager::pushNexusData -> caught NexusException: "
          << n.to_string()
          << std::endl;

        if (m_pAcqWriter)
          m_pAcqWriter->Abort();
        ERROR_STREAM << "Nexus storage ABORTED." << std::endl;
        
        Tango::DevFailed df = nexusToTangoException(n);
        throw df;
      }
	  catch(yat::Exception &e)
      {
        ERROR_STREAM << "NexusManager::pushNexusData -> caught YAT Exception: " << std::endl;
		YAT_LOG_EXCEPTION(e);
        manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Failed to store data in Nexus file (caught YAT exception)!", 
                        "NexusManager::pushNexusData");      
      }
	  catch(std::exception &f)
      {
		ERROR_STREAM << "NexusManager::pushNexusData -> caught std Exception: " << f.what() << std::endl;
        manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Failed to store data in Nexus file (caught std exception)!", 
                        "NexusManager::pushNexusData");
      }
	  catch(...)
      {
        ERROR_STREAM << "NexusManager::pushData -> caught [...] Exception" << std::endl;
        manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR", 
                        "Failed to store data in Nexus file (caught [...])!", 
                        "NexusManager::pushNexusData");      
      }
  }

  //- Gets Nexus storage state: if true, storage in progress
  bool getNexusStorageState()
  {
    return !(m_finalizeDone);
  }

  //- Gets Nexus storage exception
  bool hasStorageError()
  {
    return m_excptOccured;
  }

  // Nexus exception handler
  void OnNexusException(const nxcpp::NexusException &e)
  {
    ERROR_STREAM << "NexusManager::OnNexusException -> caught NEXUS Exception:" 
      << e.to_string()
      << std::endl;
    
    if (m_pAcqWriter)
      m_pAcqWriter->Abort();

    m_excptOccured = true;
  }

protected:

  //- converts a NeXus exception to a tango exception.
  Tango::DevFailed nexusToTangoException(const nxcpp::NexusException &nxte);

  //- the Nexus buffer pointer
  nxcpp::DataStreamer* m_pAcqWriter;

  //- the Nexus buffer pointer
  nxcpp::NexusDataStreamerFinalizer * m_NexusDataStreamerFinalizer;

  //- nexus file flag for end of storage
  bool m_finalizeDone;

  //- nexus finalization lock
  yat::Mutex m_finalizeNxLock;

  //- type of storage flag
  bool m_finiteStorage;

  //- list of items to store
  nxItemList_t m_itemList;

  //- nexus exception flag
  bool m_excptOccured;
  
  //- nexus file name
  std::string m_nexus_file_name;
};

} // namespace Xbpm_ns

#endif // _NEXUS_MANAGER_H_
