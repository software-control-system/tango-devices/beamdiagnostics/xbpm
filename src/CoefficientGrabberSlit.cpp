//=============================================================================
// CoefficientGrabberSlit.cpp
//=============================================================================
// abstraction.......CoefficientGrabberSlit for XbpmPositionModes
// class.............CoefficientGrabberSlit
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CoefficientGrabberSlit.h"
#include <yat4tango/DeviceTask.h>

namespace Xbpm_ns
{

	// ============================================================================
	// CoefficientGrabberSlit::CoefficientGrabberSlit ()
	// ============================================================================ 
	CoefficientGrabberSlit::CoefficientGrabberSlit (const CoefficientGrabberSlitConfig & cfg) 
		: CoefficientGrabber(cfg.hostDevice),
		m_cfg(cfg)
	{
		m_dev_beam_energy = NULL;
		m_dev_h_slit = NULL;
		m_dev_v_slit = NULL;
		m_k_table_x_reader = NULL;
		m_k_table_z_reader = NULL;
		m_offsetx_table_reader = NULL;
		m_offsetz_table_reader = NULL;
		m_status = "";
		m_state = Tango::UNKNOWN;
        m_beam_energy_in_fault = false;
        m_hslit_in_fault = false;
        m_vslit_in_fault = false;
        m_internal_error = false;
        m_state_beam = Tango::UNKNOWN;
        m_state_h_slit = Tango::UNKNOWN;
        m_state_v_slit = Tango::UNKNOWN;
	}

	// ============================================================================
	// CoefficientGrabberSlit::~CoefficientGrabberSlit ()
	// ============================================================================ 
	CoefficientGrabberSlit::~CoefficientGrabberSlit () 
	{
		if (m_dev_beam_energy)
		{
			delete m_dev_beam_energy;
            m_dev_beam_energy = NULL;
		}
		if (m_dev_h_slit)
		{
			delete m_dev_h_slit;
            m_dev_h_slit = NULL;
		}
		if (m_dev_v_slit)
		{
			delete m_dev_v_slit;
            m_dev_v_slit = NULL;
		}
		if (m_k_table_x_reader)
		{
			delete m_k_table_x_reader;
            m_k_table_x_reader = NULL;
		}
		if (m_k_table_z_reader)
		{
			delete m_k_table_z_reader;
            m_k_table_z_reader = NULL;
		}
		if (m_offsetx_table_reader)
		{
			delete m_offsetx_table_reader;
            m_offsetx_table_reader = NULL;
		}
		if (m_offsetz_table_reader)
		{
			delete m_offsetz_table_reader;
            m_offsetz_table_reader = NULL;
		}
	}


	// ============================================================================
	// CoefficientGrabberSlit::get_state ()
	// ============================================================================ 
	Tango::DevState CoefficientGrabberSlit::get_state()
	{
        // get beam energy device state
        if (m_dev_beam_energy)
		{
			try
			{
                m_beam_energy_in_fault = false;
				m_state_beam = m_dev_beam_energy->state();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
                m_beam_energy_in_fault = true;
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to get beam energy device state" << std::endl;
                m_beam_energy_in_fault = true;
			}
		}

        // get hslit device state
		if (m_dev_h_slit)
		{
			try
			{
                m_hslit_in_fault = false;
				m_state_h_slit = m_dev_h_slit->state();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
                m_hslit_in_fault = true;
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to get h slit device state" << std::endl;
                m_hslit_in_fault = true;
			}
		}

    	// get vslit device state
		if (m_dev_v_slit)
		{
			try
			{
        		m_vslit_in_fault = false;
				m_state_v_slit = m_dev_v_slit->state();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
        		m_vslit_in_fault = true;
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to get v slit device state" << std::endl;
        		m_vslit_in_fault = true;
			}
		}

    	// if at least one device in FAULT state, grabber in FAULT state
		if ((	m_state_beam == Tango::FAULT) ||
			  (	m_state_h_slit == Tango::FAULT) ||
			  (	m_state_v_slit == Tango::FAULT) ||
				m_beam_energy_in_fault ||
				m_hslit_in_fault ||
				m_vslit_in_fault ||
				m_internal_error)
		{				
			m_state = Tango::FAULT;
		}
    	// if at least one device in ALARM state, grabber in ALARM state
		else if (	m_state_beam == Tango::ALARM ||
				    m_state_h_slit == Tango::ALARM ||
				    m_state_v_slit == Tango::ALARM)
		{
			m_state = Tango::ALARM;
		}
    	// if at least one device in STANDBY or OFF state, grabber in STANDBY state
		else if (	(m_state_beam == Tango::STANDBY) ||
				    (m_state_h_slit == Tango::STANDBY) ||
				    (m_state_v_slit == Tango::STANDBY) ||
             		(m_state_beam == Tango::OFF) ||
				    (m_state_h_slit == Tango::OFF) ||
				    (m_state_v_slit == Tango::OFF))
		{
			m_state = Tango::STANDBY;
		}
		else
		{
			m_state = Tango::RUNNING;
		}

		return m_state;
	}

	// ============================================================================
	// CoefficientGrabberSlit::get_status ()
	// ============================================================================ 
	std::string CoefficientGrabberSlit::get_status()
	{
		// compose module general status
		switch (m_state)
		{
			case Tango::RUNNING:
			case Tango::ON:
			case Tango::MOVING:
			{
				m_status = "current module state is: RUNNING\n";
			}
			break;
			case Tango::ALARM:
			{
				m_status = "current module state is: ALARM\n";
			}
			break;
			case Tango::STANDBY:
			case Tango::OFF:
			{
				m_status = "current module state is: STANDBY\n";
			}
			break;
			case Tango::FAULT:
			{
				m_status = "module has switched to FAULT state\n";
				if (m_beam_energy_in_fault)
				m_status += "Beam energy Device proxy error!\n";
				else if (m_hslit_in_fault)
				m_status += "HSlit Device proxy error!\n";
				else if (m_vslit_in_fault)
				m_status += "VSlit Device proxy error!\n";
				else if (m_internal_error)
				m_status += "Internal error!\n";
				else
				m_status += "Unknown error!\n";
			}
			break;
			default:
			{
				m_status = "Unhandled module state!!\n";
			}
			break;
		}

		// add detailed states
		if (!m_hslit_in_fault)
			m_status += " --HSlit Device state: " + std::string(Tango::DevStateName[m_state_h_slit]) + "\n";

		if (!m_vslit_in_fault)
			m_status += " --VSlit Device state: " + std::string(Tango::DevStateName[m_state_v_slit]) + "\n";
		
		if (!m_beam_energy_in_fault)
			m_status += " --Beam Energy Device state: " + std::string(Tango::DevStateName[m_state_beam]) + "\n";

		return m_status;
	}

	// ============================================================================
	// CoefficientGrabberSlit::init ()
	// ============================================================================ 
	void CoefficientGrabberSlit::init()
	{
		m_beam_energy_in_fault = false;
		m_hslit_in_fault = false;
		m_vslit_in_fault = false;
		m_internal_error = false;

    	// create beam energy device proxy
		try
		{
			m_dev_beam_energy = new Tango::DeviceProxy(m_cfg.m_beam_energy_proxy_name);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_state = Tango::FAULT;
			m_beam_energy_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"initialization failed - failed to create beam energy proxy", 
				"CoefficientGrabberSlit::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create beam energy proxy" << std::endl;
			m_state = Tango::FAULT;
			m_beam_energy_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create beam energy proxy", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_dev_beam_energy)
		{
			ERROR_STREAM << "Failed to access Beam Energy proxy" << std::endl;
			m_state = Tango::FAULT;
			m_beam_energy_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Beam Energy proxy", 
				"CoefficientGrabberSlit::init");  
		}

    	// create hslit device proxy
		try
		{
			m_dev_h_slit = new Tango::DeviceProxy(m_cfg.m_h_slit_proxy_name);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_state = Tango::FAULT;
			m_hslit_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"initialization failed - failed to create h slit proxy", 
				"CoefficientGrabberSlit::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create h slit proxy" << std::endl;
			m_state = Tango::FAULT;
			m_hslit_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create h slit proxy", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_dev_h_slit)
		{
			ERROR_STREAM << "Failed to access HSlit proxy" << std::endl;
			m_state = Tango::FAULT;
			m_hslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access HSlit proxy", 
				"CoefficientGrabberSlit::init");  
		}

    	// create vslit device proxy
		try
		{
			m_dev_v_slit = new Tango::DeviceProxy(m_cfg.m_v_slit_proxy_name);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_state = Tango::FAULT;
			m_vslit_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"initialization failed - failed to create v slit proxy", 
				"CoefficientGrabberSlit::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create v slit proxy" << std::endl;
			m_state = Tango::FAULT;
			m_vslit_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create v slit proxy", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_dev_v_slit)
		{
			ERROR_STREAM << "Failed to access VSlit proxy" << std::endl;
			m_state = Tango::FAULT;
			m_vslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access VSlit proxy", 
				"CoefficientGrabberSlit::init");  
		}

    	// create reader for Energy --> offset X table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
			l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
			m_offsetx_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Energy -> Offset X",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offset table x" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offset table x", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_offsetx_table_reader)
		{
			ERROR_STREAM << "Failed to access OffsetX table reader" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access OffsetX table reader", 
				"CoefficientGrabberSlit::init");  
		}

    	// create reader for Energy --> offset z table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
			l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
			m_offsetz_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Energy -> Offset Z",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offset table z" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offset table z", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_offsetz_table_reader)
		{
			ERROR_STREAM << "Failed to access OffsetZ table reader" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access OffsetZ table reader", 
				"CoefficientGrabberSlit::init");  
		}

    	// create reader for Energy, hslit --> coeff x table
		try
		{
			std::vector<int> l_list;
			m_k_table_x_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_kx_table_path, "Energy, H slit -> calibration factor X",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for k table x" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for k table x", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_k_table_x_reader)
		{
			ERROR_STREAM << "Failed to access Kx table reader" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Kx table reader", 
				"CoefficientGrabberSlit::init");  
		}

    	// create reader for Energy, vslit --> coeff z table
		try
		{
			std::vector<int> l_list;
			m_k_table_z_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_kz_table_path, "Energy, V slit -> calibration factor Z",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for k table z" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for k table z", 
				"CoefficientGrabberSlit::init"); 
		}

		if (!m_k_table_z_reader)
		{
			ERROR_STREAM << "Failed to access Kz table reader" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Kz table reader", 
				"CoefficientGrabberSlit::init");  
		}

		// init OK
		m_state = Tango::ON;
	}

	// ============================================================================
	// CoefficientGrabberSlit::get_coefficients ()
	// ============================================================================ 
	void CoefficientGrabberSlit::get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z)
	{
		double l_energy = 0;
		double l_h_slit_gap = 0;
		double l_v_slit_gap = 0;
        m_internal_error = false;

    	// get energy from beam energy device
		if (!m_dev_beam_energy)
		{
			ERROR_STREAM << "Failed to access Beam Energy proxy" << std::endl;
			m_beam_energy_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Beam Energy proxy", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_beam_energy->read_attribute(m_cfg.m_energy_attr_name);
			l_attr >> l_energy;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_beam_energy_in_fault = true; 
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get energy", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get energy" << std::endl;
			m_beam_energy_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get energy", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

    	// get gap from hslit device
		if (!m_dev_h_slit)
		{
			ERROR_STREAM << "Failed to access HSlit proxy" << std::endl;
			m_hslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access HSlit proxy", 
				"CoefficientGrabberSlit::get_coefficients");  
		}
		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_h_slit->read_attribute(m_cfg.m_h_slit_gap_attr);
			l_attr >> l_h_slit_gap;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_hslit_in_fault = true; 
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get h slit gap", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get h slit gap" << std::endl;
			m_hslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get h slit gap", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

    	// get gap from vslit device
		if (!m_dev_v_slit)
		{
			ERROR_STREAM << "Failed to access VSlit proxy" << std::endl;
			m_vslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access VSlit proxy", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_v_slit->read_attribute(m_cfg.m_v_slit_gap_attr);
			l_attr >> l_v_slit_gap;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_vslit_in_fault = true; 
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get v slit gap", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get v slit gap" << std::endl;
			m_vslit_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get v slit gap", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

    	// read offsets & coeffs from tables
		if (!m_k_table_x_reader)
		{
			ERROR_STREAM << "Failed to access Kx table reader" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Kx table reader", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			p_kx = m_k_table_x_reader->get_interpolated_value(l_energy, l_h_slit_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kx" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kx", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

		if (!m_k_table_z_reader)
		{
			ERROR_STREAM << "Failed to access Kz table reader" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access Kz table reader", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			p_kz = m_k_table_z_reader->get_interpolated_value(l_energy, l_v_slit_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kz" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kz", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

		if (!m_offsetx_table_reader)
		{
			ERROR_STREAM << "Failed to access OffsetX table reader" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access OffsetX table reader", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			p_offset_x = m_offsetx_table_reader->get_interpolated_value(l_energy);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetX" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetX", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}

		if (!m_offsetz_table_reader)
		{
			ERROR_STREAM << "Failed to access OffsetZ table reader" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to access OffsetZ table reader", 
				"CoefficientGrabberSlit::get_coefficients");  
		}

		try
		{
			p_offset_z = m_offsetz_table_reader->get_interpolated_value(l_energy);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetZ" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetZ", 
				"CoefficientGrabberSlit::get_coefficients"); 
		}
	}

} // namespace Xbpm_ns