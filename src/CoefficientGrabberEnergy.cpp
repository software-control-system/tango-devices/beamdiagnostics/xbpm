//=============================================================================
// CoefficientGrabberEnergy.cpp
//=============================================================================
// abstraction.......CoefficientGrabberEnergy for XbpmPositionModes
// class.............CoefficientGrabberEnergy
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CoefficientGrabberEnergy.h"
#include <yat4tango/DeviceTask.h>

namespace Xbpm_ns
{

    // ============================================================================
    // CoefficientGrabberEnergy::CoefficientGrabberEnergy ()
    // ============================================================================ 
    CoefficientGrabberEnergy::CoefficientGrabberEnergy (const CoefficientGrabberEnergyConfig & cfg) 
        : CoefficientGrabber(cfg.hostDevice),
        m_cfg(cfg)
    {
        m_dev_beam_energy = NULL;
        m_k_table_x_reader = NULL;
        m_k_table_z_reader = NULL;
        m_offset_table_x_reader = NULL;
        m_offset_table_z_reader = NULL;
        m_status = "";
        m_state = Tango::UNKNOWN;
        m_state_beam_energy = Tango::UNKNOWN;
        m_beam_energy_in_fault = false;
        m_internal_error = false;
    }

    // ============================================================================
    // CoefficientGrabberEnergy::~CoefficientGrabberEnergy ()
    // ============================================================================ 
    CoefficientGrabberEnergy::~CoefficientGrabberEnergy () 
    {
        if (m_dev_beam_energy)
        {
            delete m_dev_beam_energy;
            m_dev_beam_energy = NULL;
        }
        if (m_k_table_x_reader)
        {
            delete m_k_table_x_reader;
            m_k_table_x_reader = NULL;
        }
        if (m_k_table_z_reader)
        {
            delete m_k_table_z_reader;
            m_k_table_z_reader = NULL;
        }
        if (m_offset_table_x_reader)
        {
            delete m_offset_table_x_reader;
            m_offset_table_x_reader = NULL;
        }
        if (m_offset_table_z_reader)
        {
            delete m_offset_table_z_reader;
            m_offset_table_z_reader = NULL;
        }
    }


    // ============================================================================
    // CoefficientGrabberEnergy::get_state ()
    // ============================================================================ 
    Tango::DevState CoefficientGrabberEnergy::get_state()
    {
    // get beam energy proxy state
        if (m_dev_beam_energy)
        {
            try
            {
                m_beam_energy_in_fault = false;
                m_state_beam_energy = m_dev_beam_energy->state();
            }
            catch (Tango::DevFailed &e)
            {
                ERROR_STREAM << e << std::endl;
                m_beam_energy_in_fault = true;
            }
            catch (...)
            {
                ERROR_STREAM << "Failed to get beam energy device state" << std::endl;
                m_beam_energy_in_fault = true;
            }

            // update module state
            if (m_internal_error || 
                m_beam_energy_in_fault ||
                (m_state_beam_energy == Tango::FAULT))
            {
                m_state = Tango::FAULT;
            }
            else if (m_state_beam_energy == Tango::STANDBY)
            {
                m_state = Tango::STANDBY;
            }
            else
            {
                m_state = Tango::RUNNING;
            }
        }
        
    return m_state;
    }

    // ============================================================================
    // CoefficientGrabberEnergy::get_status ()
    // ============================================================================ 
    std::string CoefficientGrabberEnergy::get_status()
    {
    // compose module general status
    switch (m_state)
    {
      case Tango::RUNNING:
      case Tango::ON:
      case Tango::MOVING:
      {
        m_status = "current module state is: RUNNING\n";
      }
      break;
      case Tango::ALARM:
      {
        m_status = "current module state is: ALARM\n";
      }
      break;
      case Tango::STANDBY:
      case Tango::OFF:
      {
        m_status = "current module state is: STANDBY\n";
      }
      break;
      case Tango::FAULT:
      {
        m_status = "module has switched to FAULT state\n";
        if (m_beam_energy_in_fault)
          m_status += "Beam Energy Device proxy error!\n";
        else if (m_internal_error)
          m_status += "Internal error!\n";
        else
          m_status += "Unknown error!\n";
      }
      break;
      default:
      {
        m_status = "Unhandled module state!!\n";
      }
      break;
    }

    // add detailed states
    if (!m_beam_energy_in_fault)
    {
      m_status += " --Beam Energy Device state: " + std::string(Tango::DevStateName[m_state_beam_energy]) + "\n";
    }

      return m_status;
    }

    // ============================================================================
    // CoefficientGrabberEnergy::init ()
    // ============================================================================ 
    void CoefficientGrabberEnergy::init()
    {
        m_beam_energy_in_fault = false;
        m_internal_error = false;

        // create beam energy proxy
        try
        {
            m_dev_beam_energy = new Tango::DeviceProxy(m_cfg.m_beam_energy_proxy_name);
        }
        catch (Tango::DevFailed &e)
        {
            ERROR_STREAM << e << std::endl;
            m_state = Tango::FAULT;
            m_beam_energy_in_fault = true;
            RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
                                "initialization failed - failed to create beam energy proxy", 
                                "CoefficientGrabberEnergy::init"); 
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create beam energy proxy" << std::endl;
            m_state = Tango::FAULT;
            m_beam_energy_in_fault = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to create beam energy proxy", 
                            "CoefficientGrabberEnergy::init"); 
        }

        if (!m_dev_beam_energy)
        {
            ERROR_STREAM << "initialization failed - failed to access beam energy proxy" << std::endl;
            m_state = Tango::FAULT;
            m_beam_energy_in_fault = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to access beam energy proxy", 
                            "CoefficientGrabberEnergy::init"); 
        }

        // create reader for ENERGY --> OFFSET X table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
            l_list.push_back(INDEX_COEFFX_IN_FILE_1D);
            m_offset_table_x_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Energy -> Offset X",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for offset table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to create tableReader for offset table x", 
                            "CoefficientGrabberEnergy::init"); 
        }

        if (!m_offset_table_x_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for offset table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to access tableReader for offset table x", 
                            "CoefficientGrabberEnergy::init"); 
        }

        // create reader for ENERGY --> OFFSET Z table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
            l_list.push_back(INDEX_COEFFZ_IN_FILE_1D);
            m_offset_table_z_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Energy -> Offset Z",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for offset table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to create tableReader for offset table z", 
                            "CoefficientGrabberEnergy::init"); 
        }

        if (!m_offset_table_z_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for offset table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to access tableReader for offset table z", 
                            "CoefficientGrabberEnergy::init"); 
        }

        // create reader for ENERGY --> COEFF X table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
            l_list.push_back(INDEX_COEFFX_IN_FILE_1D);
            m_k_table_x_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_k_table_path, "Energy -> calibration factor X",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for k table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to create tableReader for k table x", 
                            "CoefficientGrabberEnergy::init"); 
        }

        if (!m_k_table_x_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for k table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to access tableReader for k table x", 
                            "CoefficientGrabberEnergy::init"); 
        }

        // create reader for ENERGY --> COEFF Z table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
            l_list.push_back(INDEX_COEFFZ_IN_FILE_1D);
            m_k_table_z_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_k_table_path, "Energy -> calibration factor Z",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for k table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to create tableReader for k table z", 
                            "CoefficientGrabberEnergy::init"); 
        }

        if (!m_k_table_z_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for k table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "initialization failed - failed to access tableReader for k table z", 
                            "CoefficientGrabberEnergy::init"); 
        }

        // init OK
        m_state = Tango::ON;
    }

    // ============================================================================
    // CoefficientGrabberEnergy::get_coefficients()
    // ============================================================================ 
    void CoefficientGrabberEnergy::get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z)
    {
        double l_energy = 0.0;

        // check proxies & reader

        //get energy from beam energy device
        if (!m_dev_beam_energy)
        {
            ERROR_STREAM << "get coefficients - failed to access beam energy proxy" << std::endl;
            m_beam_energy_in_fault = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "get coefficients - failed to access beam energy proxy", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        try
        {
            Tango::DeviceAttribute l_attr;
            l_attr = m_dev_beam_energy->read_attribute(m_cfg.m_energy_attr_name);
            l_attr >> l_energy;
        }
        catch (Tango::DevFailed &e)
        {
            ERROR_STREAM << e << std::endl;
            m_beam_energy_in_fault = true;
            RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
                                "Failed to get energy", 
                                "CoefficientGrabberEnergy::get_coefficients"); 
        }
        catch (...)
        {
            ERROR_STREAM << "Failed to get energy" << std::endl;
            m_beam_energy_in_fault = true; 
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Failed to get energy", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        // get offset and coefficient values for the energy value
        if (!m_k_table_x_reader)
        {
            ERROR_STREAM << "get coefficients - failed to access tableReader for k table x" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "get coefficients - failed to access tableReader for k table x", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        try
        {
            p_kx = m_k_table_x_reader->get_interpolated_value(l_energy);
        }
        catch (...)
        {
            ERROR_STREAM << "Failed to compute value for Kx" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Failed to compute value for Kx", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        if (!m_k_table_z_reader)
        {
            ERROR_STREAM << "get coefficients - failed to access tableReader for k table z" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "get coefficients - failed to access tableReader for k table z", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        try
        {
            p_kz = m_k_table_z_reader->get_interpolated_value(l_energy);
        }
        catch (...)
        {
            ERROR_STREAM << "Failed to compute value for Kz" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Failed to compute value for Kz", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        if (!m_offset_table_x_reader)
        {
            ERROR_STREAM << "get coefficients - failed to access tableReader for offset table x" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "get coefficients - failed to access tableReader for offset table x", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        try
        {
            p_offset_x = m_offset_table_x_reader->get_interpolated_value(l_energy);
        }
        catch (...)
        {
            ERROR_STREAM << "Failed to compute value for OffsetX" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Failed to compute value for OffsetX", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        if (!m_offset_table_z_reader)
        {
            ERROR_STREAM << "get coefficients - failed to access tableReader for offset table z" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "get coefficients - failed to access tableReader for offset table z", 
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }

        try
        {
            p_offset_z = m_offset_table_z_reader->get_interpolated_value(l_energy);
        }
        catch (...)
        {
            ERROR_STREAM << "Failed to compute value for OffsetZ" << std::endl;
            m_internal_error = true;
            THROW_DEVFAILED("DEVICE_ERROR", 
                            "Failed to compute value for OffsetZ",
                            "CoefficientGrabberEnergy::get_coefficients"); 
        }
    }

} // namespace Xbpm_ns