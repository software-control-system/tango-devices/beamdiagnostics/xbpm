//=============================================================================
// CoefficientGrabberSlit.h
//=============================================================================
// abstraction.......CoefficientGrabberSlit for XbpmPositionModes
// class.............CoefficientGrabberSlit
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COEFFICIENT_GRABBER_SLIT
#define _COEFFICIENT_GRABBER_SLIT

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "CoefficientGrabber.h"
#include "TableReader.h"
#include <yat4tango/LogHelper.h>


namespace Xbpm_ns
{

	// ============================================================================
	// class: CoefficientGrabberSlit
	// ============================================================================
	class CoefficientGrabberSlit : public CoefficientGrabber
	{

	public:

		//- constructor
		CoefficientGrabberSlit(const CoefficientGrabberSlitConfig& cfg);

		//- destructor
		virtual ~CoefficientGrabberSlit();

		//- init
		void init();

		//- gets state
		Tango::DevState get_state();

		//- gets status
		std::string get_status();

		//- gets coefficients
		void get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z);

	private:
		//- configuration
		CoefficientGrabberSlitConfig m_cfg;

		//- module state
		Tango::DevState m_state;

		//- status
		std::string m_status;

		//- Proxy on beam energy
		Tango::DeviceProxy * m_dev_beam_energy;

		//- Proxy on Hslit
		Tango::DeviceProxy * m_dev_h_slit;

		//- Proxy on Vslit
		Tango::DeviceProxy * m_dev_v_slit;

		//- reader for k_table x
		TableReader * m_k_table_x_reader;

		//- reader for k_table z
		TableReader * m_k_table_z_reader;

		//- reader for offsetx table
		TableReader * m_offsetx_table_reader;

		//- reader for offsetz table
		TableReader * m_offsetz_table_reader;

        //- boolean for status details
        bool m_beam_energy_in_fault;
        bool m_hslit_in_fault;
        bool m_vslit_in_fault;
        bool m_internal_error;

        // detailed states
        Tango::DevState m_state_beam;
        Tango::DevState m_state_h_slit;
        Tango::DevState m_state_v_slit;
	};

} // namespace Xbpm_ns

#endif // _COEFFICIENT_GRABBER_SLIT