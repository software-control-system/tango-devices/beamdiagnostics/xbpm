//=============================================================================
// CoefficientGrabberInsertXZ.cpp
//=============================================================================
// abstraction.......CoefficientGrabberInsertXZ for XbpmPositionModes
// class.............CoefficientGrabberInsertXZ
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CoefficientGrabberInsertXZ.h"
#include <yat4tango/DeviceTask.h>

namespace Xbpm_ns
{

	// ============================================================================
	// CoefficientGrabberInsertXZ::CoefficientGrabberInsertXZ()
	// ============================================================================ 
	CoefficientGrabberInsertXZ::CoefficientGrabberInsertXZ(const CoefficientGrabberInsertXZConfig & cfg) 
		: CoefficientGrabber(cfg.hostDevice),
		m_cfg(cfg)
	{
		m_dev_insertion = NULL;
		m_kx_table_reader = NULL;
		m_kz_table_reader = NULL;
		m_offsetx_table_reader = NULL;
		m_offsetz_table_reader = NULL;
		m_status = "";
		m_state = Tango::UNKNOWN;
		m_state_insertion = Tango::UNKNOWN;
		m_insertion_in_fault = false;
		m_internal_error = false;
	}

	// ============================================================================
	// CoefficientGrabberInsertXZ::~CoefficientGrabberInsertXZ()
	// ============================================================================ 
	CoefficientGrabberInsertXZ::~CoefficientGrabberInsertXZ() 
	{
		if (m_dev_insertion)
		{
			delete m_dev_insertion;
      		m_dev_insertion = NULL;
		}
		if (m_kx_table_reader)
		{
			delete m_kx_table_reader;
      		m_kx_table_reader = NULL;
		}
		if (m_kz_table_reader)
		{
			delete m_kz_table_reader;
      		m_kz_table_reader = NULL;
		}
		if (m_offsetx_table_reader)
		{
			delete m_offsetx_table_reader;
      		m_offsetx_table_reader = NULL;
		}
		if (m_offsetz_table_reader)
		{
			delete m_offsetz_table_reader;
      		m_offsetz_table_reader = NULL;
		}
	}


	// ============================================================================
	// CoefficientGrabberInsertXZ::get_state ()
	// ============================================================================ 
	Tango::DevState CoefficientGrabberInsertXZ::get_state()
	{
    	// get insertion device state
		if (m_dev_insertion)
		{
			try
			{
        		m_insertion_in_fault = false;
				m_state_insertion = m_dev_insertion->state();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
        		m_insertion_in_fault = true;
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to get insertion device state" << std::endl;
        		m_insertion_in_fault = true;
			}

			// update module state
			if (m_internal_error || 
				m_insertion_in_fault ||
				(m_state_insertion == Tango::FAULT))
			{
				m_state = Tango::FAULT;
			}
			else if (m_state_insertion == Tango::STANDBY)
			{
				m_state = Tango::STANDBY;
			}
			else
			{
				m_state = Tango::RUNNING;
			}
		}

		return m_state;
	}

	// ============================================================================
	// CoefficientGrabberInsertXZ::get_status ()
	// ============================================================================ 
	std::string CoefficientGrabberInsertXZ::get_status()
	{
		// compose module general status
		switch (m_state)
		{
			case Tango::RUNNING:
			case Tango::ON:
			case Tango::MOVING:
			{
				m_status = "current module state is: RUNNING\n";
			}
			break;
			case Tango::ALARM:
			{
				m_status = "current module state is: ALARM\n";
			}
			break;
			case Tango::STANDBY:
			case Tango::OFF:
			{
				m_status = "current module state is: STANDBY\n";
			}
			break;
			case Tango::FAULT:
			{
				m_status = "module has switched to FAULT state\n";
				if (m_insertion_in_fault)
				m_status += "Insertion Device proxy error!\n";
				else if (m_internal_error)
				m_status += "Internal error!\n";
				else
				m_status += "Unknown error!\n";
			}
			break;
			default:
			{
				m_status = "Unhandled module state!!\n";
			}
			break;
		}

		// add detailed states
		if (!m_insertion_in_fault)
		{
			m_status += " --Insertion Device state: " + std::string(Tango::DevStateName[m_state_insertion]) + "\n";
		}

		return m_status;
	}

	// ============================================================================
	// CoefficientGrabberInsertXZ::init ()
	// ============================================================================ 
	void CoefficientGrabberInsertXZ::init()
	{
		m_insertion_in_fault = false;
		m_internal_error = false;

    	// create insertion device proxy
		try
		{
			m_dev_insertion = new Tango::DeviceProxy(m_cfg.m_insertion_proxy_name);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
      		m_insertion_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"initialization failed - failed to create insertion proxy", 
				"CoefficientGrabberInsertXZ::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create insertion proxy" << std::endl;
      		m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create insertion proxy", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		if (!m_dev_insertion)
		{
			ERROR_STREAM << "initialization failed - failed to access insertion proxy" << std::endl;
      		m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access insertion proxy", 
				"CoefficientGrabberInsertXZ::init"); 
		}

    	// create reader for Gap, Phase -> Offset X table
		try
		{
			std::vector<int> l_list;
			m_offsetx_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offsetx_table_path, "Gap, Phase -> Offset X",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offsetx table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offsetx table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		if (!m_offsetx_table_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for offsetx table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for offsetx table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

    	// create reader for Gap, Phase -> Offset Z table
		try
		{
			std::vector<int> l_list;
			m_offsetz_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offsetz_table_path, "Gap, Phase -> Offset Z",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offsetz table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offsetz table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		if (!m_offsetz_table_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for offsetz table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for offsetz table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

    	// create reader for Gap, Phase -> Coeff X table
		try
		{
			std::vector<int> l_list;
			m_kx_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_kx_table_path, "Gap, Phase -> calibration factor X",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for kx table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for kx table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		if (!m_kx_table_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for kx table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for kx table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

    	// create reader for Gap, Phase -> Coeff Z table
		try
		{
			std::vector<int> l_list;
			m_kz_table_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_kz_table_path, "Gap, Phase -> calibration factor Z",true,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for kz table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for kz table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		if (!m_kz_table_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for kz table" << std::endl;
			m_state = Tango::FAULT;
			m_status = "See log for error details";
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for kz table", 
				"CoefficientGrabberInsertXZ::init"); 
		}

		// init OK
		m_state = Tango::ON;
	}

	// ============================================================================
	// CoefficientGrabberInsertXZ::get_coefficients ()
	// ============================================================================ 
	void CoefficientGrabberInsertXZ::get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z)
	{
		double l_gap = 0.0;
		double l_phase = 0.0;
		m_insertion_in_fault = false;
		m_internal_error = false;

    	// get gap from insertion device
		if (!m_dev_insertion)
		{
			ERROR_STREAM << "failed to access insertion proxy" << std::endl;
      		m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access insertion proxy", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_insertion->read_attribute(m_cfg.m_gap_attr_name);
			l_attr >> l_gap;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_insertion_in_fault = true; 
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get gap", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get gap" << std::endl;
			m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get gap", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

    	// get phase from insertion device
		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_insertion->read_attribute(m_cfg.m_phase_attr_name);
			l_attr >> l_phase;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_insertion_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get phase", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get phase" << std::endl;
			m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get phase", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

    	// read offsets & coeffs from gap, phase values
		if (!m_kx_table_reader)
		{
			ERROR_STREAM << "failed to access tableReader for kx table" << std::endl;
      m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for kx table", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

		try
		{
			p_kx = m_kx_table_reader->get_interpolated_value(l_gap, l_phase);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kx" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kx", 
				"CoefficientGrabberInsertXZ::get_coefficients");  
		}

		if (!m_kz_table_reader)
		{
			ERROR_STREAM << "failed to access tableReader for kz table" << std::endl;
      		m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for kz table", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

		try
		{
			p_kz = m_kz_table_reader->get_interpolated_value(l_gap, l_phase);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kz" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kz", 
				"CoefficientGrabberInsertXZ::get_coefficients");  
		}

		if (!m_offsetx_table_reader)
		{
			ERROR_STREAM << "failed to access tableReader for offsetx table" << std::endl;
      		m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for offsetx table", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

		try
		{
			p_offset_x = m_offsetx_table_reader->get_interpolated_value(l_gap, l_phase);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetX" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetX", 
				"CoefficientGrabberInsertXZ::get_coefficients");  
		}

		if (!m_offsetz_table_reader)
		{
			ERROR_STREAM << "failed to access tableReader for offsetz table" << std::endl;
      		m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for offsetz table", 
				"CoefficientGrabberInsertXZ::get_coefficients"); 
		}

		try
		{
			p_offset_z = m_offsetz_table_reader->get_interpolated_value(l_gap, l_phase);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetZ" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetZ", 
				"CoefficientGrabberInsertXZ::get_coefficients");  
		}
	}

} // namespace Xbpm_ns