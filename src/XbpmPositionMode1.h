//=============================================================================
// XbpmPositionMode1.h
//=============================================================================
// abstraction.......XbpmPositionMode1 for XbpmPositionModes
// class.............XbpmPositionMode1
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_POSITION_MODE1
#define _XBPM_POSITION_MODE1

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"
#include "TableReader.h"


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionMode1
// ============================================================================
class XbpmPositionMode1 : public XbpmPositionInterface
{

public:

  //- constructor
  XbpmPositionMode1(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~XbpmPositionMode1();

  //- init
  void init();

  //- gets xbpm manager state
  Tango::DevState get_state();
  
  //- gets status
  std::string get_status();

  //-gets properties
  void get_properties();
  
  //-gets position
  Position_t get_position(Currents_t p_currents);
  
  //- read callback for dyn attr OffsetTable
  void read_OffsetTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr OffsetTable
  void write_OffsetTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for dyn attr BeamSizeTable
  void read_BeamsizeTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr BeamSizeTable
  void write_BeamsizeTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);
  
private:
  
  //-computes x position
  void compute_x_position_i();
  
  //-computes z position
  void compute_z_position_i();

  //- configuration
  XbpmConfig m_cfg;

  //- module state
  Tango::DevState m_state;

  //- detailed states
  Tango::DevState m_state_beam_energy;

  //- status
  std::string m_status;

  //- BeamEnergyProxy name
  std::string m_beam_energy_proxy_name;

  //- Energy attr name
  std::string m_energy_attr;

  //- BeamSize table
  std::string m_beam_size_table;

  //- Offset table
  std::string m_offset_table;

  //- Detector Gap
  double m_detector_gap;

  //- device proxy on beam energy
  Tango::DeviceProxy * m_dev_beam_energy;

  //- position
  Position_t m_position;

  //- Currents
  Currents_t m_currents;

  //- table reader for offset table x
  TableReader * m_offset_table_x_reader;

  //- table reader for offset table z
  TableReader * m_offset_table_z_reader;

  //- table reader for beam size table x
  TableReader * m_beam_table_x_reader;

  //- table reader for beam size table z
  TableReader * m_beam_table_z_reader;

  //- kx
  double m_kx;

  //- kz
  double m_kz;

  //- Offset x
  double m_offset_x;

  //- Offset z
  double m_offset_z;

  //- boolean for status details
  bool m_beam_energy_in_fault; 
  bool m_internal_error;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- boolean for dyn beamsizetable
  bool m_is_dyn_beamsize_table;

  //- boolean for dyn offsettable
  bool m_is_dyn_offset_table;

  //- boolean for managing dynamic table reader offset
  bool m_is_offset_table_empty;

  //- boolean for managing dynamic table reader beamsize
  bool m_is_beamsize_table_empty;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_MODE1
