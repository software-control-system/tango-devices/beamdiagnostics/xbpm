//=============================================================================
// XbpmPositionMode5.h
//=============================================================================
// abstraction.......XbpmPositionMode5 for XbpmPositionModes
// class.............XbpmPositionMode5
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_POSITION_MODE5
#define _XBPM_POSITION_MODE5

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionMode5
// ============================================================================
class XbpmPositionMode5 : public XbpmPositionInterface
{

public:

  //- constructor
  XbpmPositionMode5(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~XbpmPositionMode5();

  //- init
  void init();

  //- gets xbpm manager state
  Tango::DevState get_state();
  
  //- gets status
  std::string get_status();
  
  //-gets properties
  void get_properties();
  
  //-gets position
  Position_t get_position(Currents_t p_currents);
  
  //- sets new electrometer gain value
  void set_electrometer_gain (double gain)
  {
	    // memorize new gain value
        m_gain = gain;
  };

  //- read callback for dyn attr CorrectionTable
  void read_CorrectionTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr CorrectionTable
  void write_CorrectionTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);
  
private:
  
  //-computes x position
  void compute_x_position_i();
  
  //-computes z position
  void compute_z_position_i();

  //- configuration
  XbpmConfig m_cfg;

  //- module state
  Tango::DevState m_state;

  //- correction table
  std::string m_correction_table;

  //- kx factor
  double m_kx_factor;

  //- kz factor
  double m_kz_factor;

  //- misalignment offset x
  double m_misaligement_offset_x;

  //- operator offset x
  double m_operator_offset_x;

  //- offset x
  double m_offset_x;

  //- misalignment offset z
  double m_misaligement_offset_z;

  //- operator offset z
  double m_operator_offset_z;

  //- offset z
  double m_offset_z;

  //- position
  Position_t m_position;

  //- Currents
  Currents_t m_currents;

  //- m_currentoffset_x
  double m_current_offset_x;

  //- m_currentoffset_z
  double m_current_offset_z;

  //- electrometer gain
  double m_gain;

  //- reader for offsetx correction table
  TableReader * m_offsetx_table_reader;

  //- reader for offsetz correction table
  TableReader * m_offsetz_table_reader;

  //- boolean for status details
  bool m_internal_error;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- boolean for managing dynamic attributes
  bool m_is_dyn_table;

  //- boolean for managing dynamic table reader
  bool m_is_table_empty;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_MODE5
