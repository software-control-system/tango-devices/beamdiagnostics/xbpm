//=============================================================================
// XbpmPositionMode6.cpp
//=============================================================================
// abstraction.......XbpmPositionMode6 for XbpmPositionModes
// class.............XbpmPositionMode6
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode6.h"


namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode6::XbpmPositionMode6 ()
// ============================================================================ 
XbpmPositionMode6::XbpmPositionMode6(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
	m_kx_factor = 1;
	m_kz_factor = 1;
	m_offset_x = 0;
	m_offset_z = 0;
}

// ============================================================================
// XbpmPositionMode6::~XbpmPositionMode6 ()
// ============================================================================ 
XbpmPositionMode6::~XbpmPositionMode6()
{

}

// ============================================================================
// XbpmPositionMode6::init ()
// ============================================================================ 
void XbpmPositionMode6::init()
{
	m_state = Tango::RUNNING;
	m_status = "Module is running.";
}

// ============================================================================
// XbpmPositionMode6::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode6::get_state()
{
	return m_state;
}

// ============================================================================
// XbpmPositionMode6::get_status ()
// ============================================================================ 
std::string XbpmPositionMode6::get_status()
{
	return m_status;
}

// ============================================================================
// XbpmPositionMode6::get_properties ()
// ============================================================================ 
void XbpmPositionMode6::get_properties()
{
	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode6"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);


	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	if (!dev_prop[0].is_empty())
	{
		ModeKeys_t l_keys;
		bool _l_is_mode6 = false;
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
			_l_is_mode6 = true;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"Failed to parse Mode6 property", 
				"XbpmPositionMode6::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Failed to parse Mode6 property", 
				"XbpmPositionMode6::get_properties"); 
		}
		if (_l_is_mode6)
		{
			try
			{
				m_offset_x = l_conf_parser.extractOffsetX(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OffsetX.Using default value : 0" << endl;
            }
			try
			{
				m_offset_z = l_conf_parser.extractOffsetZ(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OffsetZ.Using default value : 0" << endl;
			}
			try
            {
				m_kx_factor = l_conf_parser.extractKXFactor(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for KxFactor.Using default value : 1" << endl;
            }
			try
            {
				m_kz_factor = l_conf_parser.extractKZFactor(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for KzFactor.Using default value : 1" << endl;
			}
		}
	}
}

// ============================================================================
// XbpmPositionMode6::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode6::get_position(Currents_t p_currents)
{
	m_currents = p_currents;

	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode6::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode6::compute_x_position_i()
{
	// X = [ ((I0 + I3) - (I1 + I2)) / Isum ] * Kx_Factor + OffsetX
	m_position.x = (((m_currents.current1 + m_currents.current4) - (m_currents.current2 + m_currents.current3)) / m_currents.intensity) * m_kx_factor + m_offset_x;
}

// ============================================================================
// XbpmPositionMode6::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode6::compute_z_position_i()
{
	// Z = [ ((I0 + I3) - (I1 + I2)) / Isum ] * Kz_Factor + OffsetZ
	m_position.z = (((m_currents.current1 + m_currents.current2) - (m_currents.current4 + m_currents.current3)) / m_currents.intensity) * m_kz_factor + m_offset_z;
}

} // namespace Xbpm_ns