//=============================================================================
// XbpmManager.cpp
//=============================================================================
// abstraction.......XbpmManager for Xbpm
// class.............XbpmManager
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/LogHelper.h>
#include "XbpmManager.h"

namespace Xbpm_ns
{

//- check currentDataGrabber macro:
#define CHECK_CURRENT_DATA_GRABBER() \
do \
{ \
  if (! m_current_data_grabber) \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the current data grabber isn't accessible ", \
      "XbpmManager::check_current_data_grabber"); \
} while (0)

//- check positionDataGrabber macro:
#define CHECK_POSITION_DATA_GRABBER() \
do \
{ \
  if (! m_position_data_grabber) \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the position data grabber isn't accessible ", \
      "XbpmManager::check_position_data_grabber"); \
} while (0)

//- Check nexus manager macro:
#define CHECK_NX_MANAGER() \
do \
{ \
  if (! m_nexus_manager) \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the nexus manager isn't accessible ", \
      "XbpmManager::check_nexus_manager"); \
} while (0)

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE DEVICE TASK
// ============================================================================
#define MSG_STOP              (yat::FIRST_USER_MSG + 1001)
#define MSG_START              (yat::FIRST_USER_MSG + 1002)

// ============================================================================
// XbpmManager::XbpmManager ()
// ============================================================================ 
XbpmManager::XbpmManager (Tango::DeviceImpl * hostDevice)
: yat4tango::DeviceTask(hostDevice)
{
  //- trace/profile this method
  yat4tango::TraceHelper t("XbpmManager::XbpmManager", this);

  enable_timeout_msg(false);
  enable_periodic_msg(false);

  m_autoRange = false;
  m_integrationTime = 1000.0; // default value for polling period
  m_useDefaultPP = true;
  m_gain = 0.0;
  m_unit = 1;
  m_state = Tango::UNKNOWN;
  m_previousState = Tango::UNKNOWN;
  m_status = "Unknown";
  m_current_data_grabber = NULL;
  m_position_data_grabber = NULL;
  m_histo_size = 0;
  m_sai_histos.clear_all();
  m_nexus_manager = NULL;
  m_nexus_storage.reset();

  try
  {
    m_dyn_attr_manager = new yat4tango::DynamicAttributeManager(hostDevice);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to create Dynamic Attribute Manager", 
			"XbpmManager::XbpmManager"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to create Dynamic Attribute Manager!" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to create Dynamic Attribute Manager!", 
			"XbpmManager::XbpmManager"); 
  }
}

// ============================================================================
// XbpmManager::~XbpmManager ()
// ============================================================================ 
XbpmManager::~XbpmManager ()
{
  enable_periodic_msg(false);

  if(m_current_data_grabber)
  {
    delete m_current_data_grabber;
    m_current_data_grabber = NULL;
  }
  if (m_position_data_grabber)
  {
    delete m_position_data_grabber;
    m_position_data_grabber = NULL;
  }

  // delete nexus manager if exists
  if (m_nexus_manager)
  {
    delete m_nexus_manager;
    m_nexus_manager = NULL;
  }

  // remove dynamic attributes 
  if (m_dyn_attr_manager)
  {
    try
    {
      m_dyn_attr_manager->remove_attributes();
    }
    catch (...)
    {
      //- ignore any error
    }
  }

  // delete dynamic attributes manager
  if (m_dyn_attr_manager)
  {
    delete m_dyn_attr_manager;
    m_dyn_attr_manager = NULL;
  }

  // clear history data
  m_histo_size = 0;
  m_sai_histos.clear_all();
}

// ============================================================================
// XbpmManager::get_state ()
// ============================================================================ 
Tango::DevState XbpmManager::get_state()
{
  Tango::DevState device_state;
  {
    yat::AutoMutex<> guard(XbpmManager::m_lock);
    device_state = m_state;
  }
	return device_state;
}

// ============================================================================
// XbpmManager::get_status ()
// ============================================================================ 
std::string XbpmManager::get_status()
{
  std::string device_status;
  {
    yat::AutoMutex<> guard(XbpmManager::m_lock);
    device_status = m_status;
  }
	return device_status;
}

// ============================================================================
// XbpmManager::update_state_status ()
// ============================================================================ 
void XbpmManager::update_state_status() 
{
  std::string l_status_c = "";
  std::string l_status_p = "";
  std::string l_status = "";
  std::string l_status_nx = "";

  {
    yat::AutoMutex<> guard(XbpmManager::m_lock);
  
    try
    {
      m_state = get_device_state();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      m_state = Tango::FAULT;
      l_status = "Failed to get Device state";
    }
    catch (...)
    {
      ERROR_STREAM << "[Unknown exception] Failed to get Device state" << std::endl;
      m_state = Tango::FAULT;
      l_status = "Failed to get Device state";
    }
  }

  CHECK_CURRENT_DATA_GRABBER();
  // get current data grabber status
  try
  {
    l_status_c = m_current_data_grabber->get_status();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    l_status_c = "Failed to get current data grabber status";
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get current data grabber status" << std::endl;
    l_status_c = "Failed to get current data grabber status";
  }

  CHECK_POSITION_DATA_GRABBER();
  // get position data grabber status
  try
  {
    l_status_p = m_position_data_grabber->get_status();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    l_status_p = "Failed to get position data grabber status";
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get position data grabber status" << std::endl;
    l_status_p = "Failed to get position data grabber status";
  }
	
  // check nexus errors (if launched)
  if (m_nexus_manager)
  {
    if (m_nexus_cfg.nexusFileGeneration)
    {
      if (m_nexus_manager->getNexusStorageState())
      {
        l_status_nx = "Nexus recording in progress...";
      }
      else
      {
        l_status_nx = "Nexus recording not launched";
      }
      if (m_nexus_manager->hasStorageError())
      {
        l_status_nx += "\nERROR: Nexus exception handled!";
        // We should stop acquisition if nx problem and we are still RUNNING
        if (m_state == Tango::RUNNING)
        {
          try
          {
            ERROR_STREAM << "Stop acquisition on NEXUS error!" << std::endl;
            yat::Message * msg = yat::Message::allocate(MSG_STOP, MAX_USER_PRIORITY, true);
            post(msg, 500);
          }
          catch(...)
          {
          }
        }
      }
    }
    else
    {
      l_status_nx = "Nexus recording disabled";
    }
  }
  else
  {
    l_status_nx = "ERROR: Nexus manager not set!";
  }

  yat::OSStream oss;
  oss << l_status.c_str() << endl
    << "CURRENT data grabber: " << endl 
    << l_status_c.c_str() << endl 
    << "POSITION data grabber: " << endl
    << l_status_p.c_str() << endl << endl
    << "NEXUS manager: " << endl
    << l_status_nx.c_str() << endl;

  {
    yat::AutoMutex<> guard(XbpmManager::m_lock);
    m_status = oss.str();
  }
}

// ============================================================================
// XbpmManager::get_device_state ()
// ============================================================================ 
Tango::DevState XbpmManager::get_device_state() 
{
  Tango::DevState l_state_c;
  Tango::DevState l_state_p;
  Tango::DevState l_state;

  CHECK_CURRENT_DATA_GRABBER();
  // get current data grabber state
  try
  {
    l_state_c = m_current_data_grabber->get_state();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    l_state_c = Tango::FAULT;
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get current data grabber state" << std::endl;
    l_state_c = Tango::FAULT;
  }

  CHECK_POSITION_DATA_GRABBER();
  // get position data grabber state
  try
  {
    l_state_p = m_position_data_grabber->get_state();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    l_state_p = Tango::FAULT;
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get position data grabber state" << std::endl;
    l_state_p = Tango::FAULT;
  }

  // compose manager state & status according to grabbers' state & status
  if (l_state_c == Tango::FAULT || 
      l_state_p == Tango::FAULT)
  {
    l_state = Tango::FAULT;
  }
  else 
  {
    if (l_state_c == Tango::STANDBY)
    {
      l_state = Tango::STANDBY;
    }
    else 
    {
      if (l_state_c == Tango::ALARM)
      {
        l_state = Tango::ALARM;
      }
      else 
      {
        if (l_state_c == Tango::RUNNING)
        {
          l_state = Tango::RUNNING;
        }
        else
        {
          l_state = Tango::UNKNOWN;
        }
      }
    }
  }

  return (l_state);
}

// ============================================================================
// XbpmManager::init ()
// ============================================================================ 
void XbpmManager::init(XbpmConfig& cfg)
{
	m_cfg = cfg;

	try
	{
		m_current_data_grabber = new CurrentDataGrabber(cfg.hostDevice, m_dyn_attr_manager);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Creation of currentDataGrabber failed", 
			"XbpmManager::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Creation of currentDataGrabber failed" << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Creation of currentDataGrabber failed",
			"XbpmManager::init");
	}

	// test the current data grabber
	if (!m_current_data_grabber)
	{
		ERROR_STREAM << "initialization failed - the current data grabber is not created" << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Creation of currentDataGrabber failed",
			"XbpmManager::init");
	}

	try
	{
		m_current_data_grabber->init(m_cfg);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Initialization of currentDataGrabber failed", 
			"XbpmManager::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "Initialization of currentDataGrabber failed" << std::endl;
		THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Initialization of currentDataGrabber failed",
			"XbpmManager::init");
	}

  // In scan mode, no periodic activity
  if (!m_cfg.scanMode)
  {
    // if no defined user polling period, use SAI integration time value
    if (m_cfg.userPollingPeriod == 0.0)
    {
	  // get integration time to configure periodic activity
	  try
	  {
        m_integrationTime = m_current_data_grabber->get_sai_integration_time();
        DEBUG_STREAM << "SAI integration time: " << m_integrationTime << endl;
        m_useDefaultPP = false;
	  }
	  catch (Tango::DevFailed &e)
	  {
        ERROR_STREAM << e << std::endl;
        INFO_STREAM << "Use default integration time instead: " << m_integrationTime << std::endl;
        // if pb, use default value for it, in order to start the periodic task
	  }
	  catch (...)
	  {
        ERROR_STREAM << "Failed to read SAI integration time" << std::endl;
        INFO_STREAM << "Use default integration time instead: " << m_integrationTime << std::endl;
	  }
    }
    else // use user polling period
    {
      if (m_cfg.userPollingPeriod < 0)
      {
        ERROR_STREAM << "User polling period should not be negative!" << std::endl;
        THROW_DEVFAILED("CONFIGURATION_ERROR",
              "User polling period should not be negative!",
              "XbpmManager::init");
      }
      m_integrationTime = m_cfg.userPollingPeriod;
      DEBUG_STREAM << "User defined polling period: " << m_integrationTime << endl;
      m_useDefaultPP = false;
    }
  }

  try
  {
    m_position_data_grabber = new PositionDataGrabber(cfg.hostDevice, m_dyn_attr_manager);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Creation of positionDataGrabber failed", 
			"XbpmManager::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Creation of positionDataGrabber failed" << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Creation of positionDataGrabber failed",
			"XbpmManager::init");
  }

  // test the position data grabber
  if (!m_position_data_grabber)
  {
    ERROR_STREAM << "initialization failed - the position data grabber is not created" << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Creation of positionDataGrabber failed",
			"XbpmManager::init");
  }

  try
  {
    m_position_data_grabber->init(m_cfg);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Initialization of positionDataGrabber failed", 
			"XbpmManager::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Initialization of positionDataGrabber failed" << std::endl;
    THROW_DEVFAILED("CONFIGURATION_ERROR",
			"Initialization of positionDataGrabber failed",
			"XbpmManager::init");
  }
}

// ============================================================================
// XbpmManager::process_message
// ============================================================================
void XbpmManager::process_message (yat::Message& msg)
{
	//- handle msg
	switch (msg.type())
	{
		//- THREAD_INIT ----------------------
	case yat::TASK_INIT:
		{
			DEBUG_STREAM << "XbpmManager::process_message::THREAD_INIT::thread is starting up" << std::endl;
			init_i();
		} 
		break;

		//- THREAD_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "XbpmManager::process_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;

		//- THREAD_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			//DEBUG_STREAM << "XbpmManager::process_message::THREAD_PERIODIC" << std::endl;
			periodic_job_i();
		}
		break;
	
		//- MSG_START ------------------
	case MSG_START:
		{
			DEBUG_STREAM << "XbpmManager::process_message::MSG_START" << std::endl;
			start_i();
		}
		break;

		//- MSG_STOP ------------------
	case MSG_STOP:
		{
			DEBUG_STREAM << "XbpmManager::process_message::MSG_STOP" << std::endl;
			stop_i();
		}
		break;
		
		//- THREAD_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
			//- not used in this device
		}
		break;
		//- UNHANDLED MSG --------------------
	default:
		DEBUG_STREAM << "XbpmManager::process_message::unhandled msg type received" << std::endl;
		break;
	}
}

// ============================================================================
// XbpmManager::start ()
// ============================================================================ 
void XbpmManager::start()
{
  // in periodic mode, send message to avoid interrupting the periodic task
  if (!m_cfg.scanMode)
  {
    yat::Message * msg = yat::Message::allocate(MSG_START, MAX_USER_PRIORITY, true);
    try
    {
      wait_msg_handled(msg->duplicate(), 1500);
    }
    catch (...)
    {
      msg->release();
      throw;
    }
    msg->release();
  }
  else
  {
    start_i();
  }
}

// ============================================================================
// XbpmManager::start_i ()
// ============================================================================ 
void XbpmManager::start_i()
{
  // In scan mode, no periodic activity
  if (!m_cfg.scanMode)
  {
    if (m_cfg.userPollingPeriod == 0.0)
    {
      // get last integration time to (re)configure periodic activity
      try
      {
        m_integrationTime = m_current_data_grabber->get_sai_integration_time();
        DEBUG_STREAM << "New SAI integration time: " << m_integrationTime << endl;
        m_useDefaultPP = false;
      }
      catch (Tango::DevFailed &e)
      {
        ERROR_STREAM << e << std::endl;
        RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			  "Failed to read SAI integration time", 
			  "XbpmManager::start_i"); 
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to read SAI integration time" << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
			  "Failed to read SAI integration time",
			  "XbpmManager::start_i");
      }

      //- the XBPM polling period is set from the SAI integration time
      size_t period = (size_t)(m_integrationTime);
      DEBUG_STREAM << "Set new polling period: " << period << endl;
      enable_periodic_msg(false);
      set_periodic_msg_period(period);
      enable_periodic_msg(true);
    }
  }

  // reset history data
  m_histo_size = 0;
  m_sai_histos.clear_all();

  // initialize nexus manager (if nexus recording enabled)
  if (m_nexus_cfg.nexusFileGeneration)
  {
    try
    {
      // set item list
      nxItemList_t nx_items = build_nexus_item_list();
    
      // configure manager
      m_nexus_manager->initNexusAcquisition(
          m_nexus_cfg.nexusTargetPath,
          m_nexus_cfg.nexusFileName,
          0, // infinite storage
          m_nexus_cfg.nexusNbPerFile,
          nx_items);
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << df << std::endl;
      RETHROW_DEVFAILED(df, "DEVICE_ERROR",
          "Nexus manager init failed!",
          "XbpmManager::start_i");
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to initialize Nexus manager, caught [...]!" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
          "Failed to initialize Nexus manager, caught [...]!",
          "XbpmManager::start_i");
    }
  }

  // start acquisition for current & position grabbers
  try
  {
    m_current_data_grabber->start();
    m_position_data_grabber->start();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			"start failed", 
			"XbpmManager::start_i"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to Start acquisition" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR",
			"Failed to Start acquisition",
			"XbpmManager::start_i");
  }

  // force internal state to RUNNING
  m_previousState = Tango::RUNNING;
}

// ============================================================================
// XbpmManager::stop ()
// ============================================================================ 
void XbpmManager::stop()
{
  // in periodic mode, send message to avoid interrupting the periodic task
  if (!m_cfg.scanMode)
  {
    yat::Message * msg = yat::Message::allocate(MSG_STOP, MAX_USER_PRIORITY, true);
    try
    {
      wait_msg_handled(msg->duplicate(), 1500);
    }
    catch (...)
    {
      msg->release();
      throw;
    }
    msg->release();
  }
  else
  {
    stop_i();
  }
}

// ============================================================================
// XbpmManager::stop_i ()
// ============================================================================ 
void XbpmManager::stop_i() 
{
  try
  {
    m_current_data_grabber->stop();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			"stop failed", 
			"XbpmManager::stop"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to Stop acquisition" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR",
			"Failed to Stop acquisition",
			"XbpmManager::stop");
  }

  // Finalize Nexus file
  if (m_nexus_manager && m_nexus_cfg.nexusFileGeneration && m_nexus_manager->getNexusStorageState())
  {
    try
    {
      INFO_STREAM << "Finalize NEXUS generation..." << std::endl;
      m_nexus_manager->finalizeNexusGeneration();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << "Stop Nexus failed: " << df << std::endl;
    }
  }
}

// ============================================================================
// XbpmManager::periodic_job_i ()
// ============================================================================ 
void XbpmManager::periodic_job_i()
{
  CHECK_POSITION_DATA_GRABBER();
  CHECK_CURRENT_DATA_GRABBER();

  // get device state without updating interface for synchronization
  // reasons (ex: scanServer)
  Tango::DevState cState = get_device_state();

  if (Tango::FAULT != cState)
  {
    // get gain from electrometer if Device not in FAULT state
    try
    {
      m_gain = m_current_data_grabber->get_gain();
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to get gain" << std::endl;
      m_gain = yat::IEEE_NAN;
      // update device state & status
      update_state_status();
    }

    // update polling period if we previously use default value
    if (!m_cfg.scanMode && m_useDefaultPP && (m_cfg.userPollingPeriod == 0.0))
    {
      // get integration time to configure periodic activity
      try
      {
        m_integrationTime = m_current_data_grabber->get_sai_integration_time();
        DEBUG_STREAM << "New SAI integration time: " << m_integrationTime << endl;
        m_useDefaultPP = false;

        // set new polling period
        size_t period = (size_t)(m_integrationTime);
        set_periodic_msg_period(period);
      }
      catch (Tango::DevFailed &e)
      {
        // nothing to do, continue to use default value
      }
      catch (...)
      {
        // nothing to do, continue to use default value
      }
    }
  }

  // do the computing job if :
  // ° acquisition currently running 
  // ° or has just stopped
  // ° or we are in scan mode
  if ((Tango::RUNNING == cState) ||
      ((Tango::STANDBY == cState) && (Tango::RUNNING == m_previousState)) ||
      (m_cfg.scanMode))
  {
    // get voltage values from current data grabber
    try
    {
      m_voltage_data = m_current_data_grabber->get_voltage_data();
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to get voltages data" << std::endl;
      // update device state & status
      update_state_status();
    }

    // check for autoRange
    if (m_autoRange)
    {
      CHECK_CURRENT_DATA_GRABBER();

      try
      {
        m_current_data_grabber->update_range();
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to update range" << std::endl;
        // update device state & status
        update_state_status();
      }
    }

    // compute average currents & intensity from voltages
    try
    {
      m_current_data = m_current_data_grabber->compute_current_data(m_nexus_cfg.nexusFileGeneration && m_nexus_storage.intensity);
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << df << std::endl;
      RETHROW_DEVFAILED(df,"DEVICE_ERROR",
				"Failed to compute average currents", 
				"XbpmManager::periodic_job_i"); 
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to compute average currents" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to compute average currents",
				"XbpmManager::periodic_job_i");
    }

    // compute intensity alarms
    compute_intensity_alarms();

    // get SAI historics for intensity & position historics computing
    bool l_sai_reset_detected = false;
    if (m_cfg.enableIntensityHistory || m_cfg.enablePositionHistory)
    {
      // reset histo size if SAI restart detected
      if ((Tango::STANDBY == m_previousState) && (Tango::RUNNING == cState))
      {
        // reset history data
        m_histo_size = 0;
        m_sai_histos.clear_all();
        l_sai_reset_detected = true;
        DEBUG_STREAM << "Detected new acquisition: reset historics! " << std::endl;
      }

      try
      {
        // get new values from raw historics
        get_sai_historics(l_sai_reset_detected); // m_sai_histos contains raw data to be used in XBPM historics
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to get SAI currents historics" << std::endl;
        // update device state & status
        update_state_status();
      }
    }

    // compute other current data (if needed)
    if (m_cfg.enableSigmaIntensity ||
        m_cfg.enableFluxComputing || 
        m_cfg.enableIntensityHistory)
    {
      CHECK_CURRENT_DATA_GRABBER();
      try
      {
        m_current_data_grabber->compute_dynamic_current_data(m_sai_histos,
                 l_sai_reset_detected, 
                 m_nexus_cfg.nexusFileGeneration && m_nexus_storage.flux,
                 m_nexus_cfg.nexusFileGeneration && m_nexus_storage.intensity_history);
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to compute dynamics currents data" << std::endl;
        // update device state & status
        update_state_status();
      }
    }

    CHECK_POSITION_DATA_GRABBER();
    try
    {
      // set gain value before computing position
      m_position_data_grabber->set_electrometer_gain(m_gain);

      // compute position from current data
      m_position = m_position_data_grabber->get_position(m_current_data, 
                         m_nexus_cfg.nexusFileGeneration && m_nexus_storage.position);
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << df << std::endl;
      RETHROW_DEVFAILED(df,"DEVICE_ERROR",
				"Failed to get position", 
				"XbpmManager::periodic_job_i");
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to compute position!" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to compute position!",
				"XbpmManager::periodic_job_i");
    }

    // compute other position data (if needed)
    if (m_cfg.enablePositionHistory)
    {
      try
      {
        m_position_data_grabber->compute_dynamic_position_data(m_sai_histos,  
             l_sai_reset_detected, m_nexus_cfg.nexusFileGeneration && m_nexus_storage.position_history);
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to compute dynamic position data!" << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
              "Failed to compute dynamic position data!",
              "XbpmManager::periodic_job_i");
      }
    }
  }

  // Stop nexus recording if end of acquisition detected
  if (((Tango::STANDBY == cState) && (Tango::RUNNING == m_previousState)) && m_nexus_cfg.nexusFileGeneration)
  {
    if (!m_nexus_manager)
    {
      ERROR_STREAM << "Stop Nexus failed: manager not set!" << std::endl;
    }
    else
    {
      try
      {
        DEBUG_STREAM << "End of acquisition detected: finalize NEXUS generation..." << std::endl;
        m_nexus_manager->finalizeNexusGeneration();
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "Stop Nexus failed: " << df << std::endl;
      }
    }
  }

  m_previousState = cState;

  // update device state & status
  update_state_status();
}

// ============================================================================
// XbpmManager::set_autorange ()
// ============================================================================ 
void XbpmManager::set_autorange(bool auto_range)
{
  m_autoRange = auto_range;
}

// ============================================================================
// XbpmManager::get_autorange ()
// ============================================================================ 
bool XbpmManager::get_autorange()
{
  return m_autoRange;
}

// ============================================================================
// XbpmManager::get_gain ()
// ============================================================================ 
double XbpmManager::get_gain()
{
  return m_gain;
}

// ============================================================================
// XbpmManager::init_i ()
// ============================================================================ 
void XbpmManager::init_i()
{
  //- update all for the first time - means exec. period job
  try
  {
    //- do the periodic job
    periodic_job_i();
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			"Failed to update data for 1st time", 
			"XbpmManager::init_i"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to do periodic job" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to update data for 1st time", 
			"XbpmManager::init_i"); 
  }

  // In scan mode, no periodic activity
  if (!m_cfg.scanMode)
  {
    //- the XBPM polling period is set from the SAI integration time
    size_t period = (size_t)(m_integrationTime);
    set_periodic_msg_period(period);
	  enable_periodic_msg(true);
  }
}

// ============================================================================
// XbpmManager::set_current_unit ()
// ============================================================================ 
void XbpmManager::set_current_unit(double unit) 
{
  CHECK_CURRENT_DATA_GRABBER();

  try
  {
    m_unit = unit;
    m_current_data_grabber->set_current_unit(m_unit);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			"Failed to set current unit", 
			"XbpmManager::set_current_unit"); 
  }
  catch (...)
  {
    ERROR_STREAM << "failed to set current unit" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to set current unit", 
			"XbpmManager::set_current_unit"); 
  }
}

// ============================================================================
// XbpmManager::set_computation_mode ()
// ============================================================================ 
void XbpmManager::set_computation_mode(unsigned short mode) 
{
  // In scan mode, no periodic activity
  if (!m_cfg.scanMode)
  {
	enable_periodic_msg(false);
  }

  CHECK_POSITION_DATA_GRABBER();

  try
  {
    m_position_data_grabber->set_computation_mode(mode);
    m_computation_mode = mode;
  }
  catch (Tango::DevFailed & df)
  {
    {
      yat::AutoMutex<> guard(XbpmManager::m_lock);
      m_status = "new computation mode error [failed to initialize]";
      m_state = Tango::FAULT;
    }
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			"Failed to set computation mode", 
			"XbpmManager::set_computation_mode"); 
  }
  catch (...)
  {
    {
      yat::AutoMutex<> guard(XbpmManager::m_lock);
      m_status = "new computation mode error [failed to initialize]";
      m_state = Tango::FAULT;
    }
    ERROR_STREAM << "failed to set computation mode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to set computation mode", 
			"XbpmManager::set_computation_mode"); 
  }

  // In scan mode, no periodic activity
  if (!m_cfg.scanMode)
  {
	enable_periodic_msg(true);
  }
}

// ============================================================================
// XbpmManager::get_position_data ()
// ============================================================================ 
Position_t XbpmManager::get_position_data()
{
  return m_position;
}

// ============================================================================
// XbpmManager::get_voltage_data ()
// ============================================================================ 
Voltages_t XbpmManager::get_voltage_data()
{
  return m_voltage_data;
}

// ============================================================================
// XbpmManager::get_currents_data ()
// ============================================================================ 
Currents_t XbpmManager::get_current_data()
{
  return m_current_data;
}

// ============================================================================
// XbpmManager::get_sai_historics ()
// ============================================================================ 
void XbpmManager::get_sai_historics(bool& histo_reset)
{
  CHECK_CURRENT_DATA_GRABBER();

  // get voltage historics from SAI & transform to currents
  Currents_Histos_t raw_sai_histos;
  raw_sai_histos = m_current_data_grabber->get_current_historics();
  size_t l_buff_size = raw_sai_histos.histo0.length();

  // extract data & update histos member
  m_sai_histos.clear_all();

  // keep only new buffers' values regarding to previous buffer size    
  if (l_buff_size > m_histo_size) // current size > previous size: append new data
  {
    DEBUG_STREAM << "Got " << l_buff_size - m_histo_size << " NEW data !! " << std::endl;
    m_sai_histos.histo0.capacity(l_buff_size - m_histo_size);
    m_sai_histos.histo0.force_length(l_buff_size - m_histo_size);
    m_sai_histos.histo1.capacity(l_buff_size - m_histo_size);
    m_sai_histos.histo1.force_length(l_buff_size - m_histo_size);
    m_sai_histos.histo2.capacity(l_buff_size - m_histo_size);
    m_sai_histos.histo2.force_length(l_buff_size - m_histo_size);
    m_sai_histos.histo3.capacity(l_buff_size - m_histo_size);
    m_sai_histos.histo3.force_length(l_buff_size - m_histo_size);

    for (unsigned int l_cpt = m_histo_size; l_cpt < l_buff_size; l_cpt++)
    {
     m_sai_histos.histo0[l_cpt - m_histo_size] = raw_sai_histos.histo0[l_cpt];
      m_sai_histos.histo1[l_cpt - m_histo_size] = raw_sai_histos.histo1[l_cpt];
      m_sai_histos.histo2[l_cpt - m_histo_size] = raw_sai_histos.histo2[l_cpt];
      m_sai_histos.histo3[l_cpt - m_histo_size] = raw_sai_histos.histo3[l_cpt];
    }
    m_histo_size = l_buff_size;
  }
  else if (l_buff_size < m_histo_size) // current size < previous size: reset & get whole new data
  {
    DEBUG_STREAM << "Detected new buffer: reset & get " << l_buff_size << " NEW data !! " << std::endl;
    m_sai_histos = raw_sai_histos;
    m_histo_size = l_buff_size;
    histo_reset = true;
  }
  else // current size = previous size: nothing to do
  {
    // nothing to do...
  }
}

// ============================================================================
// XbpmManager::compute_intensity_alarms ()
// ============================================================================ 
void XbpmManager::compute_intensity_alarms()
{
  // check for intensity in alarm
  if (m_current_data.intensity > m_cfg.intensityThreshold)
  {
    m_current_data.intensityInAlarm = true;
  }
  else
  {
    m_current_data.intensityInAlarm = false;
  }
}

// ============================================================================
// XbpmManager::force_update ()
// ============================================================================ 
void XbpmManager::force_update()
{
  //- In scan mode only, update data - means exec. period job
  //- In polling mode, nothing to do (it's the TASK_PERIODIC job)
  if (m_cfg.scanMode)
  {
    try
    {
      //- do the periodic job
      periodic_job_i();
    }
    catch (Tango::DevFailed & df)
    {
      ERROR_STREAM << df << std::endl;
      RETHROW_DEVFAILED(df,"DEVICE_ERROR", 
			  "Failed to update data", 
			  "XbpmManager::force_update"); 
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to do periodic job" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR", 
			  "Failed to update data", 
			  "XbpmManager::force_update"); 
    }
  }
}

// ============================================================================
// XbpmManager::init_nexus ()
// ============================================================================ 
void XbpmManager::init_nexus(NexusConfig nx_cfg)
{
  m_nexus_cfg = nx_cfg;

  // create nexus manager
  m_nexus_manager = NULL;
  m_nexus_manager = new NexusManager(m_cfg.hostDevice);

  if (!m_nexus_manager)
  {
    ERROR_STREAM << "Failed to create Nexus manager!" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR",
          "Failed to create Nexus manager!",
          "XbpmManager::XbpmManager");
  }

  // set nexus manager in current & position grabbers
  CHECK_CURRENT_DATA_GRABBER();
  m_current_data_grabber->init_nexus(m_nexus_manager);

  CHECK_POSITION_DATA_GRABBER();
  m_position_data_grabber->init_nexus(m_nexus_manager);
}

// ============================================================================
// XbpmManager::set_nexus_file_path ()
// ============================================================================ 
void XbpmManager::set_nexus_file_path(std::string path)
{
  m_nexus_cfg.nexusTargetPath = path;
}

// ============================================================================
// XbpmManager::set_nexus_file_generation ()
// ============================================================================ 
void XbpmManager::set_nexus_file_generation(bool enable)
{
  m_nexus_cfg.nexusFileGeneration = enable;
}

// ============================================================================
// XbpmManager::set_nexus_nb_per_file ()
// ============================================================================ 
void XbpmManager::set_nexus_nb_per_file(yat::uint32 nb)
{
  m_nexus_cfg.nexusNbPerFile = nb;
}

// ============================================================================
// XbpmManager::build_nexus_item_list ()
// ============================================================================ 
nxItemList_t XbpmManager::build_nexus_item_list()
{
  nxItemList_t nx_items;
  m_nexus_storage.reset();

  // parse list of data types to archive
  for (size_t idx = 0; idx < m_nexus_cfg.nexusDataTypes.size(); idx++)
  {
    if (m_nexus_cfg.nexusDataTypes[idx].compare(NX_POSITION) == 0)
    {
      m_nexus_storage.position = true;

      nxItem item1;
      item1.name = m_nexus_cfg.nexusFileName + "_" + H_POS;
      item1.storageType = NX_DATA_TYPE_0D;
      item1.dim1 = 0;
      item1.dim2 = 0;
      nx_items.push_back(item1);

      nxItem item2;
      item2.name = m_nexus_cfg.nexusFileName + "_" + V_POS;
      item2.storageType = NX_DATA_TYPE_0D;
      item2.dim1 = 0;
      item2.dim2 = 0;
      nx_items.push_back(item2);
    }
    if (m_nexus_cfg.nexusDataTypes[idx].compare(NX_INTENSITY) == 0)
    {
      m_nexus_storage.intensity = true;

      nxItem item;
      item.name = m_nexus_cfg.nexusFileName + "_" + INTENSITY;
      item.storageType = NX_DATA_TYPE_0D;
      item.dim1 = 0;
      item.dim2 = 0;
      nx_items.push_back(item);
    }
    if (m_nexus_cfg.nexusDataTypes[idx].compare(NX_FLUX) == 0)
    {
      m_nexus_storage.flux = true;

      nxItem item;
      item.name = m_nexus_cfg.nexusFileName + "_" + FLUX;
      item.storageType = NX_DATA_TYPE_0D;
      item.dim1 = 0;
      item.dim2 = 0;
      nx_items.push_back(item);
    }
    if (m_nexus_cfg.nexusDataTypes[idx].compare(NX_POSITION_HISTORY) == 0)
    {
      m_nexus_storage.position_history = true;

      nxItem item1;
      item1.name = m_nexus_cfg.nexusFileName + "_" + H_POS_HIS;
      item1.storageType = NX_DATA_TYPE_0D;
      item1.dim1 = 0;
      item1.dim2 = 0;
      nx_items.push_back(item1);

      nxItem item2;
      item2.name = m_nexus_cfg.nexusFileName + "_" + V_POS_HIS;
      item2.storageType = NX_DATA_TYPE_0D;
      item2.dim1 = 0;
      item2.dim2 = 0;
      nx_items.push_back(item2);
    }
    if (m_nexus_cfg.nexusDataTypes[idx].compare(NX_INTENSITY_HISTORY) == 0)
    {
      m_nexus_storage.intensity_history = true;

      nxItem item;
      item.name = m_nexus_cfg.nexusFileName + "_" + INTENSITY_HIS;
      item.storageType = NX_DATA_TYPE_0D;
      item.dim1 = 0;
      item.dim2 = 0;
      nx_items.push_back(item);
    }
  }

  return nx_items;
}

// ============================================================================
// XbpmManager::reset_nexus_buffer_index ()
// ============================================================================ 
void XbpmManager::reset_nexus_buffer_index()
{
  CHECK_NX_MANAGER();
  m_nexus_manager->resetNexusBufferIndex();
}

} // namespace Xbpm_ns
