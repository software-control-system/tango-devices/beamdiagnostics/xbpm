//=============================================================================
// TableReader.h
//=============================================================================
// abstraction.......TableReader 
// class.............TableReader
// original author.... S. GARA - NEXEYA
//=============================================================================

#ifndef _TABLE_READER_H_
#define _TABLE_READER_H_

#pragma once
//=============================================================================
// DEPENDENCIES
//=============================================================================
#include "XbpmTypesAndConsts.h"
/*Use Interpolator library*/
#include "Table1D.h"
#include "Table2D.h"
#include <map>
#include <yat/any/Any.h>

namespace Xbpm_ns
{
//- Class for table reading
class TableReader : public Tango::LogAdapter
{
public:

	// ctor
	TableReader(Tango::DeviceImpl * p_dev, std::string p_path_table, std::string p_title, bool p_is_2D_table, vector<int> p_index_list);

	// dtor
	~TableReader();

	// returns value from 1D table
	double get_interpolated_value(double p_val);

	// returns value from 2D table
	double get_interpolated_value(double p_val_1,double p_val_2);

    // gets number of data in 1D or 2D table
    void get_nb_data(long & nb_data);
    void get_nb_data(long & nb_x_data, long & nb_y_data);

private:
	//- interpolator 1D table
	Interpolator::Table1D * m_table_1D;

	//- interpolator 2D table
	Interpolator::Table2D * m_table_2D;

	//- file path
	string m_path;

	//- title
	string m_title;

	//- is 2d table?
	bool m_is_2D_table;

	//- index vector
	vector<int> m_index_list;
};

} // namespace Xbpm_ns

#endif // _TABLE_READER_H_