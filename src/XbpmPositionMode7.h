//=============================================================================
// XbpmPositionMode7.h
//=============================================================================
// abstraction.......XbpmPositionMode7 for XbpmPositionModes
// class.............XbpmPositionMode7
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_POSITION_MODE7
#define _XBPM_POSITION_MODE7

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionMode7
// ============================================================================
class XbpmPositionMode7 : public XbpmPositionInterface
{

public:

  //- constructor
  XbpmPositionMode7(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~XbpmPositionMode7();

  //- init
  void init();

  //- gets xbpm manager state
  Tango::DevState get_state();
  
  //- gets status
  std::string get_status();

  //-gets properties
  void get_properties();
  
  //-gets position
  Position_t get_position(Currents_t p_currents);
  
private:
  
  //-computes x position
  void compute_x_position_i();
  
  //-computes z position
  void compute_z_position_i();

  //- configuration
  XbpmConfig m_cfg;

  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;

  //- kx factor
  double m_kx_factor;

  //- kz factor
  double m_kz_factor;

  //- offset x
  double m_offset_x;

  //- offset z
  double m_offset_z;

  //- position
  Position_t m_position;

  //- Currents
  Currents_t m_currents;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_MODE7
