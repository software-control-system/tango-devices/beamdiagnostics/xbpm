//=============================================================================
// XbpmPositionInterfaceFactory.h
//=============================================================================
// abstraction.......XbpmPositionInterfaceFactory for XbpmPositionMode
// class.............XbpmPositionInterfaceFactory
// original author...S.GARA - Nexey
//=============================================================================

#ifndef _XBPM_POSITION_INTERFACE_FACTORY_H_
#define _XBPM_POSITION_INTERFACE_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"

namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionInterfaceFactory
// ============================================================================
class XbpmPositionInterfaceFactory
{
public: 
  //- instanciate a specialized XbpmPosition Interface
  static XbpmPositionInterface * instanciate(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);
  
private:
  XbpmPositionInterfaceFactory();
  ~XbpmPositionInterfaceFactory();
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_INTERFACE_FACTORY_H_
