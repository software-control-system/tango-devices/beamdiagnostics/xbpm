//=============================================================================
// PositionDataGrabber.h
//=============================================================================
// abstraction.......Position Data Grabber for Xbpm
// class.............PositionDataGrabber
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _POSITION_DATA_GRABBER_H
#define _POSITION_DATA_GRABBER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "XbpmPositionInterfaceFactory.h"
#include "NexusManager.h"
#include <yat4tango/DynamicAttribute.h>
#include <yat4tango/DynamicAttributeManager.h>


namespace Xbpm_ns
{

// ============================================================================
// class: PositionDataGrabber
// ============================================================================
class PositionDataGrabber : public Tango::LogAdapter
{

public:

  //- constructor
  PositionDataGrabber(Tango::DeviceImpl * hostDevice, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~PositionDataGrabber();

  //- gets state
  Tango::DevState get_state();

  //- gets status
  std::string get_status();

  //- initialization
  void init(XbpmConfig& cfg);

  //- set nexus manager for recording
  void init_nexus(NexusManager * nx_mgr);

  //- sets new computation mode
  void set_computation_mode(unsigned short mode);

  //- starts the position computation (init history)
  void start();

  //- gets position
  //- set nx enabled flag to true if recording is enabled
  Position_t get_position(Currents_t p_currents, bool nx_enabled);

  //- sets new electrometer gain value
  void set_electrometer_gain (double gain);

  //- computes dynamic position data
  //- set reset flag to true if history reset is needed
  //- set nx enabled flag to true if recording is enabled
  void compute_dynamic_position_data(Currents_Histos_t sai_currents, bool histo_reset_needed, bool nx_enabled);

  //- read callback for dyn attr position history H
  void read_history_position_h(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr position history V
  void read_history_position_v(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr position std deviation H
  void read_std_position_h(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr position std deviation V
  void read_std_position_v(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr position history depth
  void read_position_histo_depth(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr position history depth
  void write_position_histo_depth(yat4tango::DynamicAttributeWriteCallbackData & cbd);

private:
    //- loads new position mode
  void load_position_mode();

  //- unloads current position mode
  void unload_position_mode();

  //- computes position standard deviation
  void compute_position_std_deviation();

  //- configuration
  XbpmConfig m_cfg;

  //- dyn attr manager
  yat4tango::DynamicAttributeManager* m_dyn_attr_manager;

  //- xbpm position mode interface
  XbpmPositionInterface * m_position_mode_interface;

  //- current position
  Position_t m_position;

  //- dynamic position data
  DynamicPositionData_t m_dynamic_position_data;

  //- position history H
  History_t m_position_history_h;

  //- position history V
  History_t m_position_history_v;

  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;

  //- thread safety
  yat::Mutex m_lock;

  //- computation mode
  unsigned short m_computation_mode;

  //- Nexus manager
  NexusManager * m_storage_mgr;
};

} // namespace Xbpm_ns

#endif // _POSITION_DATA_GRABBER_H
