//=============================================================================
// XbpmPositionMode1.cpp
//=============================================================================
// abstraction.......XbpmPositionMode1 for XbpmPositionModes
// class.............XbpmPositionMode1
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode1.h"

namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode1::XbpmPositionMode1 ()
// ============================================================================ 
XbpmPositionMode1::XbpmPositionMode1 (const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
  m_beam_energy_proxy_name = "";
  m_energy_attr = "";
  m_beam_size_table = "";
  m_offset_table = "";
  m_detector_gap = 0;
  m_dev_beam_energy = NULL;
  m_offset_table_x_reader = NULL;
  m_offset_table_z_reader = NULL;
  m_beam_table_x_reader = NULL;
  m_beam_table_z_reader = NULL;
  m_beam_energy_in_fault = false;
  m_state = Tango::UNKNOWN;
  m_status = "";
  m_beam_energy_in_fault = false;
  m_internal_error = false;
  m_is_dyn_beamsize_table = false;
  m_is_dyn_offset_table = false;
  m_is_beamsize_table_empty = true;
  m_is_offset_table_empty = true;
}

// ============================================================================
// XbpmPositionMode1::~XbpmPositionMode1 ()
// ============================================================================ 
XbpmPositionMode1::~XbpmPositionMode1 ()
{
	if (m_dev_beam_energy)
	{
		delete m_dev_beam_energy;
        m_dev_beam_energy = NULL;
	}
	if (m_offset_table_x_reader)
	{
		delete m_offset_table_x_reader;
        m_offset_table_x_reader = NULL;
	}
	if (m_offset_table_z_reader)
	{
		delete m_offset_table_z_reader;
        m_offset_table_z_reader = NULL;
	}
	if (m_beam_table_x_reader)
	{
		delete m_beam_table_x_reader;
        m_beam_table_x_reader = NULL;
	}
	if (m_beam_table_z_reader)
	{
		delete m_beam_table_z_reader;
        m_beam_table_z_reader = NULL;
	}
	if (m_is_dyn_offset_table)
	{
		m_dyn_attr_manager->remove_attribute(OFFSET_TABLE);
	}
	if (m_is_dyn_beamsize_table)
	{
		m_dyn_attr_manager->remove_attribute(BEAMSIZE_TABLE);
	}
}

// ============================================================================
// XbpmPositionMode1::init ()
// ============================================================================ 
void XbpmPositionMode1::init()
{
    m_beam_energy_in_fault = false;
    m_internal_error = false;

    // create beam energy device proxy
	try
	{
		m_dev_beam_energy = new Tango::DeviceProxy(m_beam_energy_proxy_name);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
        m_beam_energy_in_fault = true;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to create beam energy proxy", 
			"XbpmPositionMode1::init"); 
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to create beam energy proxy" << std::endl;
        m_beam_energy_in_fault = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to create beam energy proxy", 
			"XbpmPositionMode1::init"); 
	}

	if (!m_dev_beam_energy)
	{
		ERROR_STREAM << "Failed to access Beam Energy proxy" << std::endl;
		m_state = Tango::FAULT;
		m_beam_energy_in_fault = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Beam Energy proxy", 
			"XbpmPositionMode1::init");  
	}

	if (!m_is_offset_table_empty)
	{
    // create reader for Energy --> offsetX table
	  try
	  {
		  std::vector<int> l_list;
		  l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
		  l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
		  m_offset_table_x_reader = new TableReader(m_cfg.hostDevice, m_offset_table, "Energy -> Offset X",false,l_list);
	  }
	  catch (...)
	  {
		  ERROR_STREAM << "initialization failed - failed to create tableReader for offset table x" << std::endl;
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "initialization failed - failed to create tableReader for offset table x", 
			  "XbpmPositionMode1::init"); 
	  }

	  if (!m_offset_table_x_reader)
	  {
		  ERROR_STREAM << "Failed to access Offset x table reader" << std::endl;
		  m_state = Tango::FAULT;
		  m_status = "See log for error details"; 
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "Failed to access Offset x table reader", 
			  "XbpmPositionMode1::init");  
	  }

        // create reader for Energy --> Offset Z table
	  try
	  {
		  std::vector<int> l_list;
		  l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
		  l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
		  m_offset_table_z_reader = new TableReader(m_cfg.hostDevice, m_offset_table, "Energy -> Offset Z",false,l_list);
	  }
	  catch (...)
	  {
		  ERROR_STREAM << "initialization failed - failed to create tableReader for offset table z" << std::endl;
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "initialization failed - failed to create tableReader for offset table z", 
			  "XbpmPositionMode1::init"); 
	  }

	  if (!m_offset_table_z_reader)
	  {
		  ERROR_STREAM << "Failed to access Offset z table reader" << std::endl;
		  m_state = Tango::FAULT;
		  m_status = "See log for error details"; 
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "Failed to access Offset z table reader", 
			  "XbpmPositionMode1::init");  
	  }
	}

  if (!m_is_beamsize_table_empty)
  {
    // create reader for Energy --> beam size X table
	  try
	  {
		  std::vector<int> l_list;
		  l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
		  l_list.push_back(INDEX_BEAMX_IN_FILE_1D);
		  m_beam_table_x_reader = new TableReader(m_cfg.hostDevice, m_beam_size_table, "Energy -> beam size X",false,l_list);
	  }
	  catch (...)
	  {
		  ERROR_STREAM << "initialization failed - failed to create tableReader for beamsize table x" << std::endl;
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "initialization failed - failed to create tableReader for beamsize table x", 
			  "XbpmPositionMode1::init"); 
	  }

	  if (!m_beam_table_x_reader)
	  {
		  ERROR_STREAM << "Failed to access Beam x table reader" << std::endl;
		  m_state = Tango::FAULT;
		  m_status = "See log for error details"; 
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "Failed to access Beam x table reader", 
			  "XbpmPositionMode1::init");  
	  }

        // create reader for Energy --> beam size Z table
	  try
	  {
		  std::vector<int> l_list;
		  l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
		  l_list.push_back(INDEX_BEAMZ_IN_FILE_1D);
		  m_beam_table_z_reader = new TableReader(m_cfg.hostDevice, m_beam_size_table, "Energy -> beam size Z",false,l_list);
	  }
	  catch (...)
	  {
		  ERROR_STREAM << "initialization failed - failed to create tableReader for beamsize table z" << std::endl;
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "initialization failed - failed to create tableReader for beamsize table z", 
			  "XbpmPositionMode1::init"); 
	  }

	  if (!m_beam_table_z_reader)
	  {
		  ERROR_STREAM << "Failed to access Beam z table reader" << std::endl;
		  m_state = Tango::FAULT;
		  m_status = "See log for error details"; 
		  THROW_DEVFAILED("DEVICE_ERROR", 
			  "Failed to access Beam z table reader", 
			  "XbpmPositionMode1::init");  
	  }
	}

    // init OK
    m_state = Tango::ON;
}

// ============================================================================
// XbpmPositionMode1::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode1::get_state()
{
	if (m_dev_beam_energy)
	{
    // get beam energy state
		try
		{
            m_beam_energy_in_fault = false;
			m_state_beam_energy = m_dev_beam_energy->state();
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
            m_beam_energy_in_fault = true;
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get beam energy device state" << std::endl;
            m_beam_energy_in_fault = true;
		}

        // update module state
        if (m_internal_error || 
            m_beam_energy_in_fault ||
            (m_state_beam_energy == Tango::FAULT))
        {
            m_state = Tango::FAULT;
        }
        else
        {
            m_state = Tango::ON;
        }
	}

	return m_state;
}

// ============================================================================
// XbpmPositionMode1::get_status ()
// ============================================================================ 
std::string XbpmPositionMode1::get_status()
{
  // compose module general status
  switch (m_state)
  {
    case Tango::RUNNING:
    case Tango::ON:
	case Tango::MOVING:
    {
      m_status = "current module state is: RUNNING\n";
    }
    break;
    case Tango::ALARM:
    {
      m_status = "current module state is: ALARM\n";
    }
    break;
    case Tango::STANDBY:
    case Tango::OFF:
    {
      m_status = "current module state is: STANDBY\n";
    }
    break;
    case Tango::FAULT:
    {
      m_status = "module has switched to FAULT state\n";
      if (m_beam_energy_in_fault)
        m_status += "Beam Energy Device error!\n";
      else if (m_internal_error)
        m_status += "Internal error!\n";
      else
        m_status += "Unknown error!\n";
    }
    break;
    default:
    {
      m_status = "Unhandled module state!!\n";
    }
    break;
  }

  // add detailed states
  if (!m_beam_energy_in_fault)
  {
    m_status += " --Beam Energy Device state: " + std::string(Tango::DevStateName[m_state_beam_energy]) + "\n";
  }

	return m_status;
}

// ============================================================================
// XbpmPositionMode1::get_properties ()
// ============================================================================ 
void XbpmPositionMode1::get_properties()
{
	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode1"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);

	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	bool l_is_mode1 = false;
	if (!dev_prop[0].is_empty())
	{
		std::vector<std::string> l_vect;
		ModeKeys_t l_keys;
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
			m_beam_energy_proxy_name = l_conf_parser.extractBeamEnergyProxyName(l_keys);
			m_energy_attr = l_conf_parser.extractEnergyAttr(l_keys);
			l_is_mode1 = true;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
                "CONFIGURATION_ERROR",
				"The property Mode1 must be filled with at least BeamEnergyProxyName and EnergyAttr", 
				"XbpmPositionMode1::get_properties"); 
		}
		catch (...)
        {
            THROW_DEVFAILED(
                "CONFIGURATION_ERROR", 
                "The property Mode1 must be filled with at least BeamEnergyProxyName and EnergyAttr", 
                "XbpmPositionMode1::get_properties"); 
        }
        if (l_is_mode1)
        {
            try
            {
                m_detector_gap = l_conf_parser.extractDetectorGap(l_keys);
            }
            catch (...)
            {
                INFO_STREAM << "No value for DetectorGap.Using default value : 0" << endl;
            }
            try
            {
                m_offset_table = l_conf_parser.extractOffsetTable(l_keys);
                m_is_offset_table_empty = false;
            }
            catch (...)
            {
				m_is_dyn_offset_table = true;
				//add dyn attr OffsetTable
				yat4tango::DynamicAttributeInfo dai;
				dai.dev = m_cfg.hostDevice;
				dai.tai.name = OFFSET_TABLE;
				dai.tai.label = OFFSET_TABLE;
				//- describe the dyn attr we want...
				dai.tai.data_type = Tango::DEV_STRING;
				dai.tai.data_format = Tango::SCALAR;
				dai.tai.writable = Tango::READ_WRITE;
				dai.tai.disp_level = Tango::EXPERT;
				dai.tai.description = "Path and name of the Offset table";
				//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
				dai.cdb = false;
				//- read callback
				dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
					&XbpmPositionMode1::read_OffsetTable);
				dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
					&XbpmPositionMode1::write_OffsetTable);
				m_dyn_attr_manager->add_attribute(dai);

                try
                {
                    std::string prop_name = std::string("__") + OFFSET_TABLE;
                    m_offset_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
                    m_is_offset_table_empty = false;
                }
                catch(...)
                {
                    // no memorized value, set default value
                    m_offset_table = kDEFAULT_FILE_NAME;
                }
            }
			try
            {
				m_beam_size_table = l_conf_parser.extractBeamSizeTable(l_keys);
				m_is_beamsize_table_empty = false;
            }
			catch (...)
			{
				m_is_dyn_beamsize_table = true;
				//add dyn attr BeamSizeTable
				yat4tango::DynamicAttributeInfo dai;
				dai.dev = m_cfg.hostDevice;
				dai.tai.name = BEAMSIZE_TABLE;
				dai.tai.label = BEAMSIZE_TABLE;
				//- describe the dyn attr we want...
				dai.tai.data_type = Tango::DEV_STRING;
				dai.tai.data_format = Tango::SCALAR;
				dai.tai.writable = Tango::READ_WRITE;
				dai.tai.disp_level = Tango::EXPERT;
				dai.tai.description = "Path and name of the BeamSize table";
				//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
				dai.cdb = false;
				//- read callback
				dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
					&XbpmPositionMode1::read_BeamsizeTable);
				dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
					&XbpmPositionMode1::write_BeamsizeTable);
				m_dyn_attr_manager->add_attribute(dai);

                try
                {
                    std::string prop_name = std::string("__") + BEAMSIZE_TABLE;
                    m_beam_size_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
                    m_is_beamsize_table_empty = false;
                }
                catch(...)
                {
                    // no memorized value, set default value
                    m_beam_size_table = kDEFAULT_FILE_NAME;
                }
            }
		}
	}
	else
	{
		THROW_DEVFAILED(
            "CONFIGURATION_ERROR", 
			"The property Mode1 must be filled", 
			"XbpmPositionMode1::get_properties"); 
	}
}

// ============================================================================
// XbpmPositionMode1::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode1::get_position(Currents_t p_currents)
{
	m_currents = p_currents;

	double l_energy = 0;

	//get energy from beam energy device
	if (!m_dev_beam_energy)
	{
		ERROR_STREAM << "Failed to access Beam Energy proxy" << std::endl;
        m_beam_energy_in_fault = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Beam Energy proxy", 
			"XbpmPositionMode1::get_position");  
	}

	try
	{
		Tango::DeviceAttribute l_attr;
		l_attr = m_dev_beam_energy->read_attribute(m_energy_attr);
		l_attr >> l_energy;
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
        m_beam_energy_in_fault = true;
		RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to get energy", 
			"XbpmPositionMode1::get_position");
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get energy" << std::endl;
		m_beam_energy_in_fault = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to get energy", 
			"XbpmPositionMode1::get_position");  
	}

	// get offset and coefficients values for the energy value
	if (!m_offset_table_x_reader)
	{
		ERROR_STREAM << "Failed to access Offset x table reader" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Offset x table reader", 
			"XbpmPositionMode1::get_position");  
	}

	try
	{
		m_offset_x = m_offset_table_x_reader->get_interpolated_value(l_energy);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for Kx" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for Kx", 
			"XbpmPositionMode1::get_position");  
	}

	if (!m_offset_table_z_reader)
	{
		ERROR_STREAM << "Failed to access Offset z table reader" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Offset z table reader", 
			"XbpmPositionMode1::get_position");  
	}

	try
	{
		m_offset_z = m_offset_table_z_reader->get_interpolated_value(l_energy);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for Kz" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for Kz", 
			"XbpmPositionMode1::get_position");  
	}

	double l_beam_x = 0.0;
	double l_beam_z = 0.0;

	if (!m_beam_table_x_reader)
	{
		ERROR_STREAM << "Failed to access Beam x table reader" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Beam x table reader", 
			"XbpmPositionMode1::get_position");  
	}

	try
	{
		l_beam_x = m_beam_table_x_reader->get_interpolated_value(l_energy);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for beamsize x" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for beamsize x", 
			"XbpmPositionMode1::get_position");  
	}

	if (!m_beam_table_z_reader)
	{
		ERROR_STREAM << "Failed to access Beam z table reader" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access Beam z table reader", 
			"XbpmPositionMode1::get_position");  
	}

	try
	{
		l_beam_z = m_beam_table_z_reader->get_interpolated_value(l_energy);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for beamsize z" << std::endl;
		m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for beamsize z", 
			"XbpmPositionMode1::get_position");  
	}

    // compute calibration factors
	m_kx = 0.5 * l_beam_x - m_detector_gap;
	m_kz = 0.5 * l_beam_z - m_detector_gap;

    // compute position
	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode1::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode1::compute_x_position_i()
{
	// X = [((I0 + I3) - (I1 + I2)) / Isum] * Kx_cal  + OffsetX_corrected_factor
	m_position.x = (((m_currents.current1 + m_currents.current4) - (m_currents.current2 + m_currents.current3)) / m_currents.intensity)* m_kx + m_offset_x;
}

// ============================================================================
// XbpmPositionMode1::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode1::compute_z_position_i()
{
	// Z = [((I0 + I1) - (I3 + I2)) / Isum ] * Kz_cal + OffsetZ_corrected_factor
	m_position.z = (((m_currents.current1 + m_currents.current2) - (m_currents.current4 + m_currents.current3)) / m_currents.intensity) * m_kz + m_offset_z;
}

// ============================================================================
// XbpmPositionMode1::read_OffsetTable ()
// ============================================================================ 
void XbpmPositionMode1::read_OffsetTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __offset_table__;
	static std::string __offset_table_str__;

	__offset_table_str__ = m_offset_table;
	__offset_table__ = const_cast<char*>(__offset_table_str__.c_str());  
	cbd.tga->set_value(&__offset_table__);
}

// ============================================================================
// XbpmPositionMode1::write_OffsetTable ()
// ============================================================================ 
void XbpmPositionMode1::write_OffsetTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __offset_table__;
	cbd.tga->get_write_value(__offset_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing OffsetTable", 
			"XbpmPositionMode1::write_OffsetTable"); 
	}

	if (m_offset_table.compare(__offset_table__) == 0)
	{
		DEBUG_STREAM << "Same value for OffsetTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__offset_table__))
		{
			if (m_offset_table_x_reader)
			{
				delete m_offset_table_x_reader;
			}
			if (m_offset_table_z_reader)
			{
				delete m_offset_table_z_reader;
			}

			// create reader for Energy --> offsetX table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
				m_offset_table_x_reader = new TableReader(m_cfg.hostDevice, __offset_table__, "Energy -> Offset X",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for offset table x" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for offset table x", 
					"XbpmPositionMode1::write_OffsetTable"); 
			}

			if (!m_offset_table_x_reader)
			{
				ERROR_STREAM << "Failed to access Offset x table reader" << std::endl;
				m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Failed to access Offset x table reader", 
					"XbpmPositionMode1::write_OffsetTable");  
			}

			// create reader for Energy --> Offset Z table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
				m_offset_table_z_reader = new TableReader(m_cfg.hostDevice, __offset_table__, "Energy -> Offset Z",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for offset table z" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for offset table z", 
					"XbpmPositionMode1::write_OffsetTable"); 
			}

			if (!m_offset_table_z_reader)
			{
				ERROR_STREAM << "Failed to access Offset z table reader" << std::endl;
				m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Failed to access Offset z table reader", 
					"XbpmPositionMode1::write_OffsetTable");  
			}
			m_offset_table = __offset_table__;
 			std::string prop_name = std::string("__") + OFFSET_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __offset_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_OffsetTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode1::write_OffsetTable"); 
		}
	}
}
// ============================================================================
// XbpmPositionMode1::read_BeamsizeTable ()
// ============================================================================ 
void XbpmPositionMode1::read_BeamsizeTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __beamsize_table__;
	static std::string __beamsize_table_str__;

	__beamsize_table_str__ = m_beam_size_table;
	__beamsize_table__ = const_cast<char*>(__beamsize_table_str__.c_str());  
	cbd.tga->set_value(&__beamsize_table__);
}

// ============================================================================
// XbpmPositionMode1::write_BeamsizeTable ()
// ============================================================================ 
void XbpmPositionMode1::write_BeamsizeTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	char * __beamsize_table__;
	cbd.tga->get_write_value(__beamsize_table__);
	// test if not the same value

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing BeamSizeTable", 
			"XbpmPositionMode1::write_BeamsizeTable"); 
	}

	if (m_beam_size_table.compare(__beamsize_table__) == 0)
	{
		DEBUG_STREAM << "Same value for BeamSizeTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__beamsize_table__))
		{
			if (m_beam_table_x_reader)
			{
				delete m_beam_table_x_reader;
			}
			if (m_beam_table_z_reader)
			{
				delete m_beam_table_z_reader;
			}

			// create reader for Energy --> beam size X table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
				l_list.push_back(INDEX_BEAMX_IN_FILE_1D);
				m_beam_table_x_reader = new TableReader(m_cfg.hostDevice, __beamsize_table__, "Energy -> beam size X",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for beamsize table x" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for beamsize table x", 
					"XbpmPositionMode1::init"); 
			}

			if (!m_beam_table_x_reader)
			{
				ERROR_STREAM << "Failed to access Beam x table reader" << std::endl;
				m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Failed to access Beam x table reader", 
					"XbpmPositionMode1::init");  
			}

			// create reader for Energy --> beam size Z table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
				l_list.push_back(INDEX_BEAMZ_IN_FILE_1D);
				m_beam_table_z_reader = new TableReader(m_cfg.hostDevice, __beamsize_table__, "Energy -> beam size Z",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for beamsize table z" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for beamsize table z", 
					"XbpmPositionMode1::init"); 
			}

			if (!m_beam_table_z_reader)
			{
				ERROR_STREAM << "Failed to access Beam z table reader" << std::endl;
				m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"Failed to access Beam z table reader", 
					"XbpmPositionMode1::init");  
			}
			m_beam_size_table = __beamsize_table__;
 			std::string prop_name = std::string("__") + BEAMSIZE_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __beamsize_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_BeamsizeTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode1::write_BeamsizeTable"); 
		}
	}
}
} // namespace Xbpm_ns
