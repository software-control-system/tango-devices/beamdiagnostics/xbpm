//=============================================================================
// XbpmTypesAndConsts.cpp
//=============================================================================
// abstraction.......XBPM 
// class.............XbpmTypesAndConsts
// original author.... S. GARA - NEXEYA
//=============================================================================

#include "XbpmTypesAndConsts.h"

namespace Xbpm_ns
{
  // standard deviation computing
  double std_dev_calculation(yat::Buffer<double> p_buff, double l_val)
  {
    double l_sum = 0.0;
    double l_size = p_buff.length();

    // standard deviation calculation on p_buff buffer
	for (unsigned int l_cpt = 0; l_cpt < l_size; l_cpt++)
	{
      l_sum += pow(p_buff[l_cpt] - l_val, 2) / l_size;
    }
  	
    return sqrt(l_sum);
  }

  // file readable?
  bool is_file_readable( const std::string & file ) 
  { 
    std::ifstream fichier( file.c_str() ); 
    return !fichier.fail(); 
  }
}

