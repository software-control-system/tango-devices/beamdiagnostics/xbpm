//=============================================================================
// TableReader.cpp
//=============================================================================
// abstraction.......TableReader 
// class.............TableReader
// original author.... S. GARA - NEXEYA
//=============================================================================

#include "TableReader.h"
#include <yat4tango/DeviceTask.h>

namespace Xbpm_ns
{
//-----------------------------------------------------------------------------
//- TableReader::TableReader
//-----------------------------------------------------------------------------
TableReader::TableReader(Tango::DeviceImpl* p_dev, std::string p_path, std::string p_title, bool p_is_2D_table, vector<int> p_index_list)
 :Tango::LogAdapter(p_dev), 
  m_path(p_path), 
  m_title(p_title), 
  m_is_2D_table(p_is_2D_table), 
  m_index_list(p_index_list)
{
	m_table_1D = NULL;
	m_table_2D = NULL;

	try
	{
		if (!m_is_2D_table)
		{
			m_table_1D = new Interpolator::Table1D(m_title,
				m_title,
				TABLE_1D_INTERPOLATION,
				string(m_path),
				m_index_list[0],
				m_index_list[1]);
		}
		else
		{
			m_table_2D = new Interpolator::Table2D(m_title,
				m_title,
				TABLE_2D_INTERPOLATION,
				string(m_path));
		}
	}
	catch (Exception& e)
	{
		ostringstream ossMsgErr;
		ossMsgErr << "Unable to create Interpolator::Table from file : " << string(m_path) << endl;
		ossMsgErr << "Reason: " << e.getDescription() << endl;
		ERROR_STREAM << ossMsgErr.str().c_str() << endl;
		THROW_DEVFAILED("LOGIC_ERROR",
                        ossMsgErr.str().c_str(),
                        "TableReader::TableReader()"); 
	}
}

//-----------------------------------------------------------------------------
//- TableReader::~TableReader
//-----------------------------------------------------------------------------
TableReader::~TableReader()
{
	if (m_is_2D_table)
	{
		if (m_table_2D)
		{
			delete m_table_2D;
			m_table_2D = NULL;
		}
	}
	else
	{
		if (m_table_1D)
		{
			delete m_table_1D;
			m_table_1D = NULL;
		}
	}
}

//-----------------------------------------------------------------------------
//- double TableReader::get_interpolated_value()
//-----------------------------------------------------------------------------
double TableReader::get_interpolated_value(double p_val)
{
	if (m_table_1D)
	{
		//read from table
		double l_val = m_table_1D->computeValue(p_val);
		return l_val;
	}
	else
	{
		return 0;
	}
}

//-----------------------------------------------------------------------------
//- double TableReader::get_interpolated_value()
//-----------------------------------------------------------------------------
double TableReader::get_interpolated_value(double p_val_1, double p_val_2)
{
	if (m_table_2D)
	{
		//read from table
		double l_val = m_table_2D->computeValue(p_val_1, p_val_2);
		return l_val;
	}
	else
	{
		return 0;
	}
}

//-----------------------------------------------------------------------------
//- void get_nb_data()
//-----------------------------------------------------------------------------
void TableReader::get_nb_data(long & nb_data)
{
	if (m_table_1D)
	{
		// read size from table
		nb_data = m_table_1D->getNbData();
	}
	else
	{
		nb_data = 0;
	}
}

//-----------------------------------------------------------------------------
//- void get_nb_data()
//-----------------------------------------------------------------------------
void TableReader::get_nb_data(long & nb_x_data, long & nb_y_data)
{
	if (m_table_2D)
	{
		// read sizes from table
		nb_x_data = m_table_2D->getNbXData();
    nb_y_data = m_table_2D->getNbYData();
	}
	else
	{
		nb_x_data = 0;
    nb_y_data = 0;
	}
}

}

