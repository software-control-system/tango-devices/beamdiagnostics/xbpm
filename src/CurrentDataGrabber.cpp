//=============================================================================
// CurrentDataGrabber.cpp
//=============================================================================
// abstraction.......Currents data grabber for Xbpm
// class.............CurrentDataGrabber
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CurrentDataGrabber.h"
#include <yat4tango/LogHelper.h>
#include <yat/utils/String.h>

namespace Xbpm_ns
{
//- check electrometer proxy macro:
#define CHECK_ELECTROMETER_PROXY() \
do \
{ \
  if (! m_electrometer_proxy) \
  { \
    m_internal_error = true; \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the electrometer Proxy isn't accessible ", \
      "CurrentDataGrabber::check_electrometer"); \
  } \
} while (0)

//- check sai proxy macro:
#define CHECK_SAI_PROXY() \
do \
{ \
  if (! m_sai_controller_proxy) \
  { \
    m_internal_error = true; \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the sai controller Proxy isn't accessible ", \
      "CurrentDataGrabber::check_sai"); \
  } \
} while (0)

//- check beam energy proxy macro:
#define CHECK_BEAM_PROXY() \
do \
{ \
  if (! m_beam_energy_proxy) \
  { \
    m_internal_error = true; \
    THROW_DEVFAILED("DEVICE_ERROR", \
      "request aborted - the beam energy Proxy isn't accessible ", \
      "CurrentDataGrabber::check_beam"); \
  } \
} while (0)

// ============================================================================
// CurrentDataGrabber::CurrentDataGrabber ()
// ============================================================================ 
CurrentDataGrabber::CurrentDataGrabber (Tango::DeviceImpl * hostDevice, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: Tango::LogAdapter(hostDevice), m_dyn_attr_manager(p_dyn_attr_manager)
{
  m_sai_integration_time = 1000.0;
  m_gain = 1.0;
  m_range = 1.0;
  m_energy = 100000000;
  m_flux = 1.0;
  m_unit = 1.0;
  m_state = Tango::UNKNOWN;
  m_state_electrometer = Tango::UNKNOWN;
  m_state_sai = Tango::UNKNOWN;
  m_state_beam = Tango::UNKNOWN;
  m_electrometer_proxy = NULL;
  m_sai_controller_proxy = NULL;
  m_beam_energy_proxy = NULL;
  m_sai_in_fault = false;
  m_electrom_in_fault = false;
  m_beam_energy_in_fault = false;
  m_internal_error = false;
  m_electrom_set_range_in_fault = false;
  m_electrom_gain_in_fault = false;
  m_electrom_validity = LOCUM_RETRIES;
  m_table_reader = NULL;
  m_storage_mgr = NULL;
}

// ============================================================================
// CurrentDataGrabber::~CurrentDataGrabber ()
// ============================================================================ 
CurrentDataGrabber::~CurrentDataGrabber ()
{
  m_sai_average_list.clear();
  m_sai_current_list.clear();
  m_sai_histo_list.clear();

  if (m_cfg.enableFluxComputing == true)
  {
    if (m_table_reader)
    {
      delete m_table_reader;
      m_table_reader = NULL;
    }
  }
  if (m_electrometer_proxy)
  {
    delete m_electrometer_proxy;
    m_electrometer_proxy = NULL;
  }
  if (m_sai_controller_proxy)
  {
    delete m_sai_controller_proxy;
    m_sai_controller_proxy = NULL;
  }
  if (m_beam_energy_proxy)
  {
    delete m_beam_energy_proxy;
    m_beam_energy_proxy = NULL;
  }
}

// ============================================================================
// CurrentDataGrabber::get_state ()
// ============================================================================ 
Tango::DevState CurrentDataGrabber::get_state()
{
  // get electrometer device state
  if (m_electrometer_proxy)
  {
    try
    {
      m_state_electrometer = Tango::UNKNOWN;
      m_electrom_in_fault = false;
      m_state_electrometer = m_electrometer_proxy->state();
      m_electrom_validity = LOCUM_RETRIES;
    }
    catch (Tango::DevFailed &e)
    {
      WARN_STREAM << e << std::endl;
      // Give the electrometer another chance to answer...
      if (m_electrom_validity == 0)
      {
        ERROR_STREAM << "Failed to get electrometer device state: retries expired!" << std::endl;
        m_electrom_in_fault = true;
      }
      else
      {
        m_electrom_validity--;
      }
    }
    catch (...)
    {
      WARN_STREAM << "Failed to get electrometer device state" << std::endl;
      // Give the electrometer another chance to answer...
      if (m_electrom_validity == 0)
      {
        m_electrom_in_fault = true;
      }
      else
      {
        m_electrom_validity--;
      }
    }
  }

  // get sai device state
  if (m_sai_controller_proxy)
  {
    try
    {
      m_state_sai = Tango::UNKNOWN;
      m_sai_in_fault = false;
      m_state_sai = m_sai_controller_proxy->state();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      m_sai_in_fault = true;
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to get sai device state" << std::endl;
      m_sai_in_fault = true;
    }
  }

  // get beam energy device state (if needed)
  if (m_cfg.enableFluxComputing == true)
  {
    if (m_beam_energy_proxy)
    {
      try
      {
        m_state_beam = Tango::UNKNOWN;
        m_beam_energy_in_fault = false;
        m_state_beam = m_beam_energy_proxy->state();
      }
      catch (Tango::DevFailed &e)
      {
        ERROR_STREAM << e << std::endl;
        m_beam_energy_in_fault = true;
      }
      catch (...)
      {
        ERROR_STREAM << "Failed to get beam energy device state" << std::endl;
        m_beam_energy_in_fault = true;
      }
    }
  }
  else
  {
    m_state_beam = Tango::RUNNING;
  }

  // if at least one device in FAULT state, grabber in FAULT state
  if ((m_state_electrometer == Tango::FAULT) ||
      (m_state_sai == Tango::FAULT) ||
      (m_state_beam == Tango::FAULT) ||
      m_sai_in_fault ||
      m_electrom_in_fault ||
      m_beam_energy_in_fault ||
      m_electrom_set_range_in_fault ||
      m_internal_error)
  {				      
    m_state = Tango::FAULT;
  }
  // if at least one device in ALARM state, grabber in ALARM state
  else if (m_state_electrometer == Tango::ALARM ||
           m_state_sai == Tango::ALARM ||
           m_state_beam == Tango::ALARM ||
           m_electrom_gain_in_fault) // if cannot get gain, change state to ALARM instead of FAULT
  {
    m_state = Tango::ALARM;
  }
  // if at least one device in STANDBY or OFF state (except beam energy: STANDBY state
  // is a nominal state), grabber in STANDBY state
  else if ((m_state_electrometer == Tango::STANDBY) ||
           (m_state_sai == Tango::STANDBY) ||
           (m_state_electrometer == Tango::OFF) ||
           (m_state_sai == Tango::MOVING) || // SAI moving = config update in progress
           (m_state_beam == Tango::OFF))
  {
    m_state = Tango::STANDBY;
  }
  else
  {
    m_state = Tango::RUNNING;
  }

  return m_state;
}

// ============================================================================
// CurrentDataGrabber::get_status ()
// ============================================================================ 
std::string CurrentDataGrabber::get_status()
{
  std::string status = "";

  // compose module general status
  switch (m_state)
  {
    case Tango::RUNNING:
    {
      status = "current module state is: RUNNING\n";
    }
    break;
    case Tango::ALARM:
    {
      status = "current module state is: ALARM\n";
      if (m_electrom_gain_in_fault)
        status += "Electrometer Device proxy error (gain)!\n";
    }
    break;
    case Tango::STANDBY:
    {
      status = "current module state is: STANDBY\n";
    }
    break;
    case Tango::FAULT:
    {
      status = "module has switched to FAULT state\n";
      if (m_sai_in_fault)
        status += "SAI Device proxy error!\n";
      if (m_electrom_in_fault)
        status += "Electrometer Device proxy error (state)!\n";
      if (m_beam_energy_in_fault)
        status += "Beam energy Device proxy error!\n";
      if (m_internal_error)
        status += "Internal error!\n";
      if (m_electrom_set_range_in_fault)
        status += "Electrometer Device proxy error (set range)!\n";
    }
    break;
    default:
    {
      status = "Unhandled module state!!\n";
    }
    break;
  }

  // add detailed states
  status += " --SAI Device state: " + std::string(Tango::DevStateName[m_state_sai]) + "\n";
  status += " --Electrometer Device state: " + std::string(Tango::DevStateName[m_state_electrometer]) + "\n";

  if (m_cfg.enableFluxComputing == true)
  {
    status += " --Beam Energy Device state: " + std::string(Tango::DevStateName[m_state_beam]) + "\n";
  }

  return status;
}

// ============================================================================
// CurrentDataGrabber::init ()
// ============================================================================ 
void CurrentDataGrabber::init(XbpmConfig& cfg)  
{
  m_cfg = cfg;
  m_sai_in_fault = false;
  m_electrom_in_fault = false;
  m_beam_energy_in_fault = false;
  m_electrom_set_range_in_fault = false;
  m_electrom_gain_in_fault = false;
  m_electrom_validity = LOCUM_RETRIES;
  m_monoEnergyUnit = "";
  m_energyCoeff = 1;

  if (!m_dyn_attr_manager)
  {
    ERROR_STREAM << "initialization failed - failed to access DynAttrManager" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
      "initialization failed - failed to access DynAttrManager", 
      "CurrentDataGrabber::init"); 
  }

  // create SAI attribute proxy list for average currents
  std::string attr_name = "average" + m_cfg.saiControllerChan0Name;
  m_sai_average_list.push_back(attr_name);
  attr_name = "average" + m_cfg.saiControllerChan1Name;
  m_sai_average_list.push_back(attr_name);
  attr_name = "average" + m_cfg.saiControllerChan2Name;
  m_sai_average_list.push_back(attr_name);
  attr_name = "average" + m_cfg.saiControllerChan3Name;
  m_sai_average_list.push_back(attr_name);

  // create dynamic attributes associated to current data
  
  //- FLUX
  if (m_cfg.enableFluxComputing == true)
  {
    yat4tango::DynamicAttributeInfo dai;
    dai.dev = m_cfg.hostDevice;
    dai.tai.name = FLUX;
    dai.tai.label = FLUX;
    //- describe the dyn attr we want...
    dai.tai.data_type = Tango::DEV_DOUBLE;
    dai.tai.data_format = Tango::SCALAR;
    dai.tai.writable = Tango::READ;
    dai.tai.disp_level = Tango::EXPERT;
    dai.tai.description = "Beam flux (in photons/s)";
    //- attribute properties:
    dai.tai.unit = "photons/s";
    dai.tai.standard_unit = "photons/s";
    dai.tai.display_unit = "photons/s";
    dai.tai.format = "%1.4e";

    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai.cdb = true;
    //- read callback
    dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
     &CurrentDataGrabber::read_flux);
    m_dyn_attr_manager->add_attribute(dai);


    yat4tango::DynamicAttributeInfo dai_2;
    dai_2.dev = m_cfg.hostDevice;
    dai_2.tai.name = RESPONSIVITY_TABLE;
    dai_2.tai.label = RESPONSIVITY_TABLE;
    //- describe the dyn attr we want...
    dai_2.tai.data_type = Tango::DEV_STRING;
    dai_2.tai.data_format = Tango::SCALAR;
    dai_2.tai.writable = Tango::READ_WRITE;
    dai_2.tai.disp_level = Tango::EXPERT;
    dai_2.tai.description = "Path and name of the responsivity table for the flux computing (E -> responsivity).";

    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai_2.cdb = false;
    //- read callback
    dai_2.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &CurrentDataGrabber::read_responsivity_table);
    //- read callback
    dai_2.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, &CurrentDataGrabber::write_responsivity_table);
    m_dyn_attr_manager->add_attribute(dai_2);
  }

  //- INTENSITY HISTORY
  if (m_cfg.enableIntensityHistory == true)
  {
    // history
    yat4tango::DynamicAttributeInfo dai1;
    dai1.dev = m_cfg.hostDevice;
    dai1.tai.name = INTENSITY_HIS;
    dai1.tai.label = INTENSITY_HIS;
    //- describe the dyn attr we want...
    dai1.tai.data_type = Tango::DEV_DOUBLE;
    dai1.tai.data_format = Tango::SPECTRUM;
    dai1.tai.writable = Tango::READ;
    dai1.tai.disp_level = Tango::EXPERT;
    dai1.tai.description = "Intensity history.";
    dai1.tai.format = "%1.4e";
    dai1.tai.max_dim_x = LONG_MAX;
		
    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai1.cdb = true;
    //- read callback
    dai1.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &CurrentDataGrabber::read_intensity_history);
    m_dyn_attr_manager->add_attribute(dai1);
    m_intensity_history.capacity(0);
    m_intensity_history.force_length(0);
  }

  // define SAI history attributes if one history option enabled
  if (m_cfg.enableIntensityHistory || m_cfg.enablePositionHistory)
  {
    // create SAI attribute proxy list for historics (spectrums)
    std::string attr_name = "historized" + m_cfg.saiControllerChan0Name;
    m_sai_histo_list.push_back(attr_name);
    attr_name = "historized" + m_cfg.saiControllerChan1Name;
    m_sai_histo_list.push_back(attr_name);
    attr_name = "historized" + m_cfg.saiControllerChan2Name;
    m_sai_histo_list.push_back(attr_name);
    attr_name = "historized" + m_cfg.saiControllerChan3Name;
    m_sai_histo_list.push_back(attr_name);
  }

  //- STANDARD DEVIATIONS
  if (m_cfg.enableSigmaIntensity == true)
  {
    //add dyn attr standard deviation intensity
    for (unsigned int l_cpt = 1; l_cpt < 5; l_cpt++)
    {
      yat::OSStream oss;
      oss << l_cpt;
      yat4tango::DynamicAttributeInfo dai;
      dai.dev = m_cfg.hostDevice;
      dai.tai.name = INTENSITY_STD_DEV + oss.str();
      dai.tai.label = INTENSITY_STD_DEV + oss.str();
      //- describe the dyn attr we want...
      dai.tai.data_type = Tango::DEV_DOUBLE;
      dai.tai.data_format = Tango::SCALAR;
      dai.tai.writable = Tango::READ;
      dai.tai.disp_level = Tango::EXPERT;
      dai.tai.description = "Standard deviation intensity on channel " + oss.str() + " (in measurementUnit).";
      dai.tai.format = "%1.4e";
      //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
      dai.cdb = true;
      //- read callback
      dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &CurrentDataGrabber::read_intensity_std_deviation);
      m_dyn_attr_manager->add_attribute(dai);
    }

    //add dyn attr current spectrums
    for (unsigned int l_cpt = 1; l_cpt < 5; l_cpt++)
    {
      yat::OSStream oss;
      oss << l_cpt;
      yat4tango::DynamicAttributeInfo dai;
      dai.dev = m_cfg.hostDevice;
      dai.tai.name = CURRENT_SPEC_PRE + oss.str() + CURRENT_SPEC_POST;
      dai.tai.label = CURRENT_SPEC_PRE + oss.str() + CURRENT_SPEC_POST;
      //- describe the dyn attr we want...
      dai.tai.data_type = Tango::DEV_DOUBLE;
      dai.tai.data_format = Tango::SPECTRUM;
      dai.tai.writable = Tango::READ;
      dai.tai.disp_level = Tango::EXPERT;
      dai.tai.description = "Samples acquired on channel " + oss.str() + " (in measurementUnit).";
      dai.tai.format = "%1.4e";
      dai.tai.max_dim_x = LONG_MAX;
			
      //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
      dai.cdb = true;
      //- read callback
      dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &CurrentDataGrabber::read_current_spectrum );
      m_dyn_attr_manager->add_attribute(dai);
    }

    // create SAI attribute proxy list for currents (spectrums)
    m_sai_current_list.push_back(m_cfg.saiControllerChan0Name);
    m_sai_current_list.push_back(m_cfg.saiControllerChan1Name);
    m_sai_current_list.push_back(m_cfg.saiControllerChan2Name);
    m_sai_current_list.push_back(m_cfg.saiControllerChan3Name);
  }

  // create electrometer device proxy
  try
  {
    m_electrometer_proxy = new yat4tango::DeviceProxyHelper(m_cfg.electrometerProxyName);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to create electrometer device proxy", 
			"CurrentDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to create electrometer device proxy " << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to create electrometer device proxy", 
			"CurrentDataGrabber::init"); 
  }

  CHECK_ELECTROMETER_PROXY();

  // create sai device proxy
  try
  {
    m_sai_controller_proxy = new Tango::DeviceProxy(m_cfg.saiControllerProxyName);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to create sai controller device proxy", 
			"CurrentDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to create sai controller device proxy" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to create sai controller device proxy", 
			"CurrentDataGrabber::init"); 
  }

  CHECK_SAI_PROXY();

  // create beam energy device proxy (if needed)
  if (m_cfg.enableFluxComputing == true)
  {
    try
    {
      m_beam_energy_proxy = new Tango::DeviceProxy(m_cfg.beamEnergyProxyName);
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
				"initialization failed - failed to create beam energy device proxy", 
				"CurrentDataGrabber::init"); 
    }
    catch (...)
    {
      ERROR_STREAM << "initialization failed - failed to create beam energy device proxy " << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"initialization failed - failed to create beam energy device proxy", 
				"CurrentDataGrabber::init"); 
    }

    CHECK_BEAM_PROXY();

    // try to read energy unit
    try
    {
      Tango::AttributeInfoEx ai = m_beam_energy_proxy->get_attribute_config(m_cfg.energyAttrName);
		 
      m_monoEnergyUnit = ai.unit;
      if (yat::StringUtil::is_equal_no_case(m_monoEnergyUnit, std::string("eV")))
        m_energyCoeff = 1;
      else if (yat::StringUtil::is_equal_no_case(m_monoEnergyUnit, std::string("KeV")))
        m_energyCoeff = 1000;
      else // unknown unit
      {
        ERROR_STREAM << "initialization failed - unknown unit for mono energy: " << m_monoEnergyUnit << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
				"initialization failed - unknown unit for mono energy, should be: eV, KeV", 
				"CurrentDataGrabber::init"); 
      }
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
				"initialization failed - failed to read beam energy attribute", 
				"CurrentDataGrabber::init"); 
    }
    catch (...)
    {
      ERROR_STREAM << "initialization failed - failed to read beam energy attribute" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"initialization failed - failed to read beam energy attribute", 
				"CurrentDataGrabber::init"); 
    }
	
    // create reader for the flux computing
    try
    {
      vector<int> l_list;
      l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
      l_list.push_back(INDEX_RESPONSIVITY_IN_FILE_1D);
      m_table_reader = new Xbpm_ns::TableReader(m_cfg.hostDevice, m_cfg.responsivityTable, "ENERGY -> RESPONSIVITY", false, l_list);
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
				"initialization failed - failed to create responsivity table reader", 
				"CurrentDataGrabber::init"); 
    }
    catch (...)
    {
      ERROR_STREAM << "initialization failed - failed to create responsivity table reader" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"initialization failed - failed to create responsivity table reader", 
				"CurrentDataGrabber::init"); 
    }

    if (!m_table_reader)
    {
      ERROR_STREAM << "initialization failed - failed to create responsivity table reader" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
				"initialization failed - failed to create responsivity table reader", 
				"CurrentDataGrabber::init"); 
    }
  }

  // update state & status
  get_state();
  get_status();
	
  // init OK
  m_state = Tango::ON;
}


// ============================================================================
// CurrentDataGrabber::start ()
// ============================================================================ 
void CurrentDataGrabber::start()
{
  CHECK_SAI_PROXY();

  // reset history (if enabled)
  if (m_cfg.enableIntensityHistory)
  {
    m_intensity_history.clear();
    m_intensity_history.capacity(0);
    m_intensity_history.force_length(0);
  }

  try
  {
    m_sai_controller_proxy->command_inout("Start");
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_sai_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to start the SAI controller", 
			"CurrentDataGrabber::start"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to start the SAI controller" << std::endl;
    m_sai_in_fault = true;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to start the SAI controller", 
			"CurrentDataGrabber::start"); 
  }
}

// ============================================================================
// CurrentDataGrabber::stop ()
// ============================================================================ 
void CurrentDataGrabber::stop()
{
  CHECK_SAI_PROXY();

  try
  {
    m_sai_controller_proxy->command_inout("Stop");
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_sai_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to stop the SAI controller", 
			"CurrentDataGrabber::stop"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to stop the SAI controller" << std::endl;
    m_sai_in_fault = true;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to stop the SAI controller", 
			"CurrentDataGrabber::stop"); 
  }
}

// ============================================================================
// CurrentDataGrabber::get_sai_integration_time ()
// ============================================================================ 
double CurrentDataGrabber::get_sai_integration_time()
{
  CHECK_SAI_PROXY();

  try
  {
    Tango::DeviceAttribute l_attr;
    l_attr = m_sai_controller_proxy->read_attribute("integrationTime");
    l_attr >> m_sai_integration_time;
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_sai_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to get the SAI integration time", 
			"CurrentDataGrabber::get_sai_integration_time"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get the SAI integration time" << std::endl;
    m_sai_in_fault = true;
  THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to get the SAI integration time", 
			"CurrentDataGrabber::get_sai_integration_time"); 
  }

  return m_sai_integration_time;
}

// ============================================================================
// CurrentDataGrabber::get_gain ()
// ============================================================================ 
double CurrentDataGrabber::get_gain()
{
  CHECK_ELECTROMETER_PROXY();

  try
  {
    Tango::DeviceAttribute l_attr;
    l_attr = m_electrometer_proxy->read_attribute(m_cfg.electrometerGainName);
    l_attr >> m_range;
    m_electrom_validity = LOCUM_RETRIES;

    // convert the range read on the electrometer device into gain:
    // gain(microA/V)= range(microA)/10
    m_gain = m_range / 10;
    m_electrom_gain_in_fault = false;
  }
  catch (Tango::DevFailed &e)
  {
    WARN_STREAM << e << std::endl;

    // Give the electrometer another chance to answer...
    if (m_electrom_validity == 0)
    {
      ERROR_STREAM << "Failed to get gain: retries expired!" << std::endl;
      m_electrom_gain_in_fault = true;
      m_gain = yat::IEEE_NAN;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
			"Failed to get gain", 
			"CurrentDataGrabber::get_gain");
	}
    else
    {
      m_electrom_validity--;
    }	
  }
  catch (...)
  {
    WARN_STREAM << "Failed to get gain" << std::endl;
		
    // Give the electrometer another chance to answer...
    if (m_electrom_validity == 0)
    {
      m_electrom_gain_in_fault = true;
      m_gain = yat::IEEE_NAN;
      THROW_DEVFAILED("DEVICE_ERROR",
			"Failed to get gain", 
			"CurrentDataGrabber::get_gain");
	}
    else
    {
      m_electrom_validity--;
    } 
  }

  return m_gain;
}

// ============================================================================
// CurrentDataGrabber::set_current_unit ()
// ============================================================================ 
void CurrentDataGrabber::set_current_unit(double p_unit)
{
  m_unit = p_unit;
}

// ============================================================================
// CurrentDataGrabber::get_voltages_data ()
// ============================================================================ 
Voltages_t CurrentDataGrabber::get_voltage_data()
{
  CHECK_SAI_PROXY();

  std::vector<Tango::DeviceAttribute> * dev_attrs = 0;
  double l_it = 0.0;

  // read average value from the sai board for the 4 channels
  try
  {
    dev_attrs = m_sai_controller_proxy->read_attributes(m_sai_average_list);

    (*dev_attrs)[0] >> l_it;
    m_voltages.voltage1 = l_it;

    (*dev_attrs)[1] >> l_it;
    m_voltages.voltage2 = l_it;

    (*dev_attrs)[2] >> l_it;
    m_voltages.voltage3 = l_it;

    (*dev_attrs)[3] >> l_it;
    m_voltages.voltage4 = l_it;
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_sai_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to get voltages", 
			"CurrentDataGrabber::get_voltage_data"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get voltages" << std::endl;
    m_sai_in_fault = true;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to get voltages", 
			"CurrentDataGrabber::get_voltage_data"); 
  }

  delete dev_attrs;

  // check for voltage in alarm
  if ((fabs(m_voltages.voltage1) >= fabs(m_cfg.highVoltageThreshold)) || 
      (fabs(m_voltages.voltage2) >= fabs(m_cfg.highVoltageThreshold)) ||
      (fabs(m_voltages.voltage3) >= fabs(m_cfg.highVoltageThreshold)) || 
      (fabs(m_voltages.voltage4) >= fabs(m_cfg.highVoltageThreshold)) ||
      (fabs(m_voltages.voltage1) <= fabs(m_cfg.lowVoltageThreshold)) || 
      (fabs(m_voltages.voltage2) <= fabs(m_cfg.lowVoltageThreshold)) ||
      (fabs(m_voltages.voltage3) <= fabs(m_cfg.lowVoltageThreshold)) || 
      (fabs(m_voltages.voltage4) <= fabs(m_cfg.lowVoltageThreshold)))
  {
    m_voltages.voltageInAlarm = true;
  }
  else
  {
    m_voltages.voltageInAlarm = false;
  }

  return m_voltages;
}

// ============================================================================
// CurrentDataGrabber::compute_current_data ()
// ============================================================================ 
Currents_t CurrentDataGrabber::compute_current_data(bool nx_enabled)
{
  // compute current from voltage measurement
  m_currents.current1 = ((m_voltages.voltage1 - m_cfg.v1Offset) * m_gain * m_cfg.gI1 - m_cfg.i1Offset) * m_unit;

  m_currents.current2 = ((m_voltages.voltage2 - m_cfg.v2Offset) * m_gain * m_cfg.gI2 - m_cfg.i2Offset) * m_unit;

  m_currents.current3 = ((m_voltages.voltage3 - m_cfg.v3Offset) * m_gain * m_cfg.gI3 - m_cfg.i3Offset) * m_unit;

  m_currents.current4 = ((m_voltages.voltage4 - m_cfg.v4Offset) * m_gain * m_cfg.gI4 - m_cfg.i4Offset) * m_unit;

  // intensity calculation depends on XBPM type
  switch (m_cfg.xbpmType)
  {
    case TYPE_QUADRANT:
      // intensity = sum of the 4 currents
	  m_currents.intensity = (m_currents.current1 + m_currents.current2 + m_currents.current3 + m_currents.current4);
    break;

    case TYPE_PSD:
      // intensity = abs(sum of 2 currents only)
	  m_currents.intensity = fabs(m_currents.current1 + m_currents.current2);
      break;
    
    default:
      // should not be here anyway!
      m_currents.intensity = yat::IEEE_NAN;
      break;
  }

  // record new value if recording enabled
  if (nx_enabled)
  {
    if (m_storage_mgr == NULL)
    {
      ERROR_STREAM << "CurrentDataGrabber::compute_current_data: cannot record data, storage mger is NULL!" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
          "Failed to record data, internal pointer is NULL!",
          "CurrentDataGrabber::compute_current_data");
    }
    else
    {
      try
      {
        DEBUG_STREAM << "Pushing data for intensity" << endl;
        m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + INTENSITY, &m_currents.intensity, 1);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "CurrentDataGrabber::compute_current_data: pushNexusData caught DevFailed: " << df << std::endl;
        m_storage_mgr->manageNexusAbort();
        throw;
      }
      catch (...)
      {
        ERROR_STREAM << "CurrentDataGrabber::compute_current_data: pushNexusData caugth unknown exception!" << std::endl;
        m_storage_mgr->manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR",
                "Failed to record data, caught [...]!",
                "CurrentDataGrabber::compute_current_data");
      }
    }
  }

  return m_currents;
}

// ============================================================================
// CurrentDataGrabber::compute_dynamic_current_data ()
// ============================================================================ 
DynamicCurrentData_t CurrentDataGrabber::compute_dynamic_current_data(Currents_Histos_t sai_currents, 
  bool histo_reset_needed, bool flux_nx_enabled, bool intensity_nx_enabled)
{
  // Check for intensity history
  if (m_cfg.enableIntensityHistory)
  {
    size_t new_data_nb = sai_currents.histo0.length();
    History_t l_intensity_storage;
    l_intensity_storage.clear();

    // reset history if needed
    if (histo_reset_needed)
    {
      m_intensity_history.clear();
      m_intensity_history.capacity(0);
      m_intensity_history.force_length(0);
      m_intensity_history.capacity(new_data_nb);
      m_intensity_history.force_length(new_data_nb);
    }

    // update m_intensity_history with computation on new current data from SAI:
    //   currents => intensities
    for (size_t idx = 0; idx < new_data_nb; idx++)
    {
      double curr1 = sai_currents.histo0[idx];
      double curr2 = sai_currents.histo1[idx];
      double curr3 = sai_currents.histo2[idx];
      double curr4 = sai_currents.histo3[idx];

      size_t added_index = idx;

      // intensity calculation depends on XBPM type
      double intensity;
      if (m_cfg.xbpmType == TYPE_QUADRANT)
      {
        // intensity = sum of the 4 currents
        intensity = (curr1 + curr2 + curr3 + curr4);
      }
      else if (m_cfg.xbpmType == TYPE_PSD)
      {
        // intensity = abs(sum of 2 currents only)
        intensity = fabs(curr1 + curr2);
      }
      else
      {
        intensity = yat::IEEE_NAN;
      }
      // if update data: append new values
      // otherwise: overwrite existing values
      if (!histo_reset_needed)
      {
        size_t new_capacity = m_intensity_history.length() + 1;
        m_intensity_history.capacity(new_capacity, true);
        m_intensity_history.force_length(new_capacity);
        m_intensity_history[new_capacity - 1] = intensity;
        added_index = new_capacity - 1;
      }
      else
      {
        m_intensity_history[idx] = intensity;
      }

      // store value for recording
      if (intensity_nx_enabled)
      {
        size_t new_capacity = l_intensity_storage.length() + 1;
        l_intensity_storage.capacity(new_capacity, true);
        l_intensity_storage.force_length(new_capacity);
        l_intensity_storage[new_capacity - 1] = intensity;
      }
    }

    // record new values if recording enabled
    if (intensity_nx_enabled && (l_intensity_storage.length() != 0))
    {
      if (m_storage_mgr == NULL)
      {
          ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: cannot record data, storage mger is NULL!" << std::endl;
          THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, internal pointer is NULL!",
                    "CurrentDataGrabber::compute_dynamic_current_data");
      }
      else
      {
        try
        {
          DEBUG_STREAM << "Pushing data for intensity history: " << l_intensity_storage.length() << " values..." << endl;
          m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + INTENSITY_HIS, l_intensity_storage.base(), l_intensity_storage.length());
        }
        catch (Tango::DevFailed & df)
        {
          ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: pushNexusData caught DevFailed: " << df << std::endl;
          m_storage_mgr->manageNexusAbort();
          throw;
        }
        catch (...)
        {
          ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: pushNexusData caugth unknown exception!" << std::endl;
          m_storage_mgr->manageNexusAbort();
          THROW_DEVFAILED("DEVICE_ERROR",
                "Failed to record data, caught [...]!",
                    "CurrentDataGrabber::compute_dynamic_current_data");
        }
      }
    }
  }

  // Check for currentSpectrum and std_dev on each channel
  if (m_cfg.enableSigmaIntensity)
  {
    CHECK_SAI_PROXY();

    std::vector<Tango::DeviceAttribute> * dev_attrs = 0;
    yat::Buffer<double> l_buffer_0;
    yat::Buffer<double> l_buffer_1;
    yat::Buffer<double> l_buffer_2;
    yat::Buffer<double> l_buffer_3;

    vector<double> l_vector;
    size_t l_vector_size = 0;

    try
    {
      //- currents spectrum reads on sai controller
      dev_attrs = m_sai_controller_proxy->read_attributes(m_sai_current_list);
	
      //- Chan0
      (*dev_attrs)[0] >> l_vector;
      l_vector_size = l_vector.capacity();
      l_buffer_0.capacity(l_vector_size);
      l_buffer_0.force_length(l_vector_size);
			
      for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
      {
        l_buffer_0[l_cpt] = ((l_vector[l_cpt] -  m_cfg.v1Offset) * m_gain * m_cfg.gI1 - m_cfg.i1Offset) * m_unit;
      }
			
      l_vector.clear();

      //- Chan1
      (*dev_attrs)[1] >> l_vector;
      l_vector_size = l_vector.capacity();
      l_buffer_1.capacity(l_vector_size);
      l_buffer_1.force_length(l_vector_size);
			
      for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
      {
        l_buffer_1[l_cpt] = ((l_vector[l_cpt] -  m_cfg.v2Offset) * m_gain * m_cfg.gI2 - m_cfg.i2Offset) * m_unit;
      }
      l_vector.clear();
			
      //- Chan2
      (*dev_attrs)[2] >> l_vector;
      l_vector_size = l_vector.capacity();
      l_buffer_2.capacity(l_vector_size);
      l_buffer_2.force_length(l_vector_size);
			
      for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
      {
        l_buffer_2[l_cpt] = ((l_vector[l_cpt] -  m_cfg.v3Offset) * m_gain * m_cfg.gI3 - m_cfg.i3Offset) * m_unit;
      }
      l_vector.clear();

      //- Chan3
      (*dev_attrs)[3] >> l_vector;
      l_vector_size = l_vector.capacity();
      l_buffer_3.capacity(l_vector_size);
      l_buffer_3.force_length(l_vector_size);
			
      for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
      {
        l_buffer_3[l_cpt] = ((l_vector[l_cpt] - m_cfg.v4Offset) * m_gain * m_cfg.gI4 - m_cfg.i4Offset) * m_unit;
      }

      m_dyn_currents.currents_buffer[0] = l_buffer_0;
      m_dyn_currents.currents_buffer[1] = l_buffer_1;
      m_dyn_currents.currents_buffer[2] = l_buffer_2;
      m_dyn_currents.currents_buffer[3] = l_buffer_3;

      //- std dev on each spectrum (m_currents.current<i> is already the average value of the buffer, computed by the SAI)
      m_dyn_currents.intensity_std_deviation[0] = Xbpm_ns::std_dev_calculation(m_dyn_currents.currents_buffer[0], m_currents.current1) * m_gain * m_unit;
      m_dyn_currents.intensity_std_deviation[1] = Xbpm_ns::std_dev_calculation(m_dyn_currents.currents_buffer[1], m_currents.current2) * m_gain * m_unit;
      m_dyn_currents.intensity_std_deviation[2] = Xbpm_ns::std_dev_calculation(m_dyn_currents.currents_buffer[2], m_currents.current3) * m_gain * m_unit;
      m_dyn_currents.intensity_std_deviation[3] = Xbpm_ns::std_dev_calculation(m_dyn_currents.currents_buffer[3], m_currents.current4) * m_gain * m_unit;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      m_sai_in_fault = true;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
				"Failed to get voltage spectrum", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to get voltages spectrum" << std::endl;
      m_sai_in_fault = true;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to get voltages spectrum", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }

    delete dev_attrs;
  }

  // Check for the flux computing (if needed)
  if (m_cfg.enableFluxComputing)
  {
    CHECK_BEAM_PROXY();

    try
    {
      Tango::DeviceAttribute l_attr;
      l_attr = m_beam_energy_proxy->read_attribute(m_cfg.energyAttrName);
      l_attr >> m_energy;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << e << std::endl;
      m_beam_energy_in_fault = true;
      RETHROW_DEVFAILED(e, "DEVICE_ERROR",
				"Failed to get energy", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }
    catch (...)
    {
      ERROR_STREAM << "Failed to get energy" << std::endl;
      m_beam_energy_in_fault = true;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to get energy", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }

    //- get the responsivity from table
    if (!m_table_reader)
    {
      ERROR_STREAM << "Failed to get responsivity table reader" << std::endl;
      m_internal_error = true;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to get responsivity table reader", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }

    double l_response = 0.0;
    try
    {
      l_response = m_table_reader->get_interpolated_value(m_energy);
    }
    catch(...)
    {
      ERROR_STREAM << "Failed to read responsivity table" << std::endl;
      m_internal_error = true;
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to read responsivity table", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }
		
    // check if energy or responsivity is not null)
    if ((l_response == 0.0) ||
        (m_energy == 0.0))
    {
      ERROR_STREAM << "Failed to compute flux value: responsivity or energy value is null!" << std::endl;
      m_beam_energy_in_fault = (m_energy == 0.0);
      m_internal_error = (l_response == 0.0);
      THROW_DEVFAILED("DEVICE_ERROR",
				"Failed to compute flux value: responsivity or energy value is null!", 
				"CurrentDataGrabber::compute_dynamic_current_data"); 
    }
    //DEBUG_STREAM << "Responsivity : " << l_response << " with energy : " << m_energy << endl;
    m_flux = m_currents.intensity * FLUX_CST / (l_response * ELECTRON_CHARGE * m_energy * m_energyCoeff); // energy is in eV 

    // record new value if recording enabled
    if (flux_nx_enabled)
    {
      if (m_storage_mgr == NULL)
      {
        ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: cannot record data, storage mger is NULL!" << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
                "Failed to record data, internal pointer is NULL!",
                "CurrentDataGrabber::compute_dynamic_current_data");
      }
      else
      {
        try
        {
          DEBUG_STREAM << "Pushing data for flux" << endl;
          m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + FLUX, &m_flux, 1);
        }
        catch (Tango::DevFailed & df)
        {
          ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: pushNexusData caught DevFailed: " << df << std::endl;
          m_storage_mgr->manageNexusAbort();
          throw;
        }
        catch (...)
        {
          ERROR_STREAM << "CurrentDataGrabber::compute_dynamic_current_data: pushNexusData caugth unknown exception!" << std::endl;
          m_storage_mgr->manageNexusAbort();
          THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, caught [...]!",
                    "CurrentDataGrabber::compute_dynamic_current_data");
        }
      }
    }
  }

  return m_dyn_currents;
}

// ============================================================================
// CurrentDataGrabber::read_flux ()
// ============================================================================ 
void CurrentDataGrabber::read_flux(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);

  cbd.tga->set_value(&m_flux);

  if (m_voltages.voltageInAlarm)
  {
    cbd.tga->set_quality(Tango::ATTR_ALARM);
  }
  else
  {
    cbd.tga->set_quality(Tango::ATTR_VALID);
  }
}

// ============================================================================
// CurrentDataGrabber::read_intensity_history ()
// ============================================================================ 
void CurrentDataGrabber::read_intensity_history(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);
  cbd.tga->set_value(m_intensity_history.base(), m_intensity_history.length());
}

// ============================================================================
// CurrentDataGrabber::read_intensity_std_deviation ()
// ============================================================================ 
void CurrentDataGrabber::read_intensity_std_deviation(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);

  std::string l_attr_name = cbd.dya->get_name();

  // name will be standardDeviationIntensityX
  std::string l_str = l_attr_name.substr(26, 1);
  yat::uint16 l_idx = atoi(l_str.c_str());

  // choose tab depending on l_idx
  cbd.tga->set_value(&m_dyn_currents.intensity_std_deviation[l_idx - 1]);
}

// ============================================================================
// CurrentDataGrabber::read_current_spectrum ()
// ============================================================================ 
void CurrentDataGrabber::read_current_spectrum(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);

  std::string l_attr_name = cbd.dya->get_name();

  //name will be currentXSpectrum
  std::string l_str = l_attr_name.substr(7, 1);
  yat::uint16 l_idx = atoi(l_str.c_str());
  cbd.tga->set_value(m_dyn_currents.currents_buffer[l_idx - 1].base(), m_dyn_currents.currents_buffer[l_idx - 1].capacity());
}

// ============================================================================
// CurrentDataGrabber::update_range ()
// ============================================================================ 
void CurrentDataGrabber::update_range() 
{
  CHECK_ELECTROMETER_PROXY();

  // update range according to high & low voltage thresholds
  try
  {
    double l_val_max = max_value();

    if (l_val_max >= fabs(m_cfg.highVoltageThreshold))
    {
      // increase range only if not already at max value
      if (m_range < MAX_RANGE_VALUE)
      {
        m_electrometer_proxy->command("RangeUp", LOCUM_RETRIES);
      }
      else
      {
        INFO_STREAM << "Could not increase range: already at maximum value." << std::endl;
      }
    }

    if (l_val_max <= fabs(m_cfg.lowVoltageThreshold))
    {
      // decrease range only if not already at min value
      if (m_range > MIN_RANGE_VALUE) 
      {
        m_electrometer_proxy->command("RangeDown", LOCUM_RETRIES);
      }
      else
      {
        INFO_STREAM << "Could not decrease range: already at minimum value." << std::endl;
      }
    }
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_electrom_set_range_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Failed to set range", 
			"CurrentDataGrabber::update_range"); 
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to set range" << std::endl;
    m_electrom_set_range_in_fault = true;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to set range", 
			"CurrentDataGrabber::update_range"); 
  }
}

// ********************************************************************************************
//
//	CurrentDataGrabber::max() returns max value of the voltage list
//
// ********************************************************************************************
double CurrentDataGrabber::max_value()
{
  vector<Tango::DevDouble> list;
  double max;

  // - filling list
  list.push_back(m_voltages.voltage1);
  list.push_back(m_voltages.voltage2);
  list.push_back(m_voltages.voltage3);
  list.push_back(m_voltages.voltage4);

  max = fabs(list[0]);

  for(size_t i = 1; i < list.size();i++)
  {
    if( fabs(list[i]) > max )
    {
      max = fabs(list[i]);
    }
  }
  return max;
}

// ============================================================================
// CurrentDataGrabber::read_responsivity_table ()
// ============================================================================ 
void CurrentDataGrabber::read_responsivity_table(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  static char * __reponsivity_table__;
  static std::string __responsivity_table_str__;

  __responsivity_table_str__ = m_cfg.responsivityTable;
  __reponsivity_table__ = const_cast<char*>(__responsivity_table_str__.c_str());  
  cbd.tga->set_value(&__reponsivity_table__);
}

// ============================================================================
// CurrentDataGrabber::write_responsivity_table ()
// ============================================================================ 
void CurrentDataGrabber::write_responsivity_table(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
  char * __responsivity_table__;
  cbd.tga->get_write_value(__responsivity_table__);

  // check device state
  if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
  {
    THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must not be RUNNING to edit responsivityTable", 
			"CurrentDataGrabber::write_responsivity_table"); 
  }

  // check if same value than before
  if (m_cfg.responsivityTable.compare(__responsivity_table__) == 0)
  {
    DEBUG_STREAM << "Same value for responsivity table - do nothing" << endl;
  }
  else
  {
    if (m_cfg.enableFluxComputing)
    {
      if (m_table_reader)
      {
        delete m_table_reader;
      }
      vector<int> l_list;
      l_list.push_back(INDEX_ENERGY_IN_FILE_1D);
      l_list.push_back(INDEX_RESPONSIVITY_IN_FILE_1D);
      try
      {
        m_table_reader = new Xbpm_ns::TableReader(m_cfg.hostDevice, __responsivity_table__, "ENERGY -> RESPONSIVITY", false, l_list);
      }
      catch (Tango::DevFailed &e)
      {
        ERROR_STREAM << e << std::endl;
        m_table_reader = new Xbpm_ns::TableReader(m_cfg.hostDevice, m_cfg.responsivityTable, "ENERGY -> RESPONSIVITY", false, l_list);
        RETHROW_DEVFAILED(e, "DEVICE_ERROR",
					"initialization failed - failed to create responsivity table reader", 
					"CurrentDataGrabber::write_responsivity_table"); 
      }
      catch (...)
      {
        m_table_reader = new Xbpm_ns::TableReader(m_cfg.hostDevice, m_cfg.responsivityTable, "ENERGY -> RESPONSIVITY", false, l_list);
        ERROR_STREAM << "initialization failed - failed to create responsivity table reader" << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
					"initialization failed - failed to create responsivity table reader", 
					"CurrentDataGrabber::write_responsivity_table"); 
      }

      if (!m_table_reader)
      {
          ERROR_STREAM << "initialization failed - failed to create responsivity table reader" << std::endl;
          THROW_DEVFAILED("DEVICE_ERROR",
					"initialization failed - failed to create responsivity table reader", 
					"CurrentDataGrabber::write_responsivity_table"); 
      }
			
      m_cfg.responsivityTable = __responsivity_table__;
      std::string prop_name = std::string("__responsivityTable");
      yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __responsivity_table__);
    }
  }
}

// ============================================================================
// CurrentDataGrabber::get_current_historics ()
// ============================================================================ 
Currents_Histos_t CurrentDataGrabber::get_current_historics()
{
  Currents_Histos_t histos;
  size_t l_real_vector_size = 0;
  size_t l_real_size = 0;

  // read SAI voltage historic attrs
  std::vector<Tango::DeviceAttribute> * dev_attrs = 0;

  vector<double> l_vector;
  size_t l_vector_size = 0;

  try
  {
    dev_attrs = m_sai_controller_proxy->read_attributes(m_sai_histo_list);

    //**** Chan0
    (*dev_attrs)[0] >> l_vector;
    l_vector_size = l_vector.capacity();
    l_real_vector_size = l_vector_size;
    // get real size of buffer from chan0
    for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
    {
      if (yat::is_nan<double>(l_vector[l_cpt]))
      {
        l_real_vector_size = l_cpt;
        break;
      }
    }
    histos.histo0.capacity(l_real_vector_size);
    histos.histo0.force_length(l_real_vector_size);
    DEBUG_STREAM << "CurrentDataGrabber::get_current_historics() - SAI historics valid data nb = " << l_real_vector_size << std::endl;
    // get significant data & transform to current
    for (unsigned int l_cpt = 0; l_cpt < l_real_vector_size; l_cpt++)
    {
      histos.histo0[l_cpt] = ((l_vector[l_cpt] - m_cfg.v1Offset) * m_gain * m_cfg.gI1 - m_cfg.i1Offset) * m_unit;
    }
    l_vector.clear();

    //**** Chan1
    (*dev_attrs)[1] >> l_vector;
    l_vector_size = l_vector.capacity();
    l_real_size = l_vector_size;
    // get real size of buffer
    for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
    {
      if (yat::is_nan<double>(l_vector[l_cpt]))
      {
        l_real_size = l_cpt;
        break;
      }
    }
    // check equals to size of chan0
    if (l_real_size != l_real_vector_size)
    {
      ERROR_STREAM << "SAI historics sizes differ!" << std::endl;
      m_sai_in_fault = true;
      THROW_DEVFAILED("DEVICE_ERROR",
            "SAI historics sizes differ!",
            "CurrentDataGrabber::get_current_historics");
    }
    histos.histo1.capacity(l_real_vector_size);
    histos.histo1.force_length(l_real_vector_size);
    // get significant data & transform to currents
    for (unsigned int l_cpt = 0; l_cpt < l_real_vector_size; l_cpt++)
    {
      histos.histo1[l_cpt] = ((l_vector[l_cpt] - m_cfg.v2Offset) * m_gain * m_cfg.gI2 - m_cfg.i2Offset) * m_unit;
    }
    l_vector.clear();

    //**** Chan2
    (*dev_attrs)[2] >> l_vector;
    l_vector_size = l_vector.capacity();
    l_real_size = l_vector_size;
    // get real size of buffer
    for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
    {
      if (yat::is_nan<double>(l_vector[l_cpt]))
      {
        l_real_size = l_cpt;
        break;
      }
    }
    // check equals to size of chan0
    if (l_real_size != l_real_vector_size)
    {
      ERROR_STREAM << "SAI historics sizes differ!" << std::endl;
      m_sai_in_fault = true;
      THROW_DEVFAILED("DEVICE_ERROR",
            "SAI historics sizes differ!",
            "CurrentDataGrabber::get_current_historics");
    }
    histos.histo2.capacity(l_real_vector_size);
    histos.histo2.force_length(l_real_vector_size);
    // get significant data & transform to currents
    for (unsigned int l_cpt = 0; l_cpt < l_real_vector_size; l_cpt++)
    {
      histos.histo2[l_cpt] = ((l_vector[l_cpt] - m_cfg.v3Offset) * m_gain * m_cfg.gI3 - m_cfg.i3Offset) * m_unit;
    }
    l_vector.clear();

    //**** Chan3
    (*dev_attrs)[3] >> l_vector;
    l_vector_size = l_vector.capacity();
    l_real_size = l_vector_size;
    // get real size of buffer
    for (unsigned int l_cpt = 0; l_cpt < l_vector_size; l_cpt++)
    {
      if (yat::is_nan<double>(l_vector[l_cpt]))
      {
        l_real_size = l_cpt;
        break;
      }
    }
    // check equals to size of chan0
    if (l_real_size != l_real_vector_size)
    {
      ERROR_STREAM << "SAI historics sizes differ!" << std::endl;
      m_sai_in_fault = true;
      THROW_DEVFAILED("DEVICE_ERROR",
            "SAI historics sizes differ!",
            "CurrentDataGrabber::get_current_historics");
    }
    histos.histo3.capacity(l_real_vector_size);
    histos.histo3.force_length(l_real_vector_size);
    // get significant data & transform to currents
    for (unsigned int l_cpt = 0; l_cpt < l_real_vector_size; l_cpt++)
    {
      histos.histo3[l_cpt] = ((l_vector[l_cpt] - m_cfg.v4Offset) * m_gain * m_cfg.gI4 - m_cfg.i4Offset) * m_unit;
    }
    l_vector.clear();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    m_sai_in_fault = true;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR",
                "Failed to get historic spectrum!",
                "CurrentDataGrabber::get_current_historics");
  }
  catch (...)
  {
    ERROR_STREAM << "Failed to get historic spectrum" << std::endl;
    m_sai_in_fault = true;
    THROW_DEVFAILED("DEVICE_ERROR",
                "Failed to get historic spectrum!",
                "CurrentDataGrabber::get_current_historics");
  }

  delete dev_attrs;

  return histos;
}

// ============================================================================
// CurrentDataGrabber::init_nexus ()
// ============================================================================ 
void CurrentDataGrabber::init_nexus(NexusManager * nx_mgr)
{
  if (nx_mgr == NULL)
  {
    THROW_DEVFAILED("CONFIGURATION_ERROR",
          "Failed to set nexus manager: null pointer!",
          "CurrentDataGrabber::init_nexus");
  }
  m_storage_mgr = nx_mgr;
}

} // namespace Xbpm_ns

