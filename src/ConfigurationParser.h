//=============================================================================
// ConfigurationParser.h
//=============================================================================
// abstraction.......Mode configuration parser
// class.............ConfigurationParser
// original author...S. GARA - NEXEYA
//=============================================================================

#ifndef _CONFIG_PARSER_H_
#define _CONFIG_PARSER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include <yat4tango/LogHelper.h>

namespace Xbpm_ns
{

//-----------------------------------------------------------------------------
//- Consts and key words for configuration parsing
//-----------------------------------------------------------------------------
static const std::string kKEY_VALUE_SEPARATOR ("::");
static const std::string kVALUE_SEPARATOR_CL (":");
static const std::string kVALUE_SEPARATOR_COMA (",");

static const std::string kKEY_DEVICE_INSERTION ("InsertionProxyName");
static const std::string kKEY_ATTR_GAP ("GapAttr");
static const std::string kKEY_ATTR_PHASE ("PhaseAttr");

static const std::string kKEY_DEVICE_BEAM ("BeamEnergyProxyName");
static const std::string kKEY_ATTR_ENERGY ("EnergyAttr");

static const std::string kKEY_DEVICE_HSLIT ("HSlitProxyName");
static const std::string kKEY_ATTR_HSLIT_GAP ("HSlitGapAttr");

static const std::string kKEY_DEVICE_VSLIT ("VSlitProxyName");
static const std::string kKEY_ATTR_VSLIT_GAP ("VSlitGapAttr");

static const std::string kKEY_OFFSET_X_TABLE ("OffsetX_table");
static const std::string kKEY_OFFSET_Z_TABLE ("OffsetZ_table");
static const std::string kKEY_OFFSET_TABLE ("Offset_table");

static const std::string kKEY_K_X_TABLE ("Kx_table");
static const std::string kKEY_K_Z_TABLE ("Kz_table");
static const std::string kKEY_K_TABLE ("K_table");

static const std::string kKEY_BEAMSIZE_TABLE ("BeamSizeTable");
static const std::string kKEY_DETECTOR_GAP ("DetectorGap");

static const std::string kKEY_K_X_FACTOR ("Kx_Factor");
static const std::string kKEY_K_Z_FACTOR ("Kz_Factor");
static const std::string kKEY_K_Z1_FACTOR ("Kz1_Factor");
static const std::string kKEY_K_Z2_FACTOR ("Kz2_Factor");
static const std::string kKEY_DELTA_Z ("DeltaZ");

static const std::string kKEY_ZCORR_TABLE ("ZCorrectionTable");
static const std::string kKEY_CORR_TABLE ("CorrectionTable");

static const std::string kKEY_MISALIGNEMENT_OFFSET ("MisalignementOffset");
static const std::string kKEY_MISALIGNEMENT_OFFSET_X ("MisalignementOffsetX");
static const std::string kKEY_MISALIGNEMENT_OFFSET_Z ("MisalignementOffsetZ");
static const std::string kKEY_ELECTRONIC_OFFSET ("ElectronicOffset");
static const std::string kKEY_OPERATOR_OFFSET ("OperatorOffset");
static const std::string kKEY_OPERATOR_OFFSET_X ("OperatorOffsetX");
static const std::string kKEY_OPERATOR_OFFSET_Z ("OperatorOffsetZ");

static const std::string kKEY_OFFSET_X ("OffsetX");
static const std::string kKEY_OFFSET_Z ("OffsetZ");
static const std::string kKEY_OFFSET_Z1 ("OffsetZ1");
static const std::string kKEY_OFFSET_Z2 ("OffsetZ2");
//-----------------------------------------------------------------------------


// ============================================================================
// class: ConfigurationParser
// This class provides parsing and extracting methods to get data from a ConfigX
// property which contains the mode definition.
// ============================================================================
class ConfigurationParser : public Tango::LogAdapter
{
public:

  //- constructor
  ConfigurationParser(Tango::DeviceImpl * host_device);

  //- destructor
  virtual ~ConfigurationParser();
  
  //- Parses a "Config<i>" property and fills a map containing the list of 
  //- <key,value> defining the mode.
  ModeKeys_t parseConfigProperty (std::vector<std::string> config_property);

  //- Extracts offset x from mode definition.
  double extractOffsetX (ModeKeys_t acq_config);

  //- Extracts offset z from mode definition.
  double extractOffsetZ (ModeKeys_t acq_config);

  //- Extracts k x factor from mode definition.
  double extractKXFactor (ModeKeys_t acq_config);

  //- Extracts k z factor from mode definition.
  double extractKZFactor (ModeKeys_t acq_config);

  //- Extracts misalignement offset from mode definition.
  double extractMisalignementOffset (ModeKeys_t acq_config);

  //- Extracts misalignement offset x from mode definition.
  double extractMisalignementOffsetX (ModeKeys_t acq_config);

  //- Extracts misalignement offset z from mode definition.
  double extractMisalignementOffsetZ (ModeKeys_t acq_config);

  //- Extracts Electronic offset from mode definition.
  double extractElectronicOffset (ModeKeys_t acq_config);

  //- Extracts operator offset from mode definition.
  double extractOperatorOffset (ModeKeys_t acq_config);

  //- Extracts operator offset x from mode definition.
  double extractOperatorOffsetX (ModeKeys_t acq_config);

  //- Extracts operator offset z from mode definition.
  double extractOperatorOffsetZ (ModeKeys_t acq_config);

  //- Extracts correction table from mode definition.
  std::string extractCorrectionTable (ModeKeys_t acq_config);

  //- Extracts Z correction table from mode definition.
  std::string extractZCorrectionTable (ModeKeys_t acq_config);

  //- Extracts k z1 factor from mode definition.
  double extractKZ1Factor (ModeKeys_t acq_config);

  //- Extracts k z2 factor from mode definition.
  double extractKZ2Factor (ModeKeys_t acq_config);

  //- Extracts Delta z from mode definition.
  double extractDeltaZ (ModeKeys_t acq_config);

  //- Extracts offset z1 from mode definition.
  double extractOffsetZ1 (ModeKeys_t acq_config);

  //- Extracts offset z2 from mode definition.
  double extractOffsetZ2 (ModeKeys_t acq_config);

  //- Extracts detector gap from mode definition.
  double extractDetectorGap (ModeKeys_t acq_config);

  //- Extracts beam energy proxy name from mode definition.
  std::string extractBeamEnergyProxyName (ModeKeys_t acq_config);

  //- Extracts hslit proxy name from mode definition.
  std::string extractHSlitProxyName (ModeKeys_t acq_config);

  //- Extracts vslit proxy name from mode definition.
  std::string extractVSlitProxyName (ModeKeys_t acq_config);

  //- Extracts insertion proxy name from mode definition.
  std::string extractInsertionProxyName (ModeKeys_t acq_config);

  //- Extracts hslit gap attr name from mode definition.
  std::string extractHSlitGapAttr (ModeKeys_t acq_config);

  //- Extracts vslit gap attr name from mode definition.
  std::string extractVSlitGapAttr (ModeKeys_t acq_config);

  //- Extracts energy attr name from mode definition.
  std::string extractEnergyAttr (ModeKeys_t acq_config);

  //- Extracts gap attr name from mode definition.
  std::string extractGapAttr (ModeKeys_t acq_config);

  //- Extracts pahse attr name from mode definition.
  std::string extractPhaseAttr (ModeKeys_t acq_config);

  //- Extracts k table from mode definition.
  std::string extractKTable (ModeKeys_t acq_config);

  //- Extracts kx table from mode definition.
  std::string extractKxTable (ModeKeys_t acq_config);

  //- Extracts kz table from mode definition.
  std::string extractKzTable (ModeKeys_t acq_config);

  //- Extracts offset table from mode definition.
  std::string extractOffsetTable (ModeKeys_t acq_config);

  //- Extracts beam size table from mode definition.
  std::string extractBeamSizeTable (ModeKeys_t acq_config);

  //- Extracts offsetx table from mode definition.
  std::string extractOffsetXTable (ModeKeys_t acq_config);

  //- Extracts offsetz table from mode definition.
  std::string extractOffsetZTable (ModeKeys_t acq_config);

private:
	//- Splits a line using the specifed separator and returns the 
    //- result in a string vector.
    //- Checks expected minimum & maximum number of tokens if not null.
	std::vector<std::string> splitLine(std::string line, std::string separator, yat::uint16 min_token = 0, yat::uint16 max_token = 0);

	//- Gets {key,value} from line using the specified separator and tells if
    //- the key word equals the specified key
    bool testKey(std::string line, std::string separator, std::string key);

	//- Tells if token is "true" or "false".
    //- Sends exception if neither one of those strings.
    //- Not case sensitive.
    bool getBoolean(std::string token);

};

} // namespace Xbpm_ns

#endif // _CONFIG_PARSER_H_
