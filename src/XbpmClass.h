//=============================================================================
//
// file :         XbpmClass.h
//
// description :  Include for the XbpmClass root class.
//                This class is the singleton class for
//                the Xbpm device class.
//                It contains all properties and methods which the 
//                Xbpm requires only once e.g. the commands.
//			
// project :      TANGO Device Server
//
// $Author: pascal_verdier $
//
// $Revision: 14110 $
// $Date: 2010-02-10 08:47:17 +0100 (Wed, 10 Feb 2010) $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source$
// $Log$
// Revision 3.8  2009/04/07 10:53:56  pascal_verdier
// Tango-7 release.
// SVN tags added
//
// Revision 3.7  2008/04/07 12:01:57  pascal_verdier
// CVS put property modified.
//
// Revision 3.6  2007/10/23 14:04:30  pascal_verdier
// Spelling mistakes correction
//
// Revision 3.5  2007/09/14 14:36:08  pascal_verdier
// Add an ifdef WIN32 for dll generation
//
// Revision 3.4  2005/09/08 08:45:23  pascal_verdier
// For Pogo-4.4.0 and above.
//
// Revision 3.3  2005/03/02 14:06:15  pascal_verdier
// namespace is different than class name.
//
// Revision 3.2  2004/11/08 11:33:16  pascal_verdier
// if device property not found in database, it takes class property value if exists.
//
// Revision 3.1  2004/09/06 09:27:05  pascal_verdier
// Modified for Tango 5 compatibility.
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _XBPMCLASS_H
#define _XBPMCLASS_H

#include <tango.h>
#include <Xbpm.h>


namespace Xbpm_ns
{//=====================================
//	Define classes for attributes
//=====================================
class nexusNbAcqPerFileAttrib: public Tango::Attr
{
public:
	nexusNbAcqPerFileAttrib():Attr("nexusNbAcqPerFile", Tango::DEV_ULONG, Tango::READ_WRITE) {};
	~nexusNbAcqPerFileAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_nexusNbAcqPerFile(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<Xbpm *>(dev))->write_nexusNbAcqPerFile(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_nexusNbAcqPerFile_allowed(ty);}
};

class nexusTargetPathAttrib: public Tango::Attr
{
public:
	nexusTargetPathAttrib():Attr("nexusTargetPath", Tango::DEV_STRING, Tango::READ_WRITE) {};
	~nexusTargetPathAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_nexusTargetPath(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<Xbpm *>(dev))->write_nexusTargetPath(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_nexusTargetPath_allowed(ty);}
};

class nexusFileGenerationAttrib: public Tango::Attr
{
public:
	nexusFileGenerationAttrib():Attr("nexusFileGeneration", Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
	~nexusFileGenerationAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_nexusFileGeneration(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<Xbpm *>(dev))->write_nexusFileGeneration(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_nexusFileGeneration_allowed(ty);}
};

class computationModeAttrib: public Tango::Attr
{
public:
	computationModeAttrib():Attr("computationMode", Tango::DEV_USHORT, Tango::READ_WRITE) {};
	~computationModeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_computationMode(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<Xbpm *>(dev))->write_computationMode(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_computationMode_allowed(ty);}
};

class verticalPositionAttrib: public Tango::Attr
{
public:
	verticalPositionAttrib():Attr("verticalPosition", Tango::DEV_DOUBLE, Tango::READ) {};
	~verticalPositionAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_verticalPosition(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_verticalPosition_allowed(ty);}
};

class horizontalPositionAttrib: public Tango::Attr
{
public:
	horizontalPositionAttrib():Attr("horizontalPosition", Tango::DEV_DOUBLE, Tango::READ) {};
	~horizontalPositionAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_horizontalPosition(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_horizontalPosition_allowed(ty);}
};

class measurementUnitAttrib: public Tango::Attr
{
public:
	measurementUnitAttrib():Attr("measurementUnit", Tango::DEV_STRING, Tango::READ) {};
	~measurementUnitAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_measurementUnit(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_measurementUnit_allowed(ty);}
};

class intensityAttrib: public Tango::Attr
{
public:
	intensityAttrib():Attr("intensity", Tango::DEV_DOUBLE, Tango::READ) {};
	~intensityAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_intensity(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_intensity_allowed(ty);}
};

class current4Attrib: public Tango::Attr
{
public:
	current4Attrib():Attr("current4", Tango::DEV_DOUBLE, Tango::READ) {};
	~current4Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_current4(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_current4_allowed(ty);}
};

class current3Attrib: public Tango::Attr
{
public:
	current3Attrib():Attr("current3", Tango::DEV_DOUBLE, Tango::READ) {};
	~current3Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_current3(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_current3_allowed(ty);}
};

class current2Attrib: public Tango::Attr
{
public:
	current2Attrib():Attr("current2", Tango::DEV_DOUBLE, Tango::READ) {};
	~current2Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_current2(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_current2_allowed(ty);}
};

class current1Attrib: public Tango::Attr
{
public:
	current1Attrib():Attr("current1", Tango::DEV_DOUBLE, Tango::READ) {};
	~current1Attrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_current1(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_current1_allowed(ty);}
};

class enableAutoRangeAttrib: public Tango::Attr
{
public:
	enableAutoRangeAttrib():Attr("enableAutoRange", Tango::DEV_BOOLEAN, Tango::READ_WRITE) {};
	~enableAutoRangeAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_enableAutoRange(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
	{(static_cast<Xbpm *>(dev))->write_enableAutoRange(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_enableAutoRange_allowed(ty);}
};

class gainAttrib: public Tango::Attr
{
public:
	gainAttrib():Attr("gain", Tango::DEV_DOUBLE, Tango::READ) {};
	~gainAttrib() {};
	
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
	{(static_cast<Xbpm *>(dev))->read_gain(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
	{return (static_cast<Xbpm *>(dev))->is_gain_allowed(ty);}
};

//=========================================
//	Define classes for commands
//=========================================
class NexusResetBufferIndexClass : public Tango::Command
{
public:
	NexusResetBufferIndexClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	NexusResetBufferIndexClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~NexusResetBufferIndexClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Xbpm *>(dev))->is_NexusResetBufferIndex_allowed(any);}
};



class SetUnitClass : public Tango::Command
{
public:
	SetUnitClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	SetUnitClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~SetUnitClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Xbpm *>(dev))->is_SetUnit_allowed(any);}
};



class StopClass : public Tango::Command
{
public:
	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StopClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StopClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Xbpm *>(dev))->is_Stop_allowed(any);}
};



class StartClass : public Tango::Command
{
public:
	StartClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	StartClass(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~StartClass() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<Xbpm *>(dev))->is_Start_allowed(any);}
};



//
// The XbpmClass singleton definition
//

class
#ifdef _TG_WINDOWS_
	__declspec(dllexport)
#endif
	XbpmClass : public Tango::DeviceClass
{
public:
//	properties member data

//	add your own data members here
//------------------------------------

public:
	Tango::DbData	cl_prop;
	Tango::DbData	cl_def_prop;
	Tango::DbData	dev_def_prop;

//	Method prototypes
	static XbpmClass *init(const char *);
	static XbpmClass *instance();
	~XbpmClass();
	Tango::DbDatum	get_class_property(string &);
	Tango::DbDatum	get_default_device_property(string &);
	Tango::DbDatum	get_default_class_property(string &);
	
protected:
	XbpmClass(string &);
	static XbpmClass *_instance;
	void command_factory();
	void get_class_property();
	void attribute_factory(vector<Tango::Attr *> &);
	void write_class_property();
	void set_default_property();
	string get_cvstag();
	string get_cvsroot();

private:
	void device_factory(const Tango::DevVarStringArray *);
};


}	//	namespace Xbpm_ns

#endif // _XBPMCLASS_H
