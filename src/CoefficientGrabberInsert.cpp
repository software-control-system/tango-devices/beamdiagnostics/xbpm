//=============================================================================
// CoefficientGrabberInsert.cpp
//=============================================================================
// abstraction.......CoefficientGrabberInsert for XbpmPositionModes
// class.............CoefficientGrabberInsert
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "CoefficientGrabberInsert.h"
#include <yat4tango/DeviceTask.h>

namespace Xbpm_ns
{

	// ============================================================================
	// CoefficientGrabberInsert::CoefficientGrabberInsert ()
	// ============================================================================ 
	CoefficientGrabberInsert::CoefficientGrabberInsert (const CoefficientGrabberInsertConfig & cfg) 
		:   CoefficientGrabber(cfg.hostDevice),
		    m_cfg(cfg)
	{
		m_dev_insertion = NULL;
		m_k_table_x_reader = NULL;
		m_k_table_z_reader = NULL;
		m_offset_table_x_reader = NULL;
		m_offset_table_z_reader = NULL;
		m_status = "";
		m_state = Tango::UNKNOWN;
        m_state_insertion = Tango::UNKNOWN;
        m_insertion_in_fault = false;
        m_internal_error = false;
	}

	// ============================================================================
	// CoefficientGrabberInsert::~CoefficientGrabberInsert ()
	// ============================================================================ 
	CoefficientGrabberInsert::~CoefficientGrabberInsert () 
	{
		if (m_dev_insertion)
		{
			delete m_dev_insertion;
            m_dev_insertion = NULL;
		}
		if (m_k_table_x_reader)
		{
			delete m_k_table_x_reader;
            m_k_table_x_reader = NULL;
		}
		if (m_k_table_z_reader)
		{
			delete m_k_table_z_reader;
            m_k_table_z_reader = NULL;
		}
		if (m_offset_table_x_reader)
		{
			delete m_offset_table_x_reader;
            m_offset_table_x_reader = NULL;
		}
		if (m_offset_table_z_reader)
		{
			delete m_offset_table_z_reader;
            m_offset_table_z_reader = NULL;
		}
	}


	// ============================================================================
	// CoefficientGrabberInsert::get_state ()
	// ============================================================================ 
	Tango::DevState CoefficientGrabberInsert::get_state()
	{
        // get state of insertion device
		if (m_dev_insertion)
		{
			try
			{
                m_insertion_in_fault = false;
				m_state_insertion = m_dev_insertion->state();
			}
			catch (Tango::DevFailed &e)
			{
				ERROR_STREAM << e << std::endl;
                m_insertion_in_fault = true;
			}
			catch (...)
			{
				ERROR_STREAM << "Failed to get insertion device state" << std::endl;
                m_insertion_in_fault = true;
			}

            // update module state
            if (m_internal_error || 
                m_insertion_in_fault ||
                (m_state_insertion == Tango::FAULT))
            {
                      m_state = Tango::FAULT;
            }
            else if (m_state_insertion == Tango::STANDBY)
            {
                m_state = Tango::STANDBY;
            }
            else
            {
                m_state = Tango::RUNNING;
            }
        }

		return m_state;
	}

	// ============================================================================
	// CoefficientGrabberInsert::get_status ()
	// ============================================================================ 
	std::string CoefficientGrabberInsert::get_status()
	{
    // compose module general status
    switch (m_state)
    {
      case Tango::RUNNING:
      case Tango::ON:
	  case Tango::MOVING:
      {
        m_status = "current module state is: RUNNING\n";
      }
      break;
      case Tango::ALARM:
      {
        m_status = "current module state is: ALARM\n";
      }
      break;
      case Tango::STANDBY:
      case Tango::OFF:
      {
        m_status = "current module state is: STANDBY\n";
      }
      break;
      case Tango::FAULT:
      {
        m_status = "module has switched to FAULT state\n";
        if (m_insertion_in_fault)
          m_status += "Insertion Device proxy error!\n";
        else if (m_internal_error)
          m_status += "Internal error!\n";
        else
          m_status += "Unknown error!\n";
      }
      break;
      default:
      {
        m_status = "Unhandled module state!!\n";
      }
      break;
    }

    // add detailed states
    if (!m_insertion_in_fault)
    {
      m_status += " --Insertion Device state: " + std::string(Tango::DevStateName[m_state_insertion]) + "\n";
    }

	  return m_status;
	}

	// ============================================================================
	// CoefficientGrabberInsert::init ()
	// ============================================================================ 
	void CoefficientGrabberInsert::init()
	{
        m_insertion_in_fault = false;
        m_internal_error = false;

    // create insertion device proxy
		try
		{
			m_dev_insertion = new Tango::DeviceProxy(m_cfg.m_insertion_proxy_name);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
            m_state = Tango::FAULT;
            m_insertion_in_fault = true; 
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"initialization failed - failed to create insertion proxy", 
				"CoefficientGrabberInsert::init"); 
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create insertion proxy" << std::endl;
            m_state = Tango::FAULT;
            m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create insertion proxy", 
				"CoefficientGrabberInsert::init"); 
		}

		if (!m_dev_insertion)
		{
			ERROR_STREAM << "initialization failed - failed to access insertion proxy" << std::endl;
            m_state = Tango::FAULT;
            m_insertion_in_fault = true; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access insertion proxy", 
				"CoefficientGrabberInsert::init"); 
		}

    // create reader for GAP --> COEFF X table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_GAP_IN_FILE_1D);
			l_list.push_back(INDEX_COEFFX_IN_FILE_1D);
			m_offset_table_x_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Gap -> Offset X",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offset table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offset table x", 
				"CoefficientGrabberInsert::init"); 
		}

		if (!m_offset_table_x_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for offset table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for offset table x", 
				"CoefficientGrabberInsert::init"); 
		}

    // create reader for GAP --> COEFF Z table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_GAP_IN_FILE_1D);
			l_list.push_back(INDEX_COEFFZ_IN_FILE_1D);
			m_offset_table_z_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_offset_table_path, "Gap -> Offset Z",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for offset table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for offset table z", 
				"CoefficientGrabberInsert::init"); 
		}

		if (!m_offset_table_z_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for offset table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for offset table z", 
				"CoefficientGrabberInsert::init"); 
		}

    // create reader for GAP --> COEFF X table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_GAP_IN_FILE_1D);
			l_list.push_back(INDEX_COEFFX_IN_FILE_1D);
			m_k_table_x_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_k_table_path, "Gap -> calibration factor X",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for k table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for k table x", 
				"CoefficientGrabberInsert::init"); 
		}

		if (!m_k_table_x_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for k table x" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for k table x", 
				"CoefficientGrabberInsert::init"); 
		}

    // create reader for GAP --> COEFF Z table
		try
		{
			std::vector<int> l_list;
			l_list.push_back(INDEX_GAP_IN_FILE_1D);
			l_list.push_back(INDEX_COEFFZ_IN_FILE_1D);
			m_k_table_z_reader = new TableReader(m_cfg.hostDevice, m_cfg.m_k_table_path, "Gap -> calibration factor Z",false,l_list);
		}
		catch (...)
		{
			ERROR_STREAM << "initialization failed - failed to create tableReader for k table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to create tableReader for k table z", 
				"CoefficientGrabberInsert::init"); 
		}

		if (!m_k_table_z_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for k table z" << std::endl;
            m_state = Tango::FAULT;
            m_status = "See log for error details"; 
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for k table z", 
				"CoefficientGrabberInsert::init"); 
		}
		
    // init OK
    m_state = Tango::ON;
	}

	// ============================================================================
	// CoefficientGrabberInsert::get_coefficients ()
	// ============================================================================ 
	void CoefficientGrabberInsert::get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z)
	{
		double l_gap = 0.0;

    // get gap from insertion device
		if (!m_dev_insertion)
		{
			ERROR_STREAM << "failed to access insertion proxy" << std::endl;
            m_insertion_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access insertion proxy", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		try
		{
			Tango::DeviceAttribute l_attr;
			l_attr = m_dev_insertion->read_attribute(m_cfg.m_gap_attr_name);
			l_attr >> l_gap;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			m_insertion_in_fault = true;
			RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
				"Failed to get gap", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to get gap" << std::endl;
			m_insertion_in_fault = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to get gap", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

    // get offsets & factors from gap 
		if (!m_k_table_x_reader)
		{
			ERROR_STREAM << "failed to access tableReader for k table x" << std::endl;
            m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for k table x", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		try
		{
			p_kx = m_k_table_x_reader->get_interpolated_value(l_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kx" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kx", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		if (!m_k_table_z_reader)
		{
			ERROR_STREAM << "failed to access tableReader for k table z" << std::endl;
            m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"failed to access tableReader for k table z", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		try
		{
			p_kz = m_k_table_z_reader->get_interpolated_value(l_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for Kz" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for Kz", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		if (!m_offset_table_x_reader)
		{
			ERROR_STREAM << "failed to access tableReader for offset table x" << std::endl;
            m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"ifailed to access tableReader for offset table x", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		try
		{
			p_offset_x = m_offset_table_x_reader->get_interpolated_value(l_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetX" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetX", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		if (!m_offset_table_z_reader)
		{
			ERROR_STREAM << "initialization failed - failed to access tableReader for offset table z" << std::endl;
            m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"initialization failed - failed to access tableReader for offset table z", 
				"CoefficientGrabberInsert::get_coefficients"); 
		}

		try
		{
			p_offset_z = m_offset_table_z_reader->get_interpolated_value(l_gap);
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to compute value for OffsetZ" << std::endl;
			m_internal_error = true;
			THROW_DEVFAILED("DEVICE_ERROR", 
				"Failed to compute value for OffsetZ", 
				"CoefficientGrabberInsert::get_coefficients");
		}
	}

} // namespace Xbpm_ns