//=============================================================================
// CoefficientGrabberInsertXZ.h
//=============================================================================
// abstraction.......CoefficientGrabberInsertXZ for XbpmPositionModes
// class.............CoefficientGrabberInsertXZ
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COEFFICIENT_GRABBER_INSERTXZ
#define _COEFFICIENT_GRABBER_INSERTXZ

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "CoefficientGrabber.h"
#include "TableReader.h"
#include <yat4tango/LogHelper.h>


namespace Xbpm_ns
{

	// ============================================================================
	// class: CoefficientGrabberInsertXZ
	// ============================================================================
	class CoefficientGrabberInsertXZ : public CoefficientGrabber
	{

	public:

		//- constructor
		CoefficientGrabberInsertXZ(const CoefficientGrabberInsertXZConfig& cfg);

		//- destructor
		virtual ~CoefficientGrabberInsertXZ();

		//- init
		void init();

		//- gets state
		Tango::DevState get_state();

		//- gets status
		std::string get_status();

		//- gets coefficients
		void get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z);

	private:
		//- configuration
		CoefficientGrabberInsertXZConfig m_cfg;

		//- module state
		Tango::DevState m_state;

		//- detailed states
		Tango::DevState m_state_insertion;

		//- status
		std::string m_status;

		//- Proxy on insertion
		Tango::DeviceProxy * m_dev_insertion;

		//- reader for kx_table
		TableReader * m_kx_table_reader;

		//- reader for kz_table 
		TableReader * m_kz_table_reader;

		//- reader for offsetx table
		TableReader * m_offsetx_table_reader;

		//- reader for offsetz table
		TableReader * m_offsetz_table_reader;

		//- boolean for status details
		bool m_insertion_in_fault;
		bool m_internal_error;
	};

} // namespace Xbpm_ns

#endif // _COEFFICIENT_GRABBER_INSERTXZ