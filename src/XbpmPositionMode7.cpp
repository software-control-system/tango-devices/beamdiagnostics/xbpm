//=============================================================================
// XbpmPositionMode7.cpp
//=============================================================================
// abstraction.......XbpmPositionMode7 for XbpmPositionModes
// class.............XbpmPositionMode7
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode7.h"


namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode7::XbpmPositionMode7()
// ============================================================================ 
XbpmPositionMode7::XbpmPositionMode7(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
	m_kx_factor = 1;
	m_kz_factor = 1;
	m_offset_x = 0;
	m_offset_z = 0;
}

// ============================================================================
// XbpmPositionMode7::~XbpmPositionMode7()
// ============================================================================ 
XbpmPositionMode7::~XbpmPositionMode7()
{

}

// ============================================================================
// XbpmPositionMode7::init ()
// ============================================================================ 
void XbpmPositionMode7::init()
{
	m_state = Tango::RUNNING;
	m_status = "Module is running.";
}

// ============================================================================
// XbpmPositionMode7::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode7::get_state()
{
	return m_state;
}

// ============================================================================
// XbpmPositionMode7::get_status ()
// ============================================================================ 
std::string XbpmPositionMode7::get_status()
{
	return m_status;
}

// ============================================================================
// XbpmPositionMode7::get_properties ()
// ============================================================================ 
void XbpmPositionMode7::get_properties()
{
	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode7"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);


	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	if (!dev_prop[0].is_empty())
	{
		ModeKeys_t l_keys;
		bool l_is_mode7 = false;
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
			l_is_mode7 = true;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"Failed to parse Mode7 property", 
				"XbpmPositionMode7::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Failed to parse Mode7 property", 
				"XbpmPositionMode7::get_properties"); 
		}		
		if (l_is_mode7)
		{
			try
			{
				m_offset_x = l_conf_parser.extractOffsetX(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OffsetX.Using default value : 0" << endl;
            }
			try
			{
				m_offset_z = l_conf_parser.extractOffsetZ(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OffsetZ.Using default value : 0" << endl;
			}
			try
            {
				m_kx_factor = l_conf_parser.extractKXFactor(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for KxFactor.Using default value : 1" << endl;
            }
			try
            {
				m_kz_factor = l_conf_parser.extractKZFactor(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for KzFactor.Using default value : 1" << endl;
			}
		}
	}
}

// ============================================================================
// XbpmPositionMode7::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode7::get_position(Currents_t p_currents)
{
	m_currents = p_currents;

	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode7::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode7::compute_x_position_i()
{
	// X = [ (I1 - I0) / (I1 + I0) ] * Kx_Factor  + OffsetX
	m_position.x = ((m_currents.current2 - m_currents.current1) / (m_currents.current2 + m_currents.current1)) * m_kx_factor + m_offset_x;
}

// ============================================================================
// XbpmPositionMode7::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode7::compute_z_position_i()
{
	// Z = [ (I2 - I3) / (I2 + I3) ] * Kz_Factor +  OffsetZ
	m_position.z = ((m_currents.current3 - m_currents.current4) / (m_currents.current3 + m_currents.current4)) * m_kz_factor + m_offset_z;
}

} // namespace Xbpm_ns