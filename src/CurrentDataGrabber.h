//=============================================================================
// CurrentDataGrabber.h
//=============================================================================
// abstraction.......Current Data Grabber for Xbpm
// class.............CurrentDataGrabber
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _CURRENT_DATA_GRABBER_H
#define _CURRENT_DATA_GRABBER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "TableReader.h"
#include "NexusManager.h"
#include <yat4tango/DynamicAttribute.h>
#include <yat4tango/DynamicAttributeManager.h>
#include <yat4tango/PropertyHelper.h>
#include <yat4tango/DeviceProxyHelper.h>

namespace Xbpm_ns
{

// Number of tries to get the locum state or gain
#define LOCUM_RETRIES 5

// ============================================================================
// class: CurrentDataGrabber
// ============================================================================
class CurrentDataGrabber : public Tango::LogAdapter
{

public:

  //- constructor
  CurrentDataGrabber(Tango::DeviceImpl * hostDevice, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~CurrentDataGrabber();

  //- gets  state
  Tango::DevState get_state();

  //- gets status
  std::string get_status();

  //- initialization
  void init(XbpmConfig& cfg);

  //- set nexus manager for recording
  void init_nexus(NexusManager * nx_mgr);

  //- starts the voltage acquisition
  void start();

  //- stops the voltage acquisition
  void stop();

  //- gets sai integration time
  double get_sai_integration_time();

  //- gets gain
  double get_gain();

  //- gets voltage data
  Voltages_t get_voltage_data();

  //- computes current data (current & intensity)
  //- set nx enabled flag to true if recording is enabled
  Currents_t compute_current_data(bool nx_enabled);

  //- computes dynamic current data
  //- set reset flag to true if history reset is needed
  //- set nx enabled flags to true if recording is enabled
  DynamicCurrentData_t compute_dynamic_current_data(Currents_Histos_t sai_currents, bool histo_reset_needed, bool flux_nx_enabled, bool intensity_nx_enabled);

  //- get SAI voltage historics & transform to currents
  Currents_Histos_t get_current_historics();

  //- read callback for dyn attr flux
  void read_flux(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr intensity history
  void read_intensity_history(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr intensity std deviation
  void read_intensity_std_deviation(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr current spectrum
  void read_current_spectrum(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- read callback for dyn attr responsivityTable
  void read_responsivity_table(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr responsivityTable
  void write_responsivity_table(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for dyn attr intensityHistoryDepth
  //void read_intensity_histo_depth(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr intensityHistoryDepth
 // void write_intensity_histo_depth(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- updates range according to voltage thresholds
  void update_range();

  //- sets current unit
  void set_current_unit(double p_unit);

  //- returns voltage max value
  double max_value();
              
private:
  //- configuration
  XbpmConfig m_cfg;

  //- dyn attr manager
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- electrometer device proxy
  yat4tango::DeviceProxyHelper * m_electrometer_proxy;

  //- sai controller device proxy
  Tango::DeviceProxy * m_sai_controller_proxy;   

  //- beam energy device proxy
  Tango::DeviceProxy * m_beam_energy_proxy;   

  //- flux
  double m_flux;  

  //- electrometer gain & range
  //- gain(microA/V)= range(microA)/10
  double m_gain;  
  double m_range;  

  //- energy
  double m_energy; 

  //- sai integration time
  double m_sai_integration_time;

  //- Current data (part I)
  Currents_t m_currents;

  //- Voltage data
  Voltages_t m_voltages;

  //- Current data (part II)
  DynamicCurrentData_t m_dyn_currents;

  //- intensity history
  History_t m_intensity_history;

  //- unit factor
  double m_unit;

  //- reader for energy --> reponsivity table
  TableReader * m_table_reader;

  //- general state
  Tango::DevState m_state;

  //- detailed states
  Tango::DevState m_state_electrometer;
  Tango::DevState m_state_sai;
  Tango::DevState m_state_beam;

  //- boolean for status details
  bool m_sai_in_fault;
  bool m_electrom_in_fault;
  bool m_beam_energy_in_fault;
  bool m_internal_error;
  bool m_electrom_set_range_in_fault;
  bool m_electrom_gain_in_fault;

  //- boolean for electrometer validity
  //- The SOLEIL Locum device is very slow and can take many
  //- seconds to anwser a read_attribute(), so let's give it
  //- a second chance to reply us...
  unsigned short m_electrom_validity;

  //- thread safety
  yat::Mutex m_lock;

  //- list of attributes to read from SAI
  std::vector<std::string> m_sai_average_list;
  std::vector<std::string> m_sai_current_list;
  std::vector<std::string> m_sai_histo_list;
  
  // mono energy unit
  std::string m_monoEnergyUnit;
  unsigned int m_energyCoeff; // coeff to convert mono energy to eV

  //- Nexus manager
  NexusManager * m_storage_mgr;
};

} // namespace Xbpm_ns

#endif // _CURRENT_DATA_GRABBER_H
