//=============================================================================
// XbpmPositionMode4.cpp
//=============================================================================
// abstraction.......XbpmPositionMode4 for XbpmPositionModes
// class.............XbpmPositionMode4
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode4.h"

namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode4::XbpmPositionMode4 ()
// ============================================================================ 
XbpmPositionMode4::XbpmPositionMode4 (const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
	m_delta_z = 0;
	m_electronic_offset = 0;
	m_kz1_factor = 1;
	m_kz2_factor = 1;
	m_misaligement_offset = 0;
	m_offset_z1 = 0;
	m_offset_z2 = 0;
	m_operator_offset = 0;
	m_z_correction_table = "";
	m_z1_offset_table_reader = NULL;
	m_z2_offset_table_reader = NULL;
    m_state = Tango::UNKNOWN;
    m_internal_error = false;
    m_is_dyn_table = true;
    m_is_table_empty = true;
    m_gain = yat::IEEE_NAN;
}

// ============================================================================
// XbpmPositionMode4::~XbpmPositionMode4 ()
// ============================================================================ 
XbpmPositionMode4::~XbpmPositionMode4 ()
{
	if (m_z1_offset_table_reader)
	{
		delete m_z1_offset_table_reader;
        m_z1_offset_table_reader = NULL;
	}
	if (m_z2_offset_table_reader)
	{
		delete m_z2_offset_table_reader;
        m_z2_offset_table_reader = NULL;
	}
	if (m_is_dyn_table)
	{
		m_dyn_attr_manager->remove_attribute(ZCORR_TABLE);
	}
}

// ============================================================================
// XbpmPositionMode4::init ()
// ============================================================================ 
void XbpmPositionMode4::init()
{
    m_internal_error = false;

	if (!m_is_table_empty)
	{
        // create reader for gain --> coeff Z1 table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_GAIN_IN_FILE_1D);
            l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
            m_z1_offset_table_reader = new TableReader(m_cfg.hostDevice, m_z_correction_table, "Gain -> KZ1",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for z correction table z1" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to create tableReader for z correction table z1", 
                "XbpmPositionMode4::init"); 
        }

        if (!m_z1_offset_table_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for z correction table z1" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to access tableReader for z correction table z1", 
                "XbpmPositionMode4::init"); 
        }

        // create reader for gain --> coeff z2 table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_GAIN_IN_FILE_1D);
            l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
            m_z2_offset_table_reader = new TableReader(m_cfg.hostDevice, m_z_correction_table, "Gain -> KZ2",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for z correction table z2" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to create tableReader for z correction table z2", 
                "XbpmPositionMode4::init"); 
        }

        if (!m_z2_offset_table_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for z correction table z2" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to access tableReader for z correction table z2", 
                "XbpmPositionMode4::init"); 
        }

        // check if the correction table has the wrigth number of gain values
        long l_size1;
        long l_size2;
        m_z1_offset_table_reader->get_nb_data(l_size1);
        m_z2_offset_table_reader->get_nb_data(l_size2);

        if ((l_size1 != NB_ELTS_IN_GAIN_CORRECTION_TABLE) ||
            (l_size2 != NB_ELTS_IN_GAIN_CORRECTION_TABLE))
        {
            ostringstream ossMsgErr;
            ossMsgErr << "initialization failed - gain correction table should contain " 
            << NB_ELTS_IN_GAIN_CORRECTION_TABLE << " offsets values" << std::endl;
            ERROR_STREAM << ossMsgErr.str().c_str() << endl;
            THROW_DEVFAILED("DEVICE_ERROR",
                            ossMsgErr.str().c_str(),
                            "XbpmPositionMode4::init"); 
        }
    }

  // init OK
  m_state = Tango::ON;
}

// ============================================================================
// XbpmPositionMode4::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode4::get_state()
{
    // update module state
    if (m_internal_error)
    {
        m_state = Tango::FAULT;
    }
    else
    {
        m_state = Tango::ON;
    }

	return m_state;
}

// ============================================================================
// XbpmPositionMode4::get_status ()
// ============================================================================ 
std::string XbpmPositionMode4::get_status()
{
    std::string status = "";

    // compose module general status
    switch (m_state)
    {
        case Tango::RUNNING:
        case Tango::ON:
        case Tango::MOVING:
        {
            status = "current module state is: RUNNING\n";
        }
        break;
        case Tango::ALARM:
        {
            status = "current module state is: ALARM\n";
        }
        break;
        case Tango::STANDBY:
        case Tango::OFF:
        {
            status = "current module state is: STANDBY\n";
        }
        break;
        case Tango::FAULT:
        {
            status = "module has switched to FAULT state\n";
            if (m_internal_error)
                status += "Internal error!\n";
            else
                status += "Unknown error!\n";
        }
        break;
        default:
        {
            status = "Unhandled module state!!\n";
        }
        break;
    }

	return status;
}

// ============================================================================
// XbpmPositionMode4::get_properties ()
// ============================================================================ 
void XbpmPositionMode4::get_properties()
{
	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode4"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);


	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	if (!dev_prop[0].is_empty())
	{
		ModeKeys_t l_keys;
		bool l_is_mode4 = false;
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
			l_is_mode4 = true;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"Failed to parse Mode4 property", 
				"XbpmPositionMode4::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Failed to parse Mode4 property", 
				"XbpmPositionMode2::get_properties"); 
		}
		if (l_is_mode4)
		{
			try
			{
				m_kz1_factor = l_conf_parser.extractKZ1Factor(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for Kz1_Factor.Using default value : 1" << endl;
            }
			try
            {
				m_kz2_factor = l_conf_parser.extractKZ2Factor(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for Kz2_Factor.Using default value : 1" << endl;
            }
			try
            {
				m_delta_z = l_conf_parser.extractDeltaZ(l_keys);
            }
			catch (...)
			{
				INFO_STREAM << "No value for Delta_Z.Using default value : 1" << endl;
			}
			try
			{
				m_misaligement_offset = l_conf_parser.extractMisalignementOffset(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for MisalignementOffset.Using default value : 1" << endl;
			}
			try
			{
				m_electronic_offset = l_conf_parser.extractElectronicOffset(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for ElectronicOffset.Using default value : 1" << endl;
			}
			try
			{
				m_operator_offset = l_conf_parser.extractOperatorOffset(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OperatorOffset.Using default value : 1" << endl;
			}
			try
            {
				m_offset_z1 = l_conf_parser.extractOffsetZ1(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for OffsetZ1.Using default value : 1" << endl;
            }
			try
            {
				m_offset_z2 = l_conf_parser.extractOffsetZ2(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for OffsetZ2.Using default value : 1" << endl;
            }
			try
            {
				m_z_correction_table = l_conf_parser.extractZCorrectionTable(l_keys);
				m_is_table_empty = false;
				m_is_dyn_table = false;
			}
			catch(...)
            {
				INFO_STREAM << "No value for zCorrectionTable.Creating Dynamic Attribute" << endl;
            }
        }
	}

	if (m_is_dyn_table)
	{
		//add dyn attr zCorrectionTable
		yat4tango::DynamicAttributeInfo dai;
		dai.dev = m_cfg.hostDevice;
		dai.tai.name = ZCORR_TABLE;
		dai.tai.label = ZCORR_TABLE;
		//- describe the dyn attr we want...
		dai.tai.data_type = Tango::DEV_STRING;
		dai.tai.data_format = Tango::SCALAR;
		dai.tai.writable = Tango::READ_WRITE;
		dai.tai.disp_level = Tango::EXPERT;
		dai.tai.description = "Path and name of the ZCorrection table";
		//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
		dai.cdb = false;
		//- read callback
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
			&XbpmPositionMode4::read_ZCorrectionTable);
		dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
			&XbpmPositionMode4::write_ZCorrectionTable);
		m_dyn_attr_manager->add_attribute(dai);

        try
        {
        std::string prop_name = std::string("__") + ZCORR_TABLE;
        m_z_correction_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
        m_is_table_empty = false;
        }
        catch(...)
        {
        // no memorized value, set default value
        m_z_correction_table = kDEFAULT_FILE_NAME;
        }
	}
}

// ============================================================================
// XbpmPositionMode4::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode4::get_position(Currents_t p_currents)
{
    m_internal_error = false;
	m_currents = p_currents;

	// get offset and coefficients values from tables according to gain value
	if (!m_z1_offset_table_reader)
	{
		ERROR_STREAM << "Failed to access tableReader for z correction table z1" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access tableReader for z correction table z1", 
			"XbpmPositionMode4::get_position"); 
	}

	try
	{
		m_offset_1 = m_z1_offset_table_reader->get_interpolated_value(m_gain);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for offset z1" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for offset z1", 
			"XbpmPositionMode4::get_position"); 
	}

	if (!m_z2_offset_table_reader)
	{
		ERROR_STREAM << "Failed to access tableReader for z correction table z2" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access tableReader for z correction table z2", 
			"XbpmPositionMode4::get_position"); 
	}

	try
	{
		m_offset_2 = m_z2_offset_table_reader->get_interpolated_value(m_gain);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for offset z2" << std::endl;
     m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for offset z2", 
			"XbpmPositionMode4::get_position"); 
	}

    // compute position
	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode4::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode4::compute_x_position_i()
{
	// Z1 = [ (I0 - I3) / (I0 + I3) ] * Kz1_Factor 
    //      - (MisalignementOffset + ElectronicOffset + OperatorOffset + OffsetZ1 + Z1_current_offset_correction) 
    //      + Delta_Z / 2
	m_position.x = 
    ((m_currents.current1 - m_currents.current4) / (m_currents.current1 + m_currents.current4)) * m_kz1_factor 
    - (m_misaligement_offset + m_electronic_offset + m_operator_offset + m_offset_z1 + m_offset_1) 
    + m_delta_z / 2;
}

// ============================================================================
// XbpmPositionMode4::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode4::compute_z_position_i()
{
	// Z2 = [ (I1 - I2) / (I1 + I2) ] * Kz2_Factor 
    //      - (MisalignementOffset + ElectronicOffset + OperatorOffset + OffsetZ2 + Z2_current_offset_correction) 
    //      - Delta_Z / 2
	m_position.z = 
    ((m_currents.current2 - m_currents.current3) / (m_currents.current2 + m_currents.current3)) * m_kz2_factor 
    - (m_misaligement_offset + m_electronic_offset + m_operator_offset + m_offset_z2 + m_offset_2) 
    - m_delta_z / 2;
}


// ============================================================================
// XbpmPositionMode4::read_ZCorrectionTable ()
// ============================================================================ 
void XbpmPositionMode4::read_ZCorrectionTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __correction_table__;
	static std::string __correction_table_str__;

	__correction_table_str__ = m_z_correction_table;
	__correction_table__ = const_cast<char*>(__correction_table_str__.c_str());  
	cbd.tga->set_value(&__correction_table__);
}

// ============================================================================
// XbpmPositionMode4::write_ZCorrectionTable ()
// ============================================================================ 
void XbpmPositionMode4::write_ZCorrectionTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
    m_internal_error = false;

	char * __correction_table__;
	cbd.tga->get_write_value(__correction_table__);

	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing CorrectionTable", 
			"XbpmPositionMode4::write_ZCorrectionTable"); 
	}

	// check if not the same value as current one
	if (m_z_correction_table.compare(__correction_table__) == 0)
	{
		DEBUG_STREAM << "Same value for ZCorrectionTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__correction_table__))
		{
			if (m_z1_offset_table_reader)
			{
				delete m_z1_offset_table_reader;
				m_z1_offset_table_reader = NULL;
			}
			if (m_z2_offset_table_reader)
			{
				delete m_z2_offset_table_reader;
				m_z2_offset_table_reader = NULL;
			}
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_GAIN_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
				m_z1_offset_table_reader = new TableReader(m_cfg.hostDevice, __correction_table__, "Gain -> KZ1",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for z correction table z1" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for z correction table z1", 
					"XbpmPositionMode4::write_ZCorrectionTable"); 
			}

			if (!m_z1_offset_table_reader)
			{
				ERROR_STREAM << "initialization failed - failed to access tableReader for z correction table z1" << std::endl;
                m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to access tableReader for z correction table z1", 
					"XbpmPositionMode4::write_ZCorrectionTable"); 
			}

			// create reader for gain --> coeff z2 table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_GAIN_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
				m_z2_offset_table_reader = new TableReader(m_cfg.hostDevice, __correction_table__, "Gain -> KZ2",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for z correction table z2" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for z correction table z2", 
					"XbpmPositionMode4::write_ZCorrectionTable"); 
			}

			if (!m_z2_offset_table_reader)
			{
				ERROR_STREAM << "initialization failed - failed to access tableReader for z correction table z2" << std::endl;
                m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to access tableReader for z correction table z2", 
					"XbpmPositionMode4::write_ZCorrectionTable"); 
			}

			// check if the corection table has the wrigth number of gain values
			long l_size1;
			long l_size2;
			m_z1_offset_table_reader->get_nb_data(l_size1);
			m_z2_offset_table_reader->get_nb_data(l_size2);

			if ((l_size1 != NB_ELTS_IN_GAIN_CORRECTION_TABLE) ||
				  (l_size2 != NB_ELTS_IN_GAIN_CORRECTION_TABLE))
			{
				ostringstream ossMsgErr;
				ossMsgErr << "initialization failed - gain correction table should contain " 
					<< NB_ELTS_IN_GAIN_CORRECTION_TABLE << " offsets values" << std::endl;
				ERROR_STREAM << ossMsgErr.str().c_str() << endl;
				THROW_DEVFAILED("DEVICE_ERROR",
					ossMsgErr.str().c_str(),
					"XbpmPositionMode4::write_ZCorrectionTable"); 
			}
			m_z_correction_table = __correction_table__;
 			std::string prop_name = std::string("__") + ZCORR_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __correction_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_ZCorrectionTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode4::write_ZCorrectionTable"); 
		}
	}
}
} // namespace Xbpm_ns
