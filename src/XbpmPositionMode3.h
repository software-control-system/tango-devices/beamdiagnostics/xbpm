//=============================================================================
// XbpmPositionMode3.h
//=============================================================================
// abstraction.......XbpmPositionMode3 for XbpmPositionModes
// class.............XbpmPositionMode3
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _XBPM_POSITION_MODE3
#define _XBPM_POSITION_MODE3

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionInterface.h"
#include "TableReader.h"


namespace Xbpm_ns
{

// ============================================================================
// class: XbpmPositionMode3
// ============================================================================
class XbpmPositionMode3 : public XbpmPositionInterface
{

public:

  //- constructor
  XbpmPositionMode3(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager);

  //- destructor
  virtual ~XbpmPositionMode3();

  //- init
  void init();

  //- gets xbpm manager state
  Tango::DevState get_state();
  
  //- gets status
  std::string get_status();

  //-gets properties
  void get_properties();
  
  //-gets position
  Position_t get_position(Currents_t p_currents);
  
  //- sets new electrometer gain value
  void set_electrometer_gain (double gain)
  {
	    // memorize new gain value
        m_gain = gain;
  };

  //- read callback for dyn attr ZCorrectionTable
  void read_ZCorrectionTable(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for dyn attr ZCorrectionTable
  void write_ZCorrectionTable(yat4tango::DynamicAttributeWriteCallbackData & cbd);
  
private:
  
  //-computes x position
  void compute_x_position_i();
  
  //-computes z position
  void compute_z_position_i();

  //- configuration
  XbpmConfig m_cfg;

  //- module state
  Tango::DevState m_state;

  //- delta z
  double m_delta_z;

  //- electronic offset
  double m_electronic_offset;

  //- kz1 factor
  double m_kz1_factor;

  //- kz2 factor
  double m_kz2_factor;

  //- misalignment offset
  double m_misaligement_offset;

  //- offset z1
  double m_offset_z1;

  //- offset z2
  double m_offset_z2;

  //- operator offset
  double m_operator_offset;

  //- z correction table
  std::string m_z_correction_table;

  //- position
  Position_t m_position;

  //- Currents
  Currents_t m_currents;

  //- m_offset_1
  double m_offset_1;

  //- m_offset_2
  double m_offset_2;

  //- electrometer gain
  double m_gain;

  //- reader for Z1 offset correction table
  TableReader * m_z1_offset_table_reader;

  //- reader for Z2 offset correction table
  TableReader * m_z2_offset_table_reader;

  //- boolean for status details
  bool m_internal_error;

  //- Manager for Dynamic Attributes
  yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- boolean for managing dynamic attributes
  bool m_is_dyn_table;

  //- boolean for managing dynamic table reader
  bool m_is_table_empty;
};

} // namespace Xbpm_ns

#endif // _XBPM_POSITION_MODE3
