//=============================================================================
// CoefficientGrabberInsert.h
//=============================================================================
// abstraction.......CoefficientGrabberInsert for XbpmPositionModes
// class.............CoefficientGrabberInsert
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _COEFFICIENT_GRABBER_INSERT
#define _COEFFICIENT_GRABBER_INSERT

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmTypesAndConsts.h"
#include "CoefficientGrabber.h"
#include "TableReader.h"
#include <yat4tango/LogHelper.h>


namespace Xbpm_ns
{

	// ============================================================================
	// class: CoefficientGrabberInsert
	// ============================================================================
	class CoefficientGrabberInsert : public CoefficientGrabber
	{

	public:

		//- constructor
		CoefficientGrabberInsert(const CoefficientGrabberInsertConfig& cfg);

		//- destructor
		virtual ~CoefficientGrabberInsert();

		//- init
		void init();

		//- gets state
		Tango::DevState get_state();

		//- gets status
		std::string get_status();

		//- gets coefficients
		void get_coefficients(double & p_kx, double & p_kz, double & p_offset_x, double & p_offset_z);

	private:
		//- configuration
		CoefficientGrabberInsertConfig m_cfg;

		//- module state
		Tango::DevState m_state;

    //- detailed states
    Tango::DevState m_state_insertion;

		//- status
		std::string m_status;

		//- Proxy on insertion
		Tango::DeviceProxy * m_dev_insertion;

		//- table reader for k_table x
		TableReader * m_k_table_x_reader;

		//- reader for k_table z
		TableReader * m_k_table_z_reader;

		//- reader for offset table x
		TableReader * m_offset_table_x_reader;

		//- reader for offset table z
		TableReader * m_offset_table_z_reader;

		//- boolean for status details
		bool m_insertion_in_fault;
		bool m_internal_error;
	};

} // namespace Xbpm_ns

#endif // _COEFFICIENT_GRABBER_INSERT