//=============================================================================
// PositionDataGrabber.cpp
//=============================================================================
// abstraction.......Positions data grabber for Xbpm
// class.............PositionDataGrabber
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PositionDataGrabber.h"
#include <yat4tango/LogHelper.h>
#include <math.h>

namespace Xbpm_ns
{
// ============================================================================
// PositionDataGrabber::PositionDataGrabber()
// ============================================================================ 
PositionDataGrabber::PositionDataGrabber(Tango::DeviceImpl * hostDevice, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: Tango::LogAdapter(hostDevice), 
  m_dyn_attr_manager(p_dyn_attr_manager)
{
  m_state = Tango::UNKNOWN;
  m_status = "";
  m_position_mode_interface = NULL;
  m_storage_mgr = NULL;
}

// ============================================================================
// PositionDataGrabber::~PositionDataGrabber()
// ============================================================================ 
PositionDataGrabber::~PositionDataGrabber()
{
  if (m_position_mode_interface)
  {
    delete m_position_mode_interface;
    m_position_mode_interface = NULL;
  }
}

// ============================================================================
// PositionDataGrabber::get_state ()
// ============================================================================ 
Tango::DevState PositionDataGrabber::get_state()
{
  // get position mode interface state
  if (m_position_mode_interface)
  {
    m_state = m_position_mode_interface->get_state();
  }
  else
  {
    m_state = Tango::FAULT;
  }

  return m_state;
}

// ============================================================================
// PositionDataGrabber::get_status ()
// ============================================================================ 
std::string PositionDataGrabber::get_status()
{
  // get position mode interface status
  if (m_position_mode_interface)
  {
    m_status = m_position_mode_interface->get_status();
  }
  else
  {
    m_status = "Unknown";
  }

  return m_status;
}

// ============================================================================
// PositionDataGrabber::init ()
// ============================================================================ 
void PositionDataGrabber::init(XbpmConfig& cfg)
{
  m_cfg = cfg;

  if (!m_dyn_attr_manager)
  {
    ERROR_STREAM << "initialization failed - failed to access DynAttrManager" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to access DynAttrManager", 
			"PositionDataGrabber::init"); 
  }

  // create dynamic attributes associated to position data

  //- POSITION HISTORY
  if (m_cfg.enablePositionHistory == true)
  {
    //add dyn attr position history H, V, and std deviation for H, V
    yat4tango::DynamicAttributeInfo dai;
    dai.dev = m_cfg.hostDevice;
    dai.tai.name = H_POS_HIS;
    dai.tai.label = H_POS_HIS;
    //- describe the dyn attr we want...
    dai.tai.data_type = Tango::DEV_DOUBLE;
    dai.tai.data_format = Tango::SPECTRUM;
    dai.tai.writable = Tango::READ;
    dai.tai.disp_level = Tango::EXPERT;
    dai.tai.format = "%1.4e";
    dai.tai.max_dim_x = LONG_MAX;
		
    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai.cdb = true;
    //- read callback
    dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &PositionDataGrabber::read_history_position_h);
    m_dyn_attr_manager->add_attribute(dai);

    yat4tango::DynamicAttributeInfo dai2;
    dai2.dev = m_cfg.hostDevice;
    dai2.tai.name = V_POS_HIS;
    dai2.tai.label = V_POS_HIS;
    //- describe the dyn attr we want...
    dai2.tai.data_type = Tango::DEV_DOUBLE;
    dai2.tai.data_format = Tango::SPECTRUM;
    dai2.tai.writable = Tango::READ;
    dai2.tai.disp_level = Tango::EXPERT;
    dai2.tai.format = "%1.4e";
    dai2.tai.max_dim_x = LONG_MAX;
		
    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai2.cdb = true;
    //- read callback
    dai2.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &PositionDataGrabber::read_history_position_v);
    m_dyn_attr_manager->add_attribute(dai2);

    yat4tango::DynamicAttributeInfo dai3;
    dai3.dev = m_cfg.hostDevice;
    dai3.tai.name = H_POS_STD_DEV;
    dai3.tai.label = H_POS_STD_DEV;
    //- describe the dyn attr we want...
    dai3.tai.data_type = Tango::DEV_DOUBLE;
    dai3.tai.data_format = Tango::SCALAR;
    dai3.tai.writable = Tango::READ;
    dai3.tai.disp_level = Tango::EXPERT;
    dai3.tai.format = "%1.4e";
    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai3.cdb = true;
    //- read callback
    dai3.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &PositionDataGrabber::read_std_position_h);
    m_dyn_attr_manager->add_attribute(dai3);

    yat4tango::DynamicAttributeInfo dai4;
    dai4.dev = m_cfg.hostDevice;
    dai4.tai.name = V_POS_STD_DEV;
    dai4.tai.label = V_POS_STD_DEV;
    //- describe the dyn attr we want...
    dai4.tai.data_type = Tango::DEV_DOUBLE;
    dai4.tai.data_format = Tango::SCALAR;
    dai4.tai.writable = Tango::READ;
    dai4.tai.disp_level = Tango::EXPERT;
    dai4.tai.format = "%1.4e";
    //- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
    dai4.cdb = true;
    //- read callback
    dai4.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, &PositionDataGrabber::read_std_position_v);
    m_dyn_attr_manager->add_attribute(dai4);

    m_position_history_h.capacity(0);
    m_position_history_h.force_length(0);
    m_position_history_v.capacity(0);
    m_position_history_v.force_length(0);

    m_dynamic_position_data.position_std_dev_h = 0;
    m_dynamic_position_data.position_std_dev_v = 0;
  }

  // instanciate position mode
  try
  {
    load_position_mode();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to create positionMode", 
			"PositionDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to create positionMode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to create positionMode", 
			"PositionDataGrabber::init"); 
  }
}

// ============================================================================
// PositionDataGrabber::load_position_mode ()
// ============================================================================ 
void PositionDataGrabber::load_position_mode() 
{
  // instanciate position mode
  try
  {
    m_position_mode_interface = XbpmPositionInterfaceFactory::instanciate(m_cfg, m_dyn_attr_manager);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to create positionMode", 
			"PositionDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to create positionMode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to create positionMode", 
			"PositionDataGrabber::init"); 
  }

  if (!m_position_mode_interface)
  {
    ERROR_STREAM << "initialization failed - failed to access positionMode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to access positionMode", 
			"PositionDataGrabber::init"); 
  }

  // get position mode properties
  try
  {
    m_position_mode_interface->get_properties();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to get position mode properties", 
			"PositionDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to get position mode properties" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to get position mode properties", 
			"PositionDataGrabber::init"); 
  }

  // initialize position mode
  try
  {
    m_position_mode_interface->init();
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"initialization failed - failed to get init positionMode", 
			"PositionDataGrabber::init"); 
  }
  catch (...)
  {
    ERROR_STREAM << "initialization failed - failed to init positionMode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to init positionMode", 
			"PositionDataGrabber::init"); 
  }
}

// ============================================================================
// PositionDataGrabber::unload_position_mode ()
// ============================================================================ 
void PositionDataGrabber::unload_position_mode()
{
  if (m_position_mode_interface)
  {
    delete m_position_mode_interface;
    m_position_mode_interface = NULL;
  }
}

// ============================================================================
// PositionDataGrabber::set_electrometer_gain ()
// ============================================================================ 
void PositionDataGrabber::set_electrometer_gain(double gain)
{
  // set electrometer gain
  try
  {
    m_position_mode_interface->set_electrometer_gain(gain);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"failed to set gain", 
			"PositionDataGrabber::set_electrometer_gain");
  }
  catch (...)
  {
    ERROR_STREAM << "failed to load new position mode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"failed to set gain", 
			"PositionDataGrabber::set_electrometer_gain"); 
  }
}

// ============================================================================
// PositionDataGrabber::set_computation_mode ()
// ============================================================================ 
void PositionDataGrabber::set_computation_mode(unsigned short mode)
{
  unload_position_mode();

  m_cfg.xBPMmode = mode;

  // initialize position mode
  try
  {
    load_position_mode();
    m_computation_mode = mode;
  }
  catch (Tango::DevFailed &e)
  {
    m_cfg.xBPMmode = m_computation_mode;
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"failed to load new position mode", 
			"PositionDataGrabber::set_computation_mode");
  }
  catch (...)
  {
    m_cfg.xBPMmode = m_computation_mode;
    ERROR_STREAM << "failed to load new position mode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"failed to load new position mode", 
			"PositionDataGrabber::set_computation_mode"); 
  }
}

// ============================================================================
// PositionDataGrabber::get_position ()
// ============================================================================ 
Position_t PositionDataGrabber::get_position(Currents_t p_currents, bool nx_enabled)
{
  if (!m_position_mode_interface)
  {
    ERROR_STREAM << "initialization failed - failed to access positionMode" << std::endl;
    THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to access positionMode", 
			"PositionDataGrabber::get_position"); 
  }

  try
  {
    m_position = m_position_mode_interface->get_position(p_currents);
  }
  catch (Tango::DevFailed &e)
  {
    ERROR_STREAM << e << std::endl;
    RETHROW_DEVFAILED(e, "DEVICE_ERROR", 
			"Position computation failed. See log for further details", 
			"PositionDataGrabber::get_position"); 
  }
  catch (...)
  {
    THROW_DEVFAILED("DEVICE_ERROR", 
                    "Position computation failed. See log for further details", 
                    "PositionDataGrabber::get_position"); 
  }

  // record new value if recording enabled
  if (nx_enabled)
  {
    if (m_storage_mgr == NULL)
    {
      ERROR_STREAM << "PositionDataGrabber::get_position: cannot record data, storage mger is NULL!" << std::endl;
      THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, internal pointer is NULL!",
                    "CurrentDataGrabber::get_position");
    }
    else
    {
      try
      {
        DEBUG_STREAM << "Pushing data for posH" << endl;
        m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + H_POS, &m_position.x, 1);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "PositionDataGrabber::get_position: pushNexusData caught DevFailed: " << df << std::endl;
        m_storage_mgr->manageNexusAbort();
        throw;
      }
      catch (...)
      {
        ERROR_STREAM << "PositionDataGrabber::get_position: pushNexusData caugth unknown exception!" << std::endl;
        m_storage_mgr->manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, caught [...]!",
                    "CurrentDataGrabber::get_position");
      }
      try
      {
        DEBUG_STREAM << "Pushing data for posV" << endl;
        m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + V_POS, &m_position.z, 1);
      }
      catch (Tango::DevFailed & df)
      {
        ERROR_STREAM << "PositionDataGrabber::get_position: pushNexusData caught DevFailed: " << df << std::endl;
        m_storage_mgr->manageNexusAbort();
        throw;
      }
      catch (...)
      {
        ERROR_STREAM << "PositionDataGrabber::get_position: pushNexusData caugth unknown exception!" << std::endl;
        m_storage_mgr->manageNexusAbort();
        THROW_DEVFAILED("DEVICE_ERROR",
                "Failed to record data, caught [...]!",
                "CurrentDataGrabber::get_position");
      }
    }
  }

  return m_position;
}

// ============================================================================
// PositionDataGrabber::compute_dynamic_position_data ()
// ============================================================================ 
void PositionDataGrabber::compute_dynamic_position_data(Currents_Histos_t sai_currents, bool histo_reset_needed, bool nx_enabled)
{
  if (m_cfg.enablePositionHistory)
  {
    size_t new_data_nb = sai_currents.histo0.length();
    History_t l_position_h_storage;
    l_position_h_storage.clear();
    History_t l_position_v_storage;
    l_position_v_storage.clear();

    if (histo_reset_needed)
    {
      m_position_history_h.clear();
      m_position_history_h.capacity(0);
      m_position_history_h.force_length(0);
      m_position_history_v.clear();
      m_position_history_v.capacity(0);
      m_position_history_v.force_length(0);
      m_position_history_h.capacity(new_data_nb);
      m_position_history_h.force_length(new_data_nb);
      m_position_history_v.capacity(new_data_nb);
      m_position_history_v.force_length(new_data_nb);
    }

    // update m_position_history_h & m_position_history_v with computation on new current data from SAI:
    //   currents => position
    for (size_t idx = 0; idx < new_data_nb; idx++)
    {
      Currents_t l_currents;
      l_currents.current1 = sai_currents.histo0[idx];
      l_currents.current2 = sai_currents.histo1[idx];
      l_currents.current3 = sai_currents.histo2[idx];
      l_currents.current4 = sai_currents.histo3[idx];

      size_t added_index = idx;

      Position_t l_pos = m_position_mode_interface->get_position(l_currents);

      // if update data: append new values
      // otherwise: overwrite existing values
      if (!histo_reset_needed)
      {
        size_t new_capacity = m_position_history_h.length() + 1;
        m_position_history_h.capacity(new_capacity, true);
        m_position_history_h.force_length(new_capacity);
        m_position_history_h[new_capacity - 1] = l_pos.x;
        m_position_history_v.capacity(new_capacity, true);
        m_position_history_v.force_length(new_capacity);
        m_position_history_v[new_capacity - 1] = l_pos.z;
        added_index = new_capacity - 1;
      }
      else
      {
        m_position_history_h[idx] = l_pos.x;
        m_position_history_v[idx] = l_pos.z;
      }

      // store value for recording
      if (nx_enabled)
      {
        size_t new_capacity = l_position_h_storage.length() + 1;
        l_position_h_storage.capacity(new_capacity, true);
        l_position_h_storage.force_length(new_capacity);
        l_position_h_storage[new_capacity - 1] = l_pos.x;
        l_position_v_storage.capacity(new_capacity, true);
        l_position_v_storage.force_length(new_capacity);
        l_position_v_storage[new_capacity - 1] = l_pos.z;
      }

      compute_position_std_deviation();
    }

    // record new values if recording enabled
    if (nx_enabled && (l_position_h_storage.length() != 0))
    {
      if (m_storage_mgr == NULL)
      {
        ERROR_STREAM << "PositionDataGrabber::compute_dynamic_position_data: cannot record data, storage mger is NULL!" << std::endl;
        THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, internal pointer is NULL!",
                    "CurrentDataGrabber::compute_dynamic_position_data");
      }
      else
      {
        try
        {
            DEBUG_STREAM << "Pushing data for posH history: " << l_position_h_storage.length() << " values..." << endl;
            m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + H_POS_HIS, l_position_h_storage.base(), l_position_h_storage.length());
        }
        catch (Tango::DevFailed & df)
        {
            ERROR_STREAM << "PositionDataGrabber::compute_dynamic_position_data: pushNexusData caught DevFailed: " << df << std::endl;
            m_storage_mgr->manageNexusAbort();
            throw;
        }
        catch (...)
        {
            ERROR_STREAM << "PositionDataGrabber::compute_dynamic_position_data: pushNexusData caugth unknown exception!" << std::endl;
            m_storage_mgr->manageNexusAbort();
            THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, caught [...]!",
                    "CurrentDataGrabber::compute_dynamic_position_data");
        }
        try
        {
            DEBUG_STREAM << "Pushing data for posV history: " << l_position_v_storage.length() << " values..." << endl;
            m_storage_mgr->pushNexusData(m_storage_mgr->getNexusFileName() + "_" + V_POS_HIS, l_position_v_storage.base(), l_position_v_storage.length());
        }
        catch (Tango::DevFailed & df)
        {
            ERROR_STREAM << "PositionDataGrabber::compute_dynamic_position_data: pushNexusData caught DevFailed: " << df << std::endl;
            m_storage_mgr->manageNexusAbort();
            throw;
        }
        catch (...)
        {
            ERROR_STREAM << "PositionDataGrabber::compute_dynamic_position_data: pushNexusData caugth unknown exception!" << std::endl;
            m_storage_mgr->manageNexusAbort();
            THROW_DEVFAILED("DEVICE_ERROR",
                    "Failed to record data, caught [...]!",
                    "CurrentDataGrabber::compute_dynamic_position_data");
        }
      }
    }
  }
}

// ============================================================================
// PositionDataGrabber::start ()
// ============================================================================ 
void PositionDataGrabber::start()
{
  // intialize position history
  if (m_cfg.enablePositionHistory)
  {
    m_position_history_h.clear();
    m_position_history_h.capacity(0);
    m_position_history_h.force_length(0);
    m_position_history_v.clear();
    m_position_history_v.capacity(0);
    m_position_history_v.force_length(0);

    m_dynamic_position_data.position_std_dev_h = 0;
    m_dynamic_position_data.position_std_dev_v = 0;
  }
}

// ============================================================================
// PositionDataGrabber::compute_position_std_deviation ()
// ============================================================================ 
void PositionDataGrabber::compute_position_std_deviation()
{
  double l_sum_h = 0.0;
  for (size_t idx = 0; idx < m_position_history_h.length(); idx++)
  {
    l_sum_h += m_position_history_h[idx];
  }
  double l_moy_h = l_sum_h / m_position_history_h.length();
  m_dynamic_position_data.position_std_dev_h = Xbpm_ns::std_dev_calculation(m_position_history_h, l_moy_h);

  double l_sum_v = 0.0;
  for (size_t idx = 0; idx < m_position_history_v.length(); idx++)
  {
      l_sum_v += m_position_history_v[idx];
  }
  double l_moy_v = l_sum_v / m_position_history_v.length();
  m_dynamic_position_data.position_std_dev_v = Xbpm_ns::std_dev_calculation(m_position_history_v, l_moy_v);
}

// ============================================================================
// PositionDataGrabber::read_history_position_h ()
// ============================================================================ 
void PositionDataGrabber::read_history_position_h(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);
  cbd.tga->set_value(m_position_history_h.base(), m_position_history_h.length());
}

// ============================================================================
// PositionDataGrabber::read_history_position_v ()
// ============================================================================ 
void PositionDataGrabber::read_history_position_v(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);
  cbd.tga->set_value(m_position_history_v.base(), m_position_history_v.length());
}

// ============================================================================
// PositionDataGrabber::read_std_position_h ()
// ============================================================================ 
void PositionDataGrabber::read_std_position_h(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);
  cbd.tga->set_value(&m_dynamic_position_data.position_std_dev_h);
}

// ============================================================================
// PositionDataGrabber::read_std_position_v ()
// ============================================================================ 
void PositionDataGrabber::read_std_position_v(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
  yat::AutoMutex<> guard(m_lock);
  cbd.tga->set_value(&m_dynamic_position_data.position_std_dev_v);
}

// ============================================================================
// PositionDataGrabber::init_nexus ()
// ============================================================================ 
void PositionDataGrabber::init_nexus(NexusManager * nx_mgr)
{
  if (nx_mgr == NULL)
  {
    THROW_DEVFAILED("CONFIGURATION_ERROR",
          "Failed to set nexus manager: null pointer!",
          "PositionDataGrabber::init_nexus");
  }
  m_storage_mgr = nx_mgr;
}

} // namespace Xbpm_ns

