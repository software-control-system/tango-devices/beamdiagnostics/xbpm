//=============================================================================
// XbpmPositionMode5.cpp
//=============================================================================
// abstraction.......XbpmPositionMode5 for XbpmPositionModes
// class.............XbpmPositionMode5
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "XbpmPositionMode5.h"


namespace Xbpm_ns
{

// ============================================================================
// XbpmPositionMode5::XbpmPositionMode5 ()
// ============================================================================ 
XbpmPositionMode5::XbpmPositionMode5(const XbpmConfig& cfg, yat4tango::DynamicAttributeManager * p_dyn_attr_manager)
: XbpmPositionInterface(cfg, p_dyn_attr_manager),
m_cfg(cfg), 
m_dyn_attr_manager(p_dyn_attr_manager)
{
	m_kx_factor = 1;
	m_kz_factor = 1;
	m_misaligement_offset_x = 0;
	m_misaligement_offset_z = 0;
	m_offset_x = 0;
	m_offset_z = 0;
	m_operator_offset_x = 0;
	m_operator_offset_z = 0;
	m_correction_table = "";
	m_offsetx_table_reader = NULL;
	m_offsetz_table_reader = NULL;
    m_state = Tango::UNKNOWN;
    m_internal_error = false;
    m_is_dyn_table = true;
    m_is_table_empty = true;
    m_gain = yat::IEEE_NAN;
}

// ============================================================================
// XbpmPositionMode5::~XbpmPositionMode5 ()
// ============================================================================ 
XbpmPositionMode5::~XbpmPositionMode5()
{
	if (m_offsetx_table_reader)
	{
		delete m_offsetx_table_reader;
        m_offsetx_table_reader = NULL;
	}
	if (m_offsetz_table_reader)
	{
		delete m_offsetz_table_reader;
        m_offsetz_table_reader = NULL;
	}
	if (m_is_dyn_table)
	{
		m_dyn_attr_manager->remove_attribute(CORR_TABLE);
	}
}

// ============================================================================
// XbpmPositionMode5::init ()
// ============================================================================ 
void XbpmPositionMode5::init()
{
    m_internal_error = false;

	if (!m_is_table_empty)
	{
        // create reader for gain --> offset X table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_GAIN_IN_FILE_1D);
            l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
            m_offsetx_table_reader = new TableReader(m_cfg.hostDevice, m_correction_table, "Gain -> OffsetX",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for correction table x" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to create tableReader for correction table x", 
                "XbpmPositionMode5::init"); 
        }

        if (!m_offsetx_table_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for correction table x" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to access tableReader for correction table x", 
                "XbpmPositionMode5::init"); 
        }

        // create reader for gain --> offset Z table
        try
        {
            std::vector<int> l_list;
            l_list.push_back(INDEX_GAIN_IN_FILE_1D);
            l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
            m_offsetz_table_reader = new TableReader(m_cfg.hostDevice, m_correction_table, "Gain -> OffsetZ",false,l_list);
        }
        catch (...)
        {
            ERROR_STREAM << "initialization failed - failed to create tableReader for correction table z" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to create tableReader for correction table z", 
                "XbpmPositionMode5::init"); 
        }

        if (!m_offsetz_table_reader)
        {
            ERROR_STREAM << "initialization failed - failed to access tableReader for correction table z" << std::endl;
            THROW_DEVFAILED("DEVICE_ERROR", 
                "initialization failed - failed to access tableReader for correction table z", 
                "XbpmPositionMode5::init"); 
        }

        // check if the corection table has the wrigth number of gain values
        long l_size1;
        long l_size2;
        m_offsetx_table_reader->get_nb_data(l_size1);
        m_offsetz_table_reader->get_nb_data(l_size2);

        if ((l_size1 != NB_ELTS_IN_GAIN_CORRECTION_TABLE) ||
            (l_size2 != NB_ELTS_IN_GAIN_CORRECTION_TABLE))
        {
                ostringstream ossMsgErr;
                ossMsgErr   << "initialization failed - gain correction table should contain " 
                            << NB_ELTS_IN_GAIN_CORRECTION_TABLE << " offsets values" << std::endl;
                ERROR_STREAM << ossMsgErr.str().c_str() << endl;
                THROW_DEVFAILED("DEVICE_ERROR",
                                ossMsgErr.str().c_str(),
                                "XbpmPositionMode5::init"); 
        }
    }

    // init OK
    m_state = Tango::ON;
}

// ============================================================================
// XbpmPositionMode5::get_state ()
// ============================================================================ 
Tango::DevState XbpmPositionMode5::get_state()
{
    // update module state
    if (m_internal_error)
    {
        m_state = Tango::FAULT;
    }
    else
    {
        m_state = Tango::ON;
    }

	return m_state;
}

// ============================================================================
// XbpmPositionMode5::get_status ()
// ============================================================================ 
std::string XbpmPositionMode5::get_status()
{
    std::string status = "";

    // compose module general status
    switch (m_state)
    {
        case Tango::RUNNING:
        case Tango::ON:
        case Tango::MOVING:
        {
            status = "current module state is: RUNNING\n";
        }
        break;
        case Tango::ALARM:
        {
            status = "current module state is: ALARM\n";
        }
        break;
        case Tango::STANDBY:
        case Tango::OFF:
        {
            status = "current module state is: STANDBY\n";
        }
        break;
        case Tango::FAULT:
        {
            status = "module has switched to FAULT state\n";
            if (m_internal_error)
                status += "Internal error!\n";
            else
                status += "Unknown error!\n";
        }
        break;
        default:
        {
            status = "Unhandled module state!!\n";
        }
        break;
    }

	return status;
}

// ============================================================================
// XbpmPositionMode5::get_properties ()
// ============================================================================ 
void XbpmPositionMode5::get_properties()
{
	if (!m_dyn_attr_manager)
	{
		ERROR_STREAM << "initialization failed - failed to access DynAttrManager" << std::endl;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"initialization failed - failed to access DynAttrManager", 
			"XbpmPositionMode5::init"); 
	}

	Tango::DbData dev_prop;
	dev_prop.push_back(Tango::DbDatum("Mode5"));

	//- Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		m_cfg.hostDevice->get_db_device()->get_property(dev_prop);

	ConfigurationParser l_conf_parser(m_cfg.hostDevice);
	std::vector<std::string> l_vect;
	if (!dev_prop[0].is_empty())
	{
		ModeKeys_t l_keys;
		bool l_is_mode5;
		dev_prop[0] >> l_vect;
		try
		{
			l_keys = l_conf_parser.parseConfigProperty(l_vect);
			l_is_mode5 = true;
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, "CONFIGURATION_ERROR",
				"Failed to parse Mode5 property", 
				"XbpmPositionMode5::get_properties"); 
		}
		catch (...)
		{
			THROW_DEVFAILED("CONFIGURATION_ERROR", 
				"Failed to parse Mode5 property", 
				"XbpmPositionMode5::get_properties"); 
		}
		if (l_is_mode5)
		{
			try
			{
				m_offset_x = l_conf_parser.extractOffsetX(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for OffsetX.Using default value : 0" << endl;
            }
			try
            {
				m_offset_z = l_conf_parser.extractOffsetZ(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for OffsetZ.Using default value : 0" << endl;
            }
			try
            {
				m_kx_factor = l_conf_parser.extractKXFactor(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for Kx_Factor.Using default value : 1" << endl;
            }
			try
			{
				m_kz_factor = l_conf_parser.extractKZFactor(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for Kz_Factor.Using default value : 1" << endl;
			}
			try
			{
				m_misaligement_offset_x = l_conf_parser.extractMisalignementOffsetX(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for MisalignementOffsetX.Using default value : 0" << endl;
			}
			try
			{
				m_misaligement_offset_z = l_conf_parser.extractMisalignementOffsetZ(l_keys);
			}
			catch (...)
			{
				INFO_STREAM << "No value for MisalignementOffsetZ.Using default value : 0" << endl;
			}
			try
			{
				m_operator_offset_x = l_conf_parser.extractOperatorOffsetX(l_keys);
			}
			catch (...)
            {
				INFO_STREAM << "No value for OperatorOffsetX.Using default value : 0" << endl;
            }
			try
            {
				m_operator_offset_z = l_conf_parser.extractOperatorOffsetZ(l_keys);
            }
			catch (...)
            {
				INFO_STREAM << "No value for OperatorOffsetX.Using default value : 0" << endl;
            }
			try
            {
				m_correction_table = l_conf_parser.extractCorrectionTable(l_keys);
				m_is_table_empty = false;
				m_is_dyn_table = false;
			}
			catch (...)
            {
				INFO_STREAM << "No value for correctionTable.Creating Dynamic Attribute" << endl;
            }
        }
	}

	if (m_is_dyn_table)
	{
		//add dyn attr correctionTable
		yat4tango::DynamicAttributeInfo dai;
		dai.dev = m_cfg.hostDevice;
		dai.tai.name = CORR_TABLE;
		dai.tai.label = CORR_TABLE;
		//- describe the dyn attr we want...
		dai.tai.data_type = Tango::DEV_STRING;
		dai.tai.data_format = Tango::SCALAR;
		dai.tai.writable = Tango::READ_WRITE;
		dai.tai.disp_level = Tango::EXPERT;
		dai.tai.description = "Path and name of the Correction table";
		//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
		dai.cdb = false;
		//- read callback
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
			&XbpmPositionMode5::read_CorrectionTable);
		dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
			&XbpmPositionMode5::write_CorrectionTable);
		m_dyn_attr_manager->add_attribute(dai);

        try
        {
        std::string prop_name = std::string("__") + CORR_TABLE;
        m_correction_table = get_value_as_property<std::string>(m_cfg.hostDevice, prop_name);
        m_is_table_empty = false;
        }
        catch(...)
        {
        // no memorized value, set default value
        m_correction_table = kDEFAULT_FILE_NAME;
        }
	}
}

// ============================================================================
// XbpmPositionMode5::get_position ()
// ============================================================================ 
Position_t XbpmPositionMode5::get_position(Currents_t p_currents)
{
    m_internal_error = false;
	m_currents = p_currents;

	// get offset and coefficients values from tables according to gain value	

	if (!m_offsetx_table_reader)
	{
		ERROR_STREAM << "Failed to access tableReader for correction table x" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access tableReader for correction table x", 
			"XbpmPositionMode5::get_position"); 
	}

	try
	{
		m_current_offset_x = m_offsetx_table_reader->get_interpolated_value(m_gain);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for OffsetX" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for OffsetX", 
			"XbpmPositionMode5::get_position"); 
	}

	if (!m_offsetz_table_reader)
	{
		ERROR_STREAM << "Failed to access tableReader for correction table z" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to access tableReader for correction table z", 
			"XbpmPositionMode5::get_position"); 
	}

	try
	{
		m_current_offset_z = m_offsetz_table_reader->get_interpolated_value(m_gain);
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to compute value for OffsetZ" << std::endl;
        m_internal_error = true;
		THROW_DEVFAILED("DEVICE_ERROR", 
			"Failed to compute value for OffsetZ", 
			"XbpmPositionMode5::get_position"); 
	}

	compute_x_position_i();
	compute_z_position_i();

	return m_position;
}

// ============================================================================
// XbpmPositionMode5::compute_x_position_i ()
// ============================================================================ 
void XbpmPositionMode5::compute_x_position_i()
{
	// X = ( ((I0 + I3) - (I1 + I2)) / Isum ) * Kx_Factor - OffsetX - MisalignementOffsetX - OperatorOffsetX - X_current_offset_correction 
	m_position.x = (((m_currents.current1 + m_currents.current4) - (m_currents.current2 + m_currents.current3)) / m_currents.intensity) * m_kx_factor - m_offset_x - m_misaligement_offset_x - m_operator_offset_x - m_current_offset_x;
}

// ============================================================================
// XbpmPositionMode5::compute_z_position_i ()
// ============================================================================ 
void XbpmPositionMode5::compute_z_position_i()
{
	// Z = ( ((I0 + I1) - (I3 + I2)) / Isum ) * Kz_Factor - OffsetZ - MisalignementOffsetZ - OperatorOffsetZ - Z_current_offset_correction
	m_position.z = (((m_currents.current1 + m_currents.current2) - (m_currents.current4 + m_currents.current3)) / m_currents.intensity) * m_kz_factor - m_offset_z - m_misaligement_offset_z - m_operator_offset_z - m_current_offset_z;
}

// ============================================================================
// XbpmPositionMode5::read_CorrectionTable ()
// ============================================================================ 
void XbpmPositionMode5::read_CorrectionTable(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static char * __correction_table__;
	static std::string __correction_table_str__;

	__correction_table_str__ = m_correction_table;
	__correction_table__ = const_cast<char*>(__correction_table_str__.c_str());  
	cbd.tga->set_value(&__correction_table__);
}

// ============================================================================
// XbpmPositionMode5::write_CorrectionTable ()
// ============================================================================ 
void XbpmPositionMode5::write_CorrectionTable(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
    m_internal_error = false;
	char * __correction_table__;
	cbd.tga->get_write_value(__correction_table__);
	
	if (m_cfg.hostDevice->dev_state() == Tango::RUNNING)
	{
		THROW_DEVFAILED("DEVICE_ERROR", 
			"The device must be in STANDBY mode for editing CorrectionTable", 
			"XbpmPositionMode5::write_CorrectionTable"); 
	}

	// check if not the same value as current one
	if (m_correction_table.compare(__correction_table__) == 0)
	{
		DEBUG_STREAM << "Same value for CorrectionTable - do nothing" << endl;
	}
	else
	{
		if (Xbpm_ns::is_file_readable(__correction_table__))
		{
			DEBUG_STREAM << "file is readable" << endl;
			if (m_offsetx_table_reader)
			{
				delete m_offsetx_table_reader;
			}
			if (m_offsetz_table_reader)
			{
				delete m_offsetz_table_reader;
			}

			// create reader for gain --> offset X table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_GAIN_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETX_IN_FILE_1D);
				m_offsetx_table_reader = new TableReader(m_cfg.hostDevice, __correction_table__, "Gain -> OffsetX",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for correction table x" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for correction table x", 
					"XbpmPositionMode5::write_CorrectionTable"); 
			}

			if (!m_offsetx_table_reader)
			{
				ERROR_STREAM << "initialization failed - failed to access tableReader for correction table x" << std::endl;
                m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to access tableReader for correction table x", 
					"XbpmPositionMode5::write_CorrectionTable"); 
			}

			// create reader for gain --> offset Z table
			try
			{
				std::vector<int> l_list;
				l_list.push_back(INDEX_GAIN_IN_FILE_1D);
				l_list.push_back(INDEX_OFFSETZ_IN_FILE_1D);
				m_offsetz_table_reader = new TableReader(m_cfg.hostDevice, __correction_table__, "Gain -> OffsetZ",false,l_list);
			}
			catch (...)
			{
				ERROR_STREAM << "initialization failed - failed to create tableReader for correction table z" << std::endl;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to create tableReader for correction table z", 
					"XbpmPositionMode5::write_CorrectionTable"); 
			}

			if (!m_offsetz_table_reader)
			{
				ERROR_STREAM << "initialization failed - failed to access tableReader for correction table z" << std::endl;
                m_internal_error = true;
				THROW_DEVFAILED("DEVICE_ERROR", 
					"initialization failed - failed to access tableReader for correction table z", 
					"XbpmPositionMode5::write_CorrectionTable"); 
			}

			// check if the corection table has the wrigth number of gain values
			long l_size1;
			long l_size2;
			m_offsetx_table_reader->get_nb_data(l_size1);
			m_offsetz_table_reader->get_nb_data(l_size2);

			if ((l_size1 != NB_ELTS_IN_GAIN_CORRECTION_TABLE) ||
				  (l_size2 != NB_ELTS_IN_GAIN_CORRECTION_TABLE))
			{
				ostringstream ossMsgErr;
				ossMsgErr << "initialization failed - gain correction table should contain " 
					<< NB_ELTS_IN_GAIN_CORRECTION_TABLE << " offsets values" << std::endl;
				ERROR_STREAM << ossMsgErr.str().c_str() << endl;
				THROW_DEVFAILED("DEVICE_ERROR",
					ossMsgErr.str().c_str(),
					"XbpmPositionMode5::write_CorrectionTable"); 
			}
			m_correction_table = __correction_table__;
 			std::string prop_name = std::string("__") + CORR_TABLE;
            yat4tango::PropertyHelper::set_property<std::string>(m_cfg.hostDevice, prop_name, __correction_table__);
		}
		else
		{
			THROW_DEVFAILED("DEVICE_ERROR",
				"write_CorrectionTable failed - the file doesn't seem to exist or isn't accessible",
				"XbpmPositionMode5::write_CorrectionTable"); 
		}
	}
}
} // namespace Xbpm_ns
